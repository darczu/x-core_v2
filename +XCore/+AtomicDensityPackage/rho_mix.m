% X-Core v2.0 developed by Piotr Darnowski - all rights reserved
% Rev 05-05-2019
% Rev 11.07.2017 % TO BE VERIFIED
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculations of the mixture density with different models. Allows any number of materials.
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: x_i			- atom fraction or mass fraction
% 2: rho_i		- density of pure constitutent
% 3: model 		- density calculation model
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: rho 			- density value
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% HULL MODEL  model -> 'Hull'
% Assumption: Mixing effects on the partial molar volumes of the constituents is small. 
% 1/rho_mix = sum( x_i/rho_i )
% x_i - vector with mole fractions or atomic fractions for HULL MODEL
% rho_i - vector of densities of the pure constitiuents
% Based on: Density Stratification and Fission Product Partitioning in Molten Corium Phases. D.A Powers et. al
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CLASSICAL Model  model = Mass Fraction -> 'MassFrac' 
% 1/rho_mix = sum( wf_i/rho_i )
%  wf_i - vector with mass fraction (wt%) of the mixture
%  rho_i - vector of densities of the pure constitiuents
% Based on: MCNP Crticiality Primer II Appendix B: Calculating Atom Densities
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CLASSICAL Model  model = Atomic Fraction -> 'AtomFrac'
% rho_mix = sum( af_i.*rho_i )
%  af_i - vector with atom fraction (at%) of the mixture
%  rho_i - vector of densities of the pure constitiuents
%  Based on: MCNP Crticiality Primer II Appendix B: Calculating Atom Densities
%
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%EXAMPLE 1: x_i=[0.2 0.8]; rho_i = [6.0 9.0]; model = 'Hull';
% rho = rho_mix(x_i,rho_i,model);
% rho = rho_mix([0.88, 0.12], [rho_INC(), rho_SS()],'MassFrac')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function rho = rho_mix(x_i,rho_i,model)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    import XCore.AtomicDensityPackage.*

if(nargin < 3)
 model ='MassFrac'; %Default model
end

tol = 1e-6;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INITIAL TESTS
 if(numel(x_i)~=numel(rho_i))
     error('ERROR #1 (rho_mix): x_i has diffrent size than mat_i !!!');
 end
 
 %if(sum(x_i) ~= 1.0)
 if(abs(sum(x_i) - 1.0) > tol)
% 2023 %	disp('WARNING #1 (rho_mix):  Atom fractions does not add to one !!! - Normalization applied');
    x_i = normalization(x_i);	 % it can normalize sometimes variables equal to 1 with 1e-16 precision !!! 
 end
 
 if( sum(x_i < 0)>0.0)
	error('ERROR #2 (rho_mix): Sum of molar fractions cannot be zero.');
 end
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch model
    case {1, 'Hull'}
        %HULL MODEL 
        % it demands atom fractions
        rho =   x_i./rho_i;
        rho = sum(rho);
        rho = 1./rho;
    
    case {2,'MassFrac','wf','Default'}
        %CLASSICAL MODEL THE SAME AS HULL BUT INPUT DATA x_i IS WEIGHT FRACTION
        % Based on [040]
        %x_i = x_i';
        
        rho = [];
        rho =   x_i./rho_i;
        rho = sum(rho);
        rho = 1./rho;  

    case {3, 'AtomFrac', 'af'}
        %CLASSICAL MODEL THE SAME AS HULL BUT INPUT DATA x_i IS ATOM FRACTION
        rho = x_i.*rho_i;
        rho = sum(rho);

    otherwise
        error('ERROR #3 (rho_mix): There is no such a model for density calculation');
 end
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
 
