% X-Core v2.0 developed by Piotr Darnowski - all rights reserved
% Rev 09-08-2017% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DENSITY function Material version; Calculation of the mixture density.
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%1: x_i        		- vector with mole/atomic fractions or numbers of atoms/moles or weight fraction or masses 
%2: MATERIAL_i 		- vector of MATERIALerial names or numbers it is cell type or numeric type
%3: Temperature      - temperature of the mixture; it can be matrix or scalar
%4: x_mode      	- x_i input vector data type - weigt or atomic  
%5: rho_model 	     - density mixture calculation mode  
%6: A_model		 - atomic weight calculations model
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%1: rho			- mass density
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% x_mode     = 'af'   or 1    		- atom fractions in input vector x_i
%              'wf'   or 2    		- weight fractions in input vector x_i - DEFAULT  
% rho_model  = 'Hull' or 1   
%             	'MassFrac' or 2  	-> DEFAULT  
% 			'AtomFrac' or 3	
% A_model	   = 'Simple' 	- uses MOLAR_BASE;  -> DEFAULT
%		 	'Complex' - uses XSDATA files	
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INPUT: x_i=[0.2 0.8]; MATERIAL_i ={'UO2', 'ZRO2'}; T=3000; model = 'Hull'; 
% EXAMPLE 1: rho    = rho_mixMAT(x_i,MATERIAL_i,T,'af',model,'Simple')
% EXAMPLE 2: rho = rho_mixMAT([1],{'UO2'},3000,'af','AtomFrac','Simple')
% 				   rho_mixMAT(1,{'SS'},1000)
%    rho = rho_mixMAT(x_i,MATERIAL_i,Temperature,x_mode,rho_model,A_model)
% EXAMPLE 3: rho_mixMAT(1,{'UO2'},2500)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function rho = rho_mixMAT(x_i,MATERIAL_i,Temperature,x_mode,rho_model,A_model)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	import XCore.AtomicDensityPackage.*

 %A_model = 'Simpe'; %or Complex
 %flag_output = 1;  %1 - active 0 -not active -> Informations about the result
flag_output = 0;

 % Available MATERIALs
[MATERIAL, MATERIAL_NO, TYPE] = MAT_LIST();
 
% SERIES OF TESTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%Matrix size test
    if(numel(x_i)~=numel(MATERIAL_i)) error('ERROR #1 (rho_mixMAT): x_i has diffrent size than MATERIAL_i');  end	 
	 
	%Temperature vector size test
	if(~or( numel(Temperature)==numel(MATERIAL_i),numel(Temperature)==1 )  )	 
		error('ERROR #2 (rho_mixMAT): T can be scalar or vector with number of values equal to number of material inputs'); 
	end 
	
	 % MATERIAL avability TEST
    for j=1:numel(MATERIAL_i)
			if(iscell(MATERIAL_i))
				dummy = sum(strcmp(MATERIAL_i{j}, MATERIAL));
			else
				dummy = sum((MATERIAL_i(j) == MATERIAL_NO));
			end
     	if( dummy==0 ) error('ERROR #3 (rho_mixMAT): There is no such Material available in MAT_LIST database!!!'); end
    end
	 
%DEFAULTS:
	if(nargin < 4)
% 2023 %	   disp('INFO #1 (rho_mixMAT): x_mode, rho_model and A_model DEFAULTs: wf, MassFrac, Simple ');
	   x_mode = 'wf';
	   rho_model ='MassFrac';
	   A_model = 'Simple';
	elseif(nargin < 5)
 % 2023 %      disp('INFO #2 (rho_mixMAT): rho_model and A_model DEFAULTs: MassFrac, Simple');
	   rho_model='MassFrac';  %DEFAULT
	   A_model = 'Simple';
	elseif(nargin < 6)
% 2023 %	    disp('INFO #3 (rho_mixMAT): A_model DEFAULT: Simple');
		A_model = 'Simple';  %DEFAULT
	end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% CALCULATIONS
% GDZIES PONIZEJ eps_a sie pojawia
rho_i = zeros(1,numel(MATERIAL_i));
 
   %density calculation
    for j=1:numel(MATERIAL_i)
			ro = -1;	%Material presence test
		
		if(numel(Temperature)>1)
			T = Temperature(j);  %Temperature
		else
			T = Temperature;
		end
		
		
		% Basic MELCOR materials - NEW DIVISION 2017
		if( iscell(MATERIAL_i))   %when material is defined as cellstring
			if( strcmp(MATERIAL_i{j}, MATERIAL(1)))
				if(strcmp(TYPE,'BWR'))     
					ro = DENSITY_BASE(15,T);   %B4C for BWRs %ro = rho_B4C(T);  
				elseif(strcmp(TYPE,'PWR'))
					ro = DENSITY_BASE(16,T);  %AIC for PWRs 	%ro = rho_AIC(T);  
				else 
					error('ERROR #4 (rho_mixMAT): No such reactor type avaiable');
				end
            else
				for k=2:numel(MATERIAL)
					if strcmp(MATERIAL_i{j}, MATERIAL(k))
						ro = DENSITY_BASE(k,T);
					end 
				end	
			end	 
			
		elseif( isnumeric(MATERIAL_i) ) %when material is defined as number
		   	if( MATERIAL_i(j) == MATERIAL_NO(1))  %Special case CRP - two materials - depend on reactor type
				if(strcmp(TYPE,'BWR'))     
					ro = DENSITY_BASE(15,T);   %B4C for BWRs %ro = rho_B4C(T);  
				elseif(strcmp(TYPE,'PWR'))
					ro = DENSITY_BASE(16,T);  %AIC for PWRs 	%ro = rho_AIC(T);  
				else 
					error('ERROR #5 (rho_mixMAT): No such reactor type avaiable');
				end
            else
				for k=2:numel(MATERIAL_NO)
					if (MATERIAL_i(j) == MATERIAL_NO(k))
						ro = DENSITY_BASE(k,T); %tutaj moze byc error z temperatura
					end 
				end	
			end	 
		end      
		if(ro < 0) error('ERROR #6 (rho_mixMAT):There is no such material - check input MATERIAL vector'); end
		   
		rho_i(j) = ro;  %density of the i-th material
    end
% GDZIES POWYZEJ eps_a sie pojawia
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 % RESULT - Density calculations
 
% Hull and AtomFrac models demands mole fractions
if( or(or(strcmp('Hull',rho_model),(rho_model==1)),or(strcmp(rho_model,'AtomFrac'),(rho_model==3)))   ) 	
	if(strcmp(x_mode,'wf'))
		%A_mixMAT(x_i,MATERIAL_i,x_mode,A_mode)
		[A_mix, A_i]=A_mixMAT(x_i,MATERIAL_i,'wf',A_model);
		af_i= wf2af(x_i,A_i,1);
	elseif(strcmp(x_mode,'af'))
		af_i=x_i;
	end
	
	rho = rho_mix(af_i,rho_i,rho_model); %Run standard rho_mix calculation model

% When MassFrac model is applied it demands mass fractions	
elseif(or((strcmp(rho_model,'MassFrac')),(rho_model==2)))
	if(strcmp(x_mode,'wf'))
		wf_i=x_i;
	elseif(strcmp(x_mode,'af'))
		[A_mix, A_i]=A_mixMAT(x_i,MATERIAL_i,'af',A_model);
		wf_i= af2wf(x_i,A_i,1);
	end


	
    rho = rho_mix(wf_i,rho_i,rho_model);  %Run standard rho_mix calculation model
	
else
	error('ERROR #7 (rho_mixMAT): No such rho model');
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUT
if(flag_output == 1)
%x_i,MATERIAL_i,T,x_mode,rho_model,A_model)
    disp(['INFO (rho_mixMAT): ']);
    disp(['Fractions [-]:           	', mat2str(x_i)]);
    disp(['Materials: 					']); disp(MATERIAL_i);   %  mat2str(cell2mat(MATERIAL_i))])
	%disp(['Materials: 					', MATERIAL_i']); 
    disp(['Pure densities [g/cm3]:  	', mat2str(rho_i)]);
    disp(['Temperature [K]:         	', mat2str(Temperature)]);
    disp(['Input fractions type:    	', mat2str(x_mode)]);
    disp(['Density mixture model:   	', mat2str(rho_model)]);
	disp(['Atomic mass mixture model: 	', mat2str(A_model)]);
    disp(['Mixture Density [g/cm3]:   	', mat2str(rho)]);
end


 
 
 
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
