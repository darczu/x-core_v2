%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% X-Core v2.0 developed by Piotr Darnowski - all rights reserved
% Stainless (and Carbon) Steel Density  - combined script including CS and SS
% Rev0 20-07-2020, Rev1 21-02-2021, Rev2 16-06-2023
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: T				      - temperature, [K]
% 2: mode			      - mode, see below - options 
% 3: p				      - pressure, [Pa]  
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: rho			      - density, [g/cc]
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% T                 		- 300 K
% mode              		- 'Default'
% p 						- 1e5 Pa
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% {1, 'Kolev_Iron', 'Leibowitz', 'Kolev1'}   - based on Kolev book
% {2, 'Kolev_Ferrite', 'Richter_Ferrite', 'Kolev2'} 
% {3, 'Kolev_Austenite', 'Richter_Austenite', 'Kolev3'}
% {4, 'Powers', 'Powers_SS304'}
% {5, 'MELCOR', 'Default'}   						   % MELCOR Manual based [4]
% {6, 'MELCOR2', 'MELCOR_SS304','SS304'}   % MELCOR SS-304 variable
% {7, 'EPR_liner'}
% {8, 'EPR_struct'}
% {9, 'SS316L'}
% {10, 'NARSIS_1', 'NARSIS_SA-533b'}
% {11, 'NARSIS_2', 'NARSIS_SA-508'}
% {12, 'NARSIS_3', 'NARSIS_SS-304'}
% {13, 'NARSIS_4', 'NARSIS_SS-316'}
% {14, 'MELCOR_CS1', 'CS1', 'Carbon_Steel1'}		
% {15, 'NARSIS_CS2', 'CS2', 'Carbon_Steel2'}
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% rho_SS(500)
% rho_SS(500,'Powers')
% rho_SS(600.0,3)
% rho_SS(linspace(300,700,10),6)
% rho_SS(linspace(300,700,10),14)
% REFERENCES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% see REFERENCES.m 
% 'Powers' or 'SS304'		% Powers [1]
% 'EPR_liner'				% UK EPR Steel for containment liner based on [22]
% 'EPR_struct'				% UK EPR Steel for containment strucutres based on [22]
% 'SS316L'					% SS316L steel taken from A. Kontautas et. al. Nuc Eng. and Des. 239 2009 1267-1274
% 'Kolev_Iron' or 'Leibowitz' or 'Kolev1' % Iron taken from Kolev [10]
% 'Kolev_Ferrite' or 'Richter_Ferrite' or 'Kolev2' % Ferrite steel taken from Kolev [10]
% 'Kolev_Austenite' or 'Richter_Austenite' or 'Kolev2' % Aust. steel taken from Kolev [10]
%	case {12, 'MELCOR_CS', 'CS1', 'Carbon_Steel1'}
%			mode = 'MELCOR';
%			rho = rho_CS(T,mode,p);
%	case {13, 'NARSIS_CS', 'CS2', 'Carbon_Steel2'}
% COMMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Prev. only Stanless Steel. Now it is general function for all steels
% Merged rho_SS, rho_SS304, rho_SS316, it uses rho_CS
% rho_CS still functional
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function rho = rho_SS(T,mode,p)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function rho = rho_SS(T,mode,p)

	import XCore.AtomicDensityPackage.*
	import XCore.Materials.Density.*

% Handle to function
% rho_liq_pT=@XCore.Materials.Density.density_liquid_pT; 
rho_liq_pT=@density_liquid_pT; 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
T_melt = 1700.0;  %Kolev & MELCOR
T_boil = 3086.75;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(nargin <1)
	T =300.0;  % Default Temperature 
end
NT = numel(T);
if(nargin < 2)
	mode ='MELCOR';  %Default 'Kolev' ; 'Powers'
end
if(nargin < 3)
	p = 1e5;
end

NT = numel(T); 
rho = zeros(1,NT);

%if(T(i)>T_boil)  flag_boiling = 1; end 
%if(T>T_boil)  flag_boiling = 1; end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch mode
	case {1, 'Kolev_Iron', 'Leibowitz', 'Kolev1'}
		
	%if(strcmp(mode,'Kolev_Iron') | strcmp(mode,'Leibowitz') | strcmp(mode,'Kolev1'))
		% KOLEV DATABASE  - Iron Density
		%[id, T0, rho0, beta, kappa, cpliq,INFO ] = MAT_MELT_DATABASE(5);
		for i=1:NT
			%if(T(i)<T0)     	
				rho(i) = 8084. + (-4.209e-1).*T(i) + (-3.894e-5).*T(i).^2;  %[10]  based on Leibowitz et al. (1976).
				rho(i)=rho(i)/1000;
			%else
			%	rho(i) = rho_liq_pT(T(i),T0,p,beta,kappa,rho0)/1000;
			%end
		end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% ferrite steel 22NiMoCr 37
	%elseif(strcmp(mode,'Kolev_Ferrite') | strcmp(mode,'Richter_Ferrite') | strcmp(mode,'Kolev2'))
	case {2, 'Kolev_Ferrite', 'Richter_Ferrite', 'Kolev2'}	

		[ ident, T0, rho0, rho0beta, beta, kappa, cpliq, INFO ] = MAT_MELT_DATABASE(5);
		for i=1:NT
			if(T(i)<T0)     	
				rho(i) = 7915.4444 + (-0.22109).*T(i) + (-1.00051e-4).*T(i).^2;  %[10] 
				rho(i)=rho(i)/1000;
			else
			% KOLEV DATABASE for liquid density
		
				%rho(i) = rho_liq_pT(T(i),T0,p,beta,kappa,rho0)/1000;
				rho(i) = rho_liq_pT(T(i),T0,p,beta,kappa,rho0)/1000;
			end

		end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%austenitesteel X10 10 CrNiNb 18 9 
	%elseif(strcmp(mode,'Kolev_Austenite') | strcmp(mode,'Richter_Austenite') | strcmp(mode,'Kolev3'))
	case {3, 'Kolev_Austenite', 'Richter_Austenite', 'Kolev3'}

	[ ident, T0, rho0, rho0beta, beta, kappa, cpliq, INFO ]= MAT_MELT_DATABASE(5);
		for i=1:NT
			if(T(i)<T0)     	
				rho(i) = 8031.04773 + (-0.242608).*T(i) + (-2.13034e-5).*T(i).^2;  %[10] 
				rho(i)=rho(i)./1000;
			else
			% KOLEV DATABASE for liquid density
				rho(i) = rho_liq_pT(T(i),T0,p,beta,kappa,rho0)./1000;
				
			end
		end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%elseif(strcmp(mode,'Powers') | strcmp(mode,'SS304') )	
	case {4, 'Powers', 'Powers_SS304'}
		for i=1:NT	
			rho(i)= 7.5512 - (0.11167e-3).*T(i) - (0.15063e-6).*T(i).^2;  %[1]	SS304
		end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% elseif(strcmp(mode,'MELCOR'))	
	% MELCOR default Steel is SS type 347
	case {5, 'MELCOR'}
		for i=1:NT
			rho(i) = 7.930;  %Ref [4]  MELCOR MANUAL - single constant value
		end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% MELCOR SS-304 Tabular 
	case {6, 'MELCOR2', 'MELCOR_SS304','SS304'}
		T1 = [
			273.15 
			323.15 
			373.15 
			423.15 
			473.15 
			523.15 
			573.15 
			623.15 
			673.15 
			723.15 
			773.15 
			823.15 
			873.15 
			923.15 
			973.15 
			1023.15 
			1073.15 
			1123.15 
			1173.15 
			1223.15 
			1273.15 
			1373.15 
			1473.15 
			1573.15 
			1673.15 
			1700.00 
			1700.01 
			1800.00 
			1900.00 
			2000.00 
			2100.00 
			2200.00 
			2300.00 
			2400.00 
			2500.00 
			2600.00 
			2700.00 
			2800.00 
			2900.00 
			3000.00 
			];

		rho1 = [
			8025.00 
			8003.00 
			7981.00 
			7958.00 
			7936.00 
			7914.00 
			7891.00 
			7869.00 
			7847.00 
			7824.00 
			7802.00 
			7780.00 
			7757.00 
			7735.00 
			7713.00 
			7690.00 
			7668.00 
			7646.00 
			7623.00 
			7601.00 
			7579.00 
			7534.00 
			7489.00 
			7445.00 
			7400.00 
			7388.00 
			6926.00 
			6862.00 
			6785.00 
			6725.00 
			6652.00 
			6576.00 
			6498.00 
			6416.00 
			6331.00 
			6243.00 
			6152.00 
			6058.00 
			5961.00 
			5861.00 
			];

		rho1 = rho1./1000;
		for i=1:NT
			rho(i) = interp1(T1,rho1,T(i));
		end	
		%if(T>T_boil)  flag_boiling = 1; end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%elseif(strcmp(mode,'EPR_liner'))	
	case {7, 'EPR_liner'}
	% Containment Liner Austenistic Steel used in EPR
	% Ref. [22] UK EPR PCSR Ch 16.2 for EPR Containment Analysis
		rho(1:NT) = 7.92;   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%elseif(strcmp(mode,'EPR_struct'))	
	case {8, 'EPR_struct'}
	% Containment Ferritic Steel for containemnt strucutres used in EPR
	% Ref. [22] UK EPR PCSR Ch 16.2 for EPR Containment Analysis
		rho(1:NT) = 7.85;   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%elseif(strcmp(mode,'SS316L'))
	case {9, 'SS316L'}
	% Stainles Steel AISI 316L
	%Ref: A. Kontautas et. al. Nuc Eng. and Des. 239 2009 1267-1274
		rho(1:NT) = 7.850; 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
	case {10, 'NARSIS_1', 'NARSIS_SA-533b'}
	% NARSIS Gen II Plant SA-533b
		rho(1:NT) = 7.850; 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	case {11, 'NARSIS_2', 'NARSIS_SA-508'}
	% NARSIS Gen II Plant SA-508 Cl. 3a
		%rho(1:NT) = 7.850; 
		rho(:) = 7.850; 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	case {12, 'NARSIS_3', 'NARSIS_SS-304'}
	% NARSIS Gen II Plant SS-304
		%rho(1:NT) = 7.9; 
		rho(:) = 7.9; 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	case {13, 'NARSIS_4', 'NARSIS_SS-316'}
	% NARSIS Gen II Plant SS-316
		%rho(1:NT) = 8.2; 
		rho(:) = 8.2; 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% Carbon Steel was added as option - REMOVED
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% Carbon Steel from MELCOR default
	case {14, 'MELCOR_CS1', 'CS1', 'Carbon_Steel1'}
			mode = 'MELCOR';
			rho = rho_CS(T,mode,p);
			
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		
	% Carbon Steel 15Mo3 used in Forver Experiment as described in exp. report
	case {15, 'NARSIS_CS2', 'CS2', 'Carbon_Steel2'}
			mode = 'NARSIS_Forever_1';
			rho = rho_CS(T,mode,p);
						
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	otherwise
		error('no such options');
	end  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%if(flag_boiling == 1)    disp('WARNING (rho_SS): Steel boiling temperature ~3086.75K exceeded'); end
	% WE need SS304, SA533, SA508