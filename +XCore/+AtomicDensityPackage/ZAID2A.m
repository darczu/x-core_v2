% X-Core v2.0 developed by Piotr Darnowski - all rights reserved
% Rev 03-04-2017
%ZAID -> MASS NUMBER
%Function to re-calculate ZAID number into mass number
%It is simiplification
% Calculates A knowing ZAID
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ATOMIC_MASS = ZAID2A(INPUT_ZAID)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    import XCore.AtomicDensityPackage.*

    format long g
%tic
%Load XSDATA Atomic masses and ZAIDs
%load XSDATA.mat;  %XSDATA = [ZAID, MASS]
%load XCore.AtomicDensityPackage.XSDATA.mat;

load +XCore/+AtomicDensityPackage/XSDATA.mat

%Z = floor(ZAID./1000);
%A = ZAID - 1000*Z;


	%if(A>299)   %META STABLE ISTOTOPE
	%			if(MASS>199)
	%				A = A-100;
	%			elseif(MASS>99)
	%				A = A-200;
	%			else		
	%			 error('No such isotope');
	%			end
	%end


	for i=1:numel(INPUT_ZAID)
		ATOMIC_MASS(i) = sum((XSDATA(:,1) == INPUT_ZAID(i)).*XSDATA(:,2));
	end

	ATOMIC_MASS = ATOMIC_MASS';
	%toc

%{
% For natural mixtures

if( sum(A == 0) > 0)
    
    n = numel(ZAID);

	
	
	
    for i=1:n
        ZAID1 = ZAID(i);
        A1 = A(i);
        if(A1 == 0)

            if (ZAID1 == 6000)   %C
                A1 = 12.001096;

            elseif (ZAID1 == 24000) %Cr
                A1 = 51.995870 ;
            elseif (ZAID1 == 26000)  %Fe
                A1 = 55.846754;    
            elseif (ZAID1 == 28000)   %Ni
                A1 = 58.687961;         
            elseif (ZAID1 == 30000)  %Zn
                A1 =  65.395784;
            elseif(ZAID1 == 40000)    %Zr
                A1 = 91.219622; 
            elseif(A==0)
                error('Natural isotope mixture. No result in the batabase!!!');
                A1 = 'NaN';
            end
           A(i) = A1; 
        end
    end
end 
%}


 %   'mActO2'
 %   'mFP'
 %   'mZr'
 %   'mZrO2'
 %   'mSS'
 %   'mSSOX'
 %   'mB4C'
 %   'mINC'