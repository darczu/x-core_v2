% X-Core v2.0 developed by Piotr Darnowski - all rights reserved
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% simple example how to use version 2
% add path
% addpath(genpath('./')); % Add main folder all subfolders
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
addpath(genpath('./')); % Add main folder all subfolders
%

help XCore.materials.Density.rho_MOX

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('MOX fuel density')
XCore.Materials.Density.rho_MOX(400.0,0.05,0.05)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Graphite specific heat capacity')
XCore.Materials.SpecificHeatCapacity.cp_GRP(1000)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Re-calculates density with correction given by thermal strain')
strain = 0.03; density = 5000;
XCore.Materials.Density.density_thermal_expansion(density, strain)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Stainless steel density vs temperature')
T = linspace(300,2500,10); % Temperatures
fun = @XCore.Materials.Density.rho_SS; %  rho_SS function handle

rho1 = fun(T, 'MELCOR_SS304');
rho2 = fun(T, 'Kolev_Iron');
rho3 = fun(T, 'Kolev_Ferrite');
rho4 = fun(T, 'Kolev_Austenite');
rho5 = fun(T, 'Powers_SS304');
rho = [rho1; rho2; rho3; rho4; rho5]
figure(1); plot(T,rho,'o'); grid on;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



