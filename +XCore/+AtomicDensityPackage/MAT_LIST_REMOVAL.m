% X-Core v2.0 developed by Piotr Darnowski - all rights reserved
% Rev 09-08-2017 - NOT USED TOO COMPLEX
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MATERIAL LIST REMOVAL -> takes vector and removes zero entries
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 	vector		- input vector, mass, etc.
% 	vector_name	- input vector material names
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  	new_vector 	- vector with size equal to the input material database size
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% vector_name = {'UO2' 'U' 'SS' 'SSOX'}; vector = [1000 2000 3000 4000];
% [new_vector]=MAT_LIST_TRANSFORM(vector, vector_name)
%
% EXAMPLE 1: 
% vector_name = {'UO2' 'SS' 'SSO' 'INC' 'ZRO2'}; vector_check = [0 1 1 1 0];  vector_main = rand(5,1)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [new_vector, new_name]=MAT_LIST_REMOVAL(vector_main, vector_check, vector_name)
    import XCore.AtomicDensityPackage.*
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dummy1 = vector_main(vector_check ~=0);
dummy2 = vector_name(vector_check ~=0);

new_vector = dummy1;
new_name  = dummy2;
error('NOT ACTIVE');

% REMOVAL OF ZERO ENTRIES
%	dummy1 = mass_i(mass_i ~=0);
%	dummy2 = mat_i(mass_i~=0);
%	dummy3 = Temperature;
%	if(numel(Temperature)>1) dummy3 = Temperature(mass_i~=0); end	
%	mass_i = dummy1; 
%	mat_i = dummy2; 
%	Temperature = dummy3;


