Rayleigh number	Ra	
R
a
�
=
�
�
�
�
(
�
�
−
�
∞
)
�
3
\mathrm{Ra}_{x} = \frac{g \beta} {\nu \alpha} (T_s - T_\infin) x^3 	heat transfer (buoyancy versus viscous forces in free convection)