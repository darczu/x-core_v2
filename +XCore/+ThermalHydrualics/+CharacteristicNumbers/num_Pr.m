
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% X-Core v2.0 developed by Piotr Darnowski - all rights reserved
% Prandtl number caclulation, Rev0 30-06-2023
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: mode
% 2: mode dependent inputs
% --------------------------------------------------------------------------------
% nu = mi/rho           - kinematic viscosity [ m2/s] or [N m s /kg] or [J s / kg]
% mi                    - dynamics viscosity  [Pa s] or [N s / m2] or [kg /m/s]
% k                     - thermal conductivity [W/m/K]
% cp                    - specific heat capacity [J/kg/K]    
% alpha                 - thermal diffusivity [m2/s]
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: Pr      - Prandtl number [-]		
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% {1, 'cp*mu/k', 'Default'}  
%        cp = in{1};
%        mi = in{2};
%        k  = in{3};
% --------------------------------------------------------------------------------
% {2, 'nu/alpha', 'kinematic viscosity-to-thermal diffusivity'}
%        nu    = in{1};  
%        alpha = in{2}; 
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% REFERENCES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% https://www.nuclear-power.com/nuclear-engineering/heat-transfer/introduction-to-heat-transfer/characteristic-numbers/
% Prandtl number	Pr	= \mathrm{Pr} = \frac{\nu}{\alpha}  = \frac{c_p \mu}{k}	heat transfer (ratio of viscous diffusion rate over thermal diffusion rate)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [Pr] = num_Pr(mode, varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
num  = numel(varargin);
in = varargin;
if (num < 3 | num > 4)
    error('Some error with input');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch mode
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case{1, 'cp*mu/k', 'Default'}    
        cp = in{1};
        mi = in{2};
        k  = in{3};

        Pr(:) = cp.*mi./k;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case {2, 'nu/alpha', 'kinematic viscosity-to-thermal diffusivity'}
        nu    = in{1};  
        alpha = in{2}; 

        Pr(:) = nu./alpha;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    otherwise
        error('No such option.');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%