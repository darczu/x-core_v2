%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% X-Core v2.0 developed by Piotr Darnowski - all rights reserved
% Nusselt number correlations for liquid metals 
% old function created ~2017, Rev1 18-06-2023
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: Pe						- Peclet Number [-]
% 2: mode					- options
% 3: s 						- pitch-to-diameter [-]
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: Nu						- Nusselt Number [-]			      
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: mode ='Default';
% 2: s = 1.2;                  		
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  {1, 'sqr', 'square'}   			% tight square lattice
%  {2, 'tri', 'triangular'}			% triangular lattice by Graber & Rieger
%  {3, 'pure'}
%  {4, 'normal', 'Default'}
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%REFERENCES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% see REFERENCES.m 
% Based on correlation given in Prof. Anglart lectures
% COMMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function [Nu] = num_Nu_metal(Pe,mode,s)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [Nu] = num_Nu_metal(Pe,mode,s)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(nargin < 1)
	Pe = 10.0; 
end
if(nargin < 2)
	mode ='Default';  
end
if(nargin < 3)
	s = 1.2;
end

NT = numel(Pe); 
Nu = zeros(1,NT);
if NT > 1
	error('No vectors allowed for criterial numbers!!!');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch mode
	case {1, 'sqr', 'square'}
		Nu(:) = 0.48 + 0.0133.*Pe.^(0.70);  %tight square lattice

	case {2, 'tri', 'triangular'}
		Nu(:) = 0.25 + 6.2.*s + (0.32.*s-0.07).*(Pe.^(0.8-0.024.*s)); 
		%triangular lattice by Graber & Rieger

	case {3, 'pure'}
		Nu(:) = 5.0 + 0.025.*Pe.^(0.8);

	case {4, 'normal', 'Default'}
		Nu(:) = 3.3 + 0.014.*Pe.^(0.8);

	otherwise
		error('ERROR#1 (fun_Nu): Nusselt Calculation Option not available !!!');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

