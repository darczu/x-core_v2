%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% X-Core v2.0 developed by Piotr Darnowski - all rights reserved
% Rev0 28-03-2017, Rev1 21-02-2021, Rev2 16-06-2023
% Fission Products Oxide Simulant/Dummy 
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: T				- temperature, [K]
% 4: p				- pressure, [Pa]     
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: rho			- density, [g/cc]
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% p 				- 1E5
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% REFERENCES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% see REFERENCES.m file
% [1] - Density Stratification and Fission Product Partitioning in Molten Corium Phases
% [2] - MATPRO RELAP5 3D Vol 4
% [3] - Carbajo  A review of the thermophysical proporties of MOX and UO2 fuels.
% [4] - MELCOR Reference Manual
% [5] - PNNL-15870rev1
% COMMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function out = rho_FPO2(T,p)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function out = rho_FPO2(T,p)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(nargin < 1)
  T =300.0;  % Default Temperature 
end
if(nargin < 2)
 p = 1e5;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ziroconium di-Oxide density
for i=1:numel(T)
	rho(i) = 5.863./(1+  T(i).*7.0e-5);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
out = rho;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Iron density
%rho = 8.612 - (0.883e-3).*T;
%%Zirconium density
%rho = 6.61 - (0.38e-3).*T;
%Steel
%rho = 7.930;

%{
  Rho(1)= 2.46;
    Rho(2)= 2.46;
    Rho(3)= 2.267; %grafit   %amorficzny wegiel 1.8-2.1
    Rho(4)= 1.141;
    Rho(5)= 0.927;  % S�d w stanie ciek�ym w punkcie topnienia
    Rho(6)= 7.19;
    Rho(7)= 7.874;
    Rho(8)= 8.908;
    Rho(9)= 10.28;
    Rho(10)=11.7;
        1 - Boron 
        2 - Boron-10
        3 - Carbon 
        4 - Oxygen
        5 - Sodium
        6 - Chormium
        7 - Iron
        8 - Nickel
        9 - Molybdenum
        10 - Thorium 232

%}