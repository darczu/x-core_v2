%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% X-Core v2.0 developed by Piotr Darnowski - all rights reserved
% Mixed DiOxide Fuel Density PuO2 + UO2, Rev0 16-03-2019, Rev1 16-06-2023
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: T				- temperature, [K]
% 2: y				- fraction of plutonium, [%/100], it is mole fraction (atomic)
% 3: porosity		- porosity, [%/100] 
% 4: p				- pressure, [Pa]  - THERE IS NO PRESSURE DEP.
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: rho			- density, [g/cc]
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% porosity 			- 0.0  
% p 				- 1E5
% y					- 0.05  == 5% of Plutonium
% T					- 300
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% No modes - correlation is based on Carbajo [3] (CONFIRM!)
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% rho = rho_MOX()
% rho = rho_MOX(400.0)
% rho = rho_MOX(400.0,0.05)
% rho = rho_MOX(400.0,0.05,0.05)
%REFERENCES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% see REFERENCES.m file
% COMMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ISSUE: Review recommended, confirm source
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function out = rho_MOX(T,y,porosity,p)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function rho = rho_MOX(T,y,porosity,p)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% Defaults
	if(nargin < 1) 
		T = 300.0;
	end
	if(nargin < 2)
		y=0.05;
	end
	if(nargin < 3)
		porosity = 0.0;
	elseif( or(porosity > 1.0,porosity < 0.0 ) )
		error('Porosity has to be: 0.0<=p<=1.0');
	end	
	if(nargin < 4)
		p = 1e5;
	end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% Solid Solution of the Pu and U  % y - mole fraction of the PuO2 
	if(or(y>1.0,y<0.0))
		error('ERROR(#1):   0.0 <= y <= 1.0');
	end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	rho0 = 10970 + 490.*y;
	%rho0_PuO2 = 11.460; % +-80
	%rho0_UO2 = 10.970 

	for i=1:numel(T)
		if (T(i)< 973)
			rho(i) = rho0.*( (  9.9734e-1  + T(i).*9.802e-6 +  (-2.705e-10).*(T(i).^2) +  (4.391e-13).*(T(i).^3)).^(-3)); %[kg/m3]
		elseif(and(T(i)< 3120,T(i)>=973))
			rho(i) = rho0.*( (  9.9672e-1  + T(i).*1.179e-5 +  (-2.429e-9).*(T(i).^2) + (1.219e-12).*(T(i).^3)).^(-3)); %[kg/m3]
		else	%fixed melting point
			rho(i) = 8860 - (0.9285).*(T(i)-3120);  %[3]
			
				%BOTH FOR liquid UO2 and PuO2
				%rho = 8.74 - (9.18e-4).*(T-3120);  %REF ?	
		end
		
		if(T(i)>4500)
			disp('WARNING: Temperature beyond liquid uranium density correlation validity range 3120K<T<4500K');
		end
	end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
rho = rho./1000;  %kg/m3 -> g/cm3
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
rho = rho.*(1-porosity);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


