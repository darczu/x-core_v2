% X-Core v2.0 developed by Piotr Darnowski - all rights reserved
% Rev 10-08-2017
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function witch calls materials database and returns material proporties
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mat_number 	- material number - as those in MAT_DATABASE_INPUT
% mat_mode 		- material mode 1 - special materials; 0 - MELCOR materials; 2 - additional materials
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COMP			- Composition
% ZAID			- ZAID number		
% NAME			- material name
% SUMC			- Composition sum
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mat_mode 				1 - special materials 
%						0 - MELCOR materials and SARCAM model materials
%						2 - additional materials 
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EXAMPLE 1: [ZAID, COMP]=MAT_DATABASE(3, 0)
% EXAMPLE 2: [COMP,ZAID, NAME, UNIT, SUMC] = MAT_DATABASE('UO2')
% EXAMPLE 3: [COMP,ZAID, NAME, UNIT, SUMC] = MAT_DATABASE({'UO2'})
% EXAMPLE 4: [COMP,ZAID, NAME, UNIT, SUMC] = MAT_DATABASE(['UO2'])
% DO NOT RUN IT THIS WAY:  MAT_DATABASE(3, 0)  - use all output variables
% EXAMPLE 5: [COMP,ZAID, NAME, UNIT, SUMC] = MAT_DATABASE(3)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [COMP, ZAID, NAME, UNIT, SUMC] = MAT_DATABASE(mat_number, mat_mode)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
import XCore.AtomicDensityPackage.*

if(nargin < 2)
	mat_mode = 0; %DEFAULT MODE	
end

% IINPUT WITH NAMES
if(mat_mode == 0)  %ONLY FOR MELCOR MATERIALS
	[MATERIAL, MATERIAL_NO, TYPE]=MAT_LIST();
	
	if( iscell(mat_number) || isstring(mat_number) || ischar(mat_number))		
			mat_number =   sum( strcmp(mat_number,MATERIAL).*MATERIAL_NO);
	elseif(~sum(mat_number == MATERIAL_NO))
			error('ERROR #1 (MAT_DATABASE): NO SUCH MATERIAL');
	end
else
	if( iscell(mat_number) || isstring(mat_number) || ischar(mat_number))		
		error('ERROR #2 (MAT_DATABASE): String, Cell, Char type input only for mode 0')
	end
end




%flag_k = 0; flag_m =0; flag_n =0;

%run MAT_DATABASE_INPUT.m; %Load materials 
run XCore.AtomicDensityPackage.MAT_DATABASE_INPUT.m

switch mat_mode
	case 0,
% 2023 %	    disp('INFO #1 (MAT_DATABASE): 	MATERIAL MODE 0 - MELCOR materials');
		%flag_m = 1;
		%run MAT_DATABASE_INPUT.m; %Load materials 
		ZAID = mat0(mat_number).zaid;
		COMP = mat0(mat_number).comp;
		NAME = mat0(mat_number).name;
		UNIT = mat0(mat_number).unitc;
		SUMC = mat0(mat_number).sumc;
% 2023 %		disp(['INFO (MAT_DATABASE):  ', NAME]);
		%MATSIZE = size_m;
	case 1,
% 2023 %	    disp('INFO #2 (MAT_DATABASE):   MATERIAL MODE 1 - SPECIAL materials');
		%flag_k = 1;
		%run MAT_DATABASE_INPUT.m; %Load materials 
		ZAID = mat1(mat_number).zaid;
		COMP = mat1(mat_number).comp;
		NAME = mat1(mat_number).name;
		UNIT = mat1(mat_number).unitc;
		SUMC = mat1(mat_number).sumc;
		disp(['INFO (MAT_DATABASE) - MATERIAL:  ', NAME]);
		%MATSIZE = size_k;
	case 2,
% 2023 %		disp('INFO #3 (MAT_DATABASE)	MATERIAL MODE: 2 - USER materials');
		%flag_n = 1;
		%run MAT_DATABASE_INPUT.m; %Load materials 
		ZAID = mat2(mat_number).zaid;
		COMP = mat2(mat_number).comp;
		NAME = mat2(mat_number).name;
		%SUMC = mat0(mat_number).sumc;
		UNIT = [];
% 2023 %		disp(['INFO (MAT_DATABASE):  ', NAME]);
		%MATSIZE = size_m;
	otherwise
		error('ERROR #2 (MAT_DATABASE): NO SUCH MODE');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

