# SHORT HISTORY
X-Core Nuclear Engineering Toolbox MATLAB Toolbox/Package with material properties, functions, 
geometry and other functions useful for Nuclear Engineering.
Previously called CETbox. Bascially it is collections of matlab scripts which I used in my research.
Developed by Piotr Darnowski in 2016/2017/2018/2019/2020/2021/2022/2023 
Several functions were developed in the framework of the NARSIS Horizon 2020 Research Project Funded by EU at Warsaw University of Technology, Institute of Heat Engineering, 
The XCore is partially based on subroutines developed for SARCAM code developed in 2015-2018.
Initial intention was to create package allowing to calculate material propoties, i.e. atomic number densities for different corium mixture and crticiality calculations. 
Package is under constant development. I hope to add object oriented functionalities but it is matter of future.


# HISTORY - DEVELOPMENT LOG
2023-06-02 - work on the new version begin.
2023-06-05 - material libs transffered. 
2023-06-06 - added ReadMe, license, etc.
2023-06-11 - added public folder, added examples with testing
2023-06-12 - various modification
2023-06-13 - AtomicDensity package moved to public folder. To be reconsidered in the future.
2023-06-14 - modifications, AtomicDensityPackage moved
2023-06-16 - changed in densities and also changed descriptions of various functions
