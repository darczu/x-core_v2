%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% X-Core v2.0 developed by Piotr Darnowski - all rights reserved
% Rev-1 01-10-2018
% T [K] and rho [g/cc]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: T				      - temperature, [K]
% 2: p				      - pressure, [Pa]
% 3: mode			      - mode, see below - options 
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: rho			      - density, [g/cc]
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% T                 - 300 K
% p                 - 1e5 Pa
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1:
% 2
% 3: 
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 
%REFERENCES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% see REFERENCES.m file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function out = cp_SS304(T,option)


if(nargin < 2)
 option ='MELCOR';  %Default 'MELCOR'
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(strcmp(option,'MELCOR'))
% Reference - MELCOR Reference Manual [4]
	T_MELCOR = [
	0.0	%added
300.00 
400.00 
500.00 
600.00 
700.00 
800.00 
900.00 
1000.00 
1100.00 
1200.00 
1300.00 
1400.00 
1500.00 
1600.00 
1700.00 
1700.01 
1800.00 
3000.00 
5000.00	%added
	];

	Cp_MELCOR = [
513.20  %added
513.20 
526.90 
540.40 
553.90 
567.50 
581.10 
594.70 
608.20 
621.80 
635.40 
648.90 
662.50 
676.80 
689.60 
703.20 
800.10 
800.10 
800.10 
800.10  %added
	];

	% Linear inteprolation %vq = interp1(x,y,xi)
	for i=1:numel(T)
		out(i) = interp1(T_MELCOR,Cp_MELCOR,T(i));
		
		% No extrapolation is allowed
		if(T(i)>T_MELCOR(end))
			error('ERROR(#1): Material temperature above allowed,  0<T<5000');
		elseif (T(i)<T_MELCOR(1))
			error('ERROR(#2): Material temperature below allowed,  0<T<5000');
		end	
	end










%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
elseif(strcmp(option,'Kolev'))	








%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
else
	error('no such options');
end 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 