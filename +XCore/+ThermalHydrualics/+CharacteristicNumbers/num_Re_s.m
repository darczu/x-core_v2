% Review 15-10-2018,  Written in 2014
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MELCOR based Reynolds Number Calculations - for two phase flows
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%A - athmosphere P - pool
% a				- Void fraction
% rho_A, rho_P 	- Density
% v_A, v_P 		- Velocity
% nu_m			- Mixture dynamic viscosity
% Ds			- Diameter Hydraulic 
% F_j			- Open Fraction
% A_s			- Segment Area
% A_j			- Nominal Area
% y = Re_s(a,rho_A,v_A,rho_P,v_P,D_s,nu_m,F_j,A_j,A_s)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function y = Num_Re_s(a,rho_A,v_A,rho_P,v_P,D_s,nu_m,F_j,A_j,A_s)


y = a.*rho_A.*abs(v_A) + (1-a).*rho_P.*abs(v_P).*D_s;
y = y./nu_m;
y = y.*F_j.*A_j./A_s;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

