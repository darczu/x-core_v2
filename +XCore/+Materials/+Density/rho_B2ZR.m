%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% X-Core v2.0 developed by Piotr Darnowski - all rights reserved
% Zirconium Diboride  Rev 2 16-06-2023, Rev 1 21-02-2021, Rev 0 2018
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: T				- temperature, [K]
% 2: p				- pressure, [Pa]     
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: rho			- density, [g/cc]
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% p 				- 1E5
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% REFERENCES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% see REFERENCES.m file
% COMMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function rho = rho_B2ZR(T,p)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function rho = rho_B2ZR(T,p)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(nargin < 1)
    T =300.0;  % Default Temperature 
end
if(nargin < 2)
 p = 1e5;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for i=1:numel(T)
	rho0 = 6.05;	%assumption
	dLL0(i) = -0.135 + (3.940e-4).*T(i) + (2.390e-7).*(T(i).^2)-(3.976e-11).*(T(i).^3);
	rho(i) = rho0.*((1+ (dLL0(i))./100).^(-3)); % tak jak w innych zrodlach to raczej naprewno jest dobrze bo
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%rho = 6.09; % https://www.americanelements.com/zirconium-diboride-12045-64-6

% rho = 6.05   %Theoretical density  http://onlinelibrary.wiley.com/doi/10.1111/jace.12893/epdf
%rho = 5.97    %Bulk density  http://onlinelibrary.wiley.com/doi/10.1111/jace.12893/epdf


%rho = rho0.*((1+ (dLL0)./100).^(3)); % NIE PODOBA MI SIE TA KORELACJA
%moim zdaniem powinono byc

% rho =m/V  rho0=m/V0 V=L*L*L  V0 = L0^3   rho=rho0*(L0/L)^3 = rho0*(L/L0)^-3 = rho0*(1+dL/L0)^-3
% w [2] jest ze rho=rho0*(1-3*epsilon)  gdzie epsilon to linear thermal expansion strain. To powinno malec a rosnie


% Exact Mass A = 111.923315 g/mol
% Melting 3246 °C
% Molecular weight 112.8
% Touloukian
% http://scholarsmine.mst.edu/cgi/viewcontent.cgi?article=8264&context=masters_theses
% http://onlinelibrary.wiley.com/doi/10.1111/jace.12893/references
% http://onlinelibrary.wiley.com/doi/10.1111/jace.12893/epdf
% rho = rho0.*(1+ (dLL0)./100).^(3);
% dLL0 = -0.135 + (3.940e-4).*T + (2.390e-7).*(T.^2)-(3.976e-11).*(T.^3);




