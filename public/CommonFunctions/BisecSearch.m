% Not sure about Authorship - likely written by Kacper Potapczyk
% Bisection algorithm 
% Be careful - old algorithm 
% EXAMPLE
%  C0 = [0.0         , 0.0625        , 0.125       , 0.1875        , 0.25        , 0.3125          , 1.0     ];
%  x =0.2;
% BisecSearch(x, C0)
function base=BisecSearch(x, temp)

bot = 1;
top = length(temp);
cntr = 0;

while(top-bot > 1)
    mid = floor((top+bot)/2);
    if(x<temp(mid))
        top = mid;
    else
        bot = mid;
    end
    cntr = cntr + 1;
    if(cntr > 10)
        break
    end

end

base = bot;

