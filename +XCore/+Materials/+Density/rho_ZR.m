%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% X-Core v2.0 developed by Piotr Darnowski - all rights reserved
% Zirconium/Zircalloy Density  Rev0 01-07-2019, Rev1 16-06-2023, Rev2 29-06-2023
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: T				      - temperature, [K]
% 2: mode			      - mode, see below - options 
% 3: p				      - pressure, [Pa]  
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: rho			      - density, [g/cc]
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% T                 - 300 K
% mode              - 'Default'
% p 				- 1e5 Pa
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% {1,'Kolev'} 
% {2,'Powers'}
% {3,'MELCOR'}
% {4, 'NARSIS_1', 'NARSIS_690'}
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% rho_ZR(500)
% rho_ZR(1000,'Kolev')
% rho_ZR([300,500,1000,1500, 3000],1)
% rho_ZR([300,500,1000,1500, 3000],2)
% rho_ZR([300,500,1000,1500, 3000],3)
% rho_ZR([300,500,1000,1500, 3000],4)
%REFERENCES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% see REFERENCES.m 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function rho = rho_ZR(T,mode,p)

	import XCore.AtomicDensityPackage.*
	import XCore.Materials.Density.*
	rho_liq_pT = @density_liquid_pT; 

	if(nargin <1)
		T =300.0;  % Default Temperature 
	end
	if(nargin <2) 
		mode = 'MELCOR';  % Default mode
	end
	if(nargin <3)
		p = 1e5;    % Default pressure
	end

	   NT = numel(T); 
	   rho = zeros(1,NT);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	   
T_melt = 2098.0; %Kolev & MELCOR
T_boil = 4702.63; %Kolev
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch mode
	case {1,'Kolev'}
		% Kolev based Ref. [10]
	[ ident, T0, rho0, rho0beta, beta, kappa, cpliq, INFO ] = MAT_MELT_DATABASE(3);	
		rho0s = 6570.0;
		c1 = [-1.11e-3 2.325e-6 5.595e-9 -1.768e-12];
		c2 = [-7.59e-3 1.474e-6 5.140e-9 -1.559e-12];
		
		for i=1:numel(T)
				if(T(i)< 1139.0)  %valid T>298.15K
					epsilon = c1(1) + c1(2).*T(i) + c1(3).*T(i).^2 + c1(4).*T(i).^3;
					rho(i) = rho0s./((1+epsilon).^3);
					rho(i) = rho(i)./1000;
				
				elseif(and( (T(i)<T_melt),(T(i)>=1139.0) )) 
					epsilon = c2(1) + c2(2).*T(i) + c2(3).*T(i).^2 + c2(4).*T(i).^3;
					rho(i) = rho0s./((1+epsilon).^3);
					rho(i) = rho(i)./1000;
				
				elseif(T(i)>=T_melt)	
					rho(i) = rho_liq_pT(T(i),T0,p,beta,kappa,rho0)./1000;			%[10]
				end
		end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	case {2,'Powers'}
		%Powers Ref. [1]
		for i=1:numel(T)
			rho(i) = 6.61 - (0.38e-3).*T(i);  % [1]
		end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	case {3,'MELCOR'}
		%[4] - Zircaloy, MELCOR
		for i=1:numel(T)
			rho(i) = 6.5; 					
		end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
	case {4, 'NARSIS_1', 'NARSIS_690'}
		% NARSIS Gen II Plant Zirlo
		rho(:) = 6.425;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
	otherwise
		error('No such mode avilable');
	end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

