%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% X-Core v2.0 developed by Piotr Darnowski - all rights reserved
% Rev 29-09-2018
% SS304 th. cond.  T [K] and rho [g/cc]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: T				      - temperature, [K]
% 2: p				      - pressure, [Pa]
% 3: mode			      - mode, see below - options 
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: rho			      - density, [g/cc]
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% T                 - 300 K
% p                 - 1e5 Pa
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1:
% 2
% 3: 
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 
%REFERENCES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% see REFERENCES.m file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function out = k_SS304(T,option)

if(nargin < 2)
 option ='MELCOR';  %Default 'MELCOR'
end

if(strcmp(option,'MELCOR'))
% Reference - MELCOR Reference Manual [4]

	T_MELCOR = [
0.00 	%add for constant
300.00 
400.00 
500.00 
600.00 
700.00 
800.00 
900.00 
1000.00 
1100.00 
1200.00 
1300.00 
1400.00 
1500.00 
1600.00 
1700.00 
1700.01 
1800.00 
1900.00 
2000.00 
2100.00 
2200.00 
2300.00 
2400.00 
2500.00 
2600.00 
2700.00 
2800.00 
2900.00 
3000.00 
	];
	k_MELCOR = [
13.00 %added
13.00 
14.60 
16.20 
17.80 
19.40 
21.10 
22.70 
24.30 
25.90 
27.50 
29.10 
30.80 
32.40 
34.00 
35.60 
17.80
18.10 
18.50 
18.80 
19.10 
19.40 
19.80 
20.10 
20.40 
20.70 
21.10 
21.40 
21.70 
22.00  
	];

	% Linear inteprolation %vq = interp1(x,y,xi)
	for i=1:numel(T)
		out(i) = interp1(T_MELCOR,k_MELCOR,T(i),'linear','extrap');
		% Extrapolation is allowed
		%if(T(i)>T_MELCOR(end))
		%	error('ERROR(#1): Material temperature above allowed');
		if (T(i)<T_MELCOR(1))
			error('ERROR(#1): Material temperature below allowed');
		end	
	end











elseif(strcmp(option,'Kolev'))	









else
	error('no such options');
end  
