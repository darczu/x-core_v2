# NOTES AND LINKS FOR DEVELOPER

# CURRENT ISSUES #
- Decide if CommonFunction and AtomicDensityPAckage to be + type packages and private or public - as now. it will demand using import in each function but it is likely needed for toolbox...
- Apply import functionality. Done...
- Convert density to SI units. Now it is g/cc. 
- revise MAT_MELT_DATABASE.
- 

# PROJECT
- [Learn more about creating GitLab projects.](https://docs.gitlab.com/ee/gitlab-basics/create-project.html)   
- More about documentation: http://repositories.compbio.cs.cmu.edu/help/development/writing_documentation.md   
- Markdown with LateX  https://ashki23.github.io/markdown-latex.html

# DOCUMENTATION AND MARKDOWN
- More about markdown style: https://google.github.io/styleguide/docguide/style.html  
- More about documentation: http://repositories.compbio.cs.cmu.edu/help/development/writing_documentation.md   


# USING PACKAGES AND IMPORT
- More about namespaces:    
https://ch.mathworks.com/help/matlab/matlab_oop/scoping-classes-with-packages.html 

- How to do import
https://ch.mathworks.com/help/matlab/ref/import.html  

- How to use private folder and package funcrions: 
https://ch.mathworks.com/matlabcentral/answers/404078-how-to-avoid-importing-the-same-package-name-repeatedly-in-all-functions-in-this-package

- General about packages and import
https://www.scivision.dev/matlab-package-import/  

- How to load or run file when in package folder
https://ch.mathworks.com/matlabcentral/answers/374083-how-to-load-mat-file-located-in-a-package-folder-folder-matfile-mat
load +XCore/+AtomicDensityPackage/XSDATA.mat it is not elegant solution
load XCore.AtomicDensityPackage.XSDATA.mat; - does not work
run XCore.AtomicDensityPackage.MAT_DATABASE_INPUT.m  - works but not elegant

# VARIABLE INPUT TO FUNCTIONS

https://ch.mathworks.com/help/matlab/matlab_prog/support-variable-number-of-inputs.html
https://ch.mathworks.com/help/matlab/ref/varargin.html

# STRUCUTURE AS A FUNCTION OUPUT
https://ch.mathworks.com/matlabcentral/answers/339839-how-to-get-a-structure-as-an-output-from-a-function
