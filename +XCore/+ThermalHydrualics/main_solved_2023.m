%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Update 12-05-2020, Class 1, Gen IV ANS670 Laboratory - MATLAB Thermal-Hydraulics 1D Lumped-Parameter code to Calc. SFR and LFR
% Developed by Piotr Darnowski, piotr.darnowski@pw.edu.pl, Warsaw University of Technology, Institute of Heat Engineering, Faculty of Power and Aeronautical Engineering
% Apply Isolated Subchannel Model (ISM)
% 1 - Sodium Cooled;  2 - Lead Cooled
% '%% '  two percent and space defines code section
% '%'    one percent - comment
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all; clear; clc;					%close all figures; clear workspace; clean workspace
%% Input Data
H_1  = 1.0;      H_2  =  1.0;			%Active Core Length, [m]   %Semicolon ';' - do not show variable in the workspace. It allows to define multiple variable in single line
He_1 = H_1 + 2* 0.35*H_1;  				%Extrapolated core height, [m]
He_2 = H_1 + 2* 0.5*H_2;   		 		%Extrapolated core height, [m]

%% 
Dclad_1 = 8.2e-3;  Dclad_2 = 10.5e-3;	%Fuel pin outer diameter, [m] - cladding outer diameter
tclad_1 = 0.52e-3;   tclad_2  = 0.6e-3; %Cladding Thickness, [m]   
Diclad_1 = Dclad_1 - 2*tclad_1;			%Inner cladding diameter, [m]
Diclad_2 = Dclad_2 - 2*tclad_2;

%% Input Data: Assume fuel cladding contact
Dfuel_1 = Diclad_1;  	
Dfuel_2 = Diclad_2;		%Fuel pin diameter equal to inner clad diam, [m]
PD_1    = 1.15;      		PD_2    = 1.32;     	 	%Pitch-to-diameter ratio, [-]
pitch_1 = Dclad_1*PD_1;    pitch_2 = Dclad_2*PD_2;       		%Pitch = (P/D) * D, [m]
v_1    = 6.7;   			v_2     = 1.8; 			%Av. Coolant velocity. [m/s]
q0_1   = 40e3;   			q0_2    = 23.2e3;		%Peak linear power, [W/m]
Tin_1  = 400 + 273;   			Tin_2   = 400 + 273;  			%Inlet Temperature, [K]
kc_1   = 20; 				kc_2 	= 28;				%Cladding thermal conductivity [W/m/K]
h_gap = 6000; 										%Gas gap heat transfer coefficient [W/m2/K]

%% Input Data: Linear Power [W/m] distribution - Chopped cosine profile assumed
z_11 = linspace(-H_1/2,H_1/2,50);	 			%location vector, [m], with 50 values, equally spaced, beetwen -H_1/2 to H_2/2		    
q_1  = q0_1*cos(pi.*z_11./He_1);										%vector with linear power values for z_11
z_22 = linspace(-H_2/2,H_2/2,50);
q_2  = q0_2*cos(pi.*z_22./He_2);


%% Plot Linear Power Profile - an example how to prepare simple plot
figure(1);								% define figure
plot(z_11,q_1,'--m',z_22,q_2,'c-');		% plot with two data sets plot(X,Y,x,y) 
legend('power Na', 'power Pb');			% legend
ylabel('Linear Heat Rate, [W/m]','fontsize',16); % y label
xlabel('Distance, m','fontsize',16);  	% x label
grid on;								% add grid

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Task 1 - Coolant temperature distribution calculation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 1.1 - Calc. average linear power density and axial peaking factor.
qav_1 = (trapz(z_11,q_1))./H_1;
qav_2 = (trapz(z_22,q_2))./H_2;

Fa_1 = q0_1./qav_1;
Fa_2 = q0_2./qav_2;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 1.2, 1.3 - Calc. flow area, wetted perimeter (in this case it is also equal to the heated perimeter).
%Calc. channel geometry for ISM % Hint: Triangle area = sqrt(3)/4*P^2

A_1 = (sqrt(3)/4)*(pitch_1.^2) - (pi*Dclad_1^2)./8;   %flow area
A_2 = (pitch_2^2) - pi*(Dclad_2^2)/4;				  %flow area
Ph_1 = 0.5*pi*Dclad_1;               %heated/wetted perimeter
Dh_1 = 4*A_1/Ph_1;

Ph_2 = pi*Dclad_2;               %heated/wetted perimeter
Dh_2 = 4*A_2/Ph_2;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 1.4 - Calc. mass flow using inlet density and temperature.
rho_in1 = rho_Na(Tin_1);
rho_in2 = rho_Pb(Tin_2);

m_1 = A_1*v_1*rho_in1;
m_2 = A_2*v_2*rho_in2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 1.5 - Calc.  axial  temperature  profile  for  bulk  flow  of  coolant,  
% assume  constant  specific  heat capacity. Find outlet coolant temperature. Plot results.
cp_1 = Cp_Na(Tin_1)
cp_2 = Cp_Pb(Tin_2)

T_1 = Tin_1  +  ((0.5.*q0_1.*He_1)./(pi.*cp_1.*m_1)).*(  sin(pi.*z_11./He_1) + sin(pi.*H_1./(2.*He_1))   ) ;
T_2 = Tin_2  +  ((1.0.*q0_2.*He_2)./(pi.*cp_2.*m_2)).*(  sin(pi.*z_22./He_2) + sin(pi.*H_2./(2.*He_2))   ) ;

figure(2)
plot(z_11,T_1,z_22,T_2);
legend('Na','Pb');
ylabel('Temperature, K','fontsize',16)
xlabel('Distance, m','fontsize',16)
grid on;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Task 2 - Cladding temperatures calculation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 2.1 - Calc. dynamic viscosity distribution in the channel. 
%Calculate dynamics viscosity distribution in the channel
mi_1 = mi_Na(T_1);
mi_2 = mi_Pb(T_2);

plot(z_11,mi_1,z_22,mi_2)
legend('Na','Pb')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 2.2 -  Calc. Reynolds number distribution in the channel. You can use mass flux
%Calculate Reynolds number across the channel
G_1 = m_1./A_1 ;  %mass flux = mass flow / flow area
G_2 = m_2./A_2;   %mass flux = mass flow / flow area
Re_1 = G_1.*Dh_1./mi_1;
Re_2 = G_2.*Dh_2./mi_2;
plot(z_11, Re_1,'o', z_22, Re_2)
legend('Na','Pb');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 2.3 -  Calc. specific heat capacity distribution. 
%calculate specific heat capacity across the channel
cp_1 = Cp_Na(T_1);
cp_2 = Cp_Pb(T_2);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 2.4 -  Calc. coolant heat conductivity distribution.
%Calculate coolant heat conductivity across the channel
k_1 = k_Na(T_1);
k_2 = k_Pb(T_2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 2.5 -  Calc. Prandtl number distribution.
%Calculate Prandtl number across the channel
Pr_1 = cp_1.*mi_1./k_1;
Pr_2 = cp_2.*mi_2./k_2;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 2.6 -  Calc. Peclet number distribution.
%Calculate Peclet number across the channel
Pe_1 = Re_1.*Pr_1;
Pe_2 = Re_2.*Pr_2;
plot(z_11,Pe_1,'o',z_22,Pe_2)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 2.7 - Calc. Heat Transfer Coefficient using Nusselt Number. Select proper correlation  (use Lecture 7).
%Caclaulte Heat Transfer Coefficient using Nusselt Number
Nu_1 = fun_Nu(Pe_1,'tri',PD_1);
Nu_2 = fun_Nu(Pe_2,'sqr',PD_2);

%Heat Transfer Coefficient
h_1 = k_1.*Nu_1./Dh_1
h_2 = k_2.*Nu_2./Dh_2

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 2.8 - Calc. the cladding surface outer temperature. Find maximum value.

%Cladding outer temperature
Tco_1 = T_1 + q_1./(h_1.*Dclad_1.*pi);
Tco_2 = T_2 + q_2./(h_2.*Dclad_2.*pi);
plot(z_11,T_1,z_22,T_2, z_11, Tco_1, z_22, Tco_2);
legend('Na flow','Pb flow', 'Na clad out', 'Pb clad out');
ylabel('Temperature, K','fontsize',16)
xlabel('Distance, m','fontsize',16)
grid on;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 2.9 - Calc. the cladding  surface  inner   temperature. Find maximum value and plot results for both cladding temperatures.

% Cladding inner temperature
Tci_1 =  Tco_1 + q_1./(2.*pi.*kc_1).*log(Dclad_1./Diclad_1);
Tci_2 =  Tco_2 + q_2./(2.*pi.*kc_2).*log(Dclad_2./Diclad_2);
figure(3)
plot(z_11,T_1,z_22,T_2, z_11, Tco_1, z_22, Tco_2,z_11,Tci_1,z_22,Tci_2);
legend('Na flow','Pb flow', 'Na clad out', 'Pb clad out','Na clad in', 'Pb clad in');
ylabel('Temperature, K','fontsize',16)
xlabel('Distance, m','fontsize',16)
grid on;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Task 3 - Fuel temperature calculation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 3.1 -  Calc. fuel outer surface temperature distribution across the channel. Find maximum value. 

%Fuel outer surface temperature
Tfo_1 = Tci_1 + q_1./(pi.*Dfuel_1.*h_gap);
Tfo_2 = Tci_2 + q_2./(pi.*Dfuel_2.*h_gap);

figure(4)
plot(z_11,T_1,z_22,T_2, z_11, Tco_1, z_22, Tco_2,z_11,Tci_1,z_22,Tci_2, z_11, Tfo_1, z_22, Tfo_2);
plot(z_11,T_1,'r-',z_22,T_2,'b-', z_11, Tco_1,'r--', z_22, Tco_2,'b--',z_11,Tci_1,'ro',z_22,Tci_2,'bo', z_11, Tfo_1,'r:', z_22, Tfo_2,'b:');
legend('Na flow','Pb flow', 'Na clad out', 'Pb clad out','Na clad in', 'PB clad out','Na fuel out', 'Pb fuel out');
ylabel('Temperature, K','fontsize',16)
xlabel('Distance, m','fontsize',16)
grid on;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 3.2 - Calc. fuel centerline temperature distribution with iterative process presented in Appendix 1. 
% Find max temperature value and plot fuel temperatures.

%Fuel centreline temperature profile
%first temperature guess
  Tfc_1 = ones(numel(q_1),1).*2500.0;
  Tfc_2 = ones(numel(q_2),1).*2500.0;
  kf_1 = zeros(numel(q_1),1);
  kf_2 = zeros(numel(q_2),1);
 
% Iterative procedure :
for i=1:50
  eps=1.0;
  while eps>1e-5  
	T_prev = Tfc_1(i);
	kf_1(i) = (1./(Tfc_1(i)-Tfo_1(i))).*quad(@fun_k_MOX,Tfo_1(i),Tfc_1(i));
	Tfc_1(i) = Tfo_1(i) +  q_1(i)./(4.*pi.*kf_1(i));
	
	eps=abs((Tfc_1(i)-T_prev)./T_prev);
  end
end

for i=1:50 
  eps=1.0;
  while eps>1e-5 
    
	T_prev = Tfc_2(i);
	kf_2(i) = (1./(Tfc_2(i)-Tfo_2(i))).*quad(@fun_k_MOX,Tfo_2(i),Tfc_2(i));
	Tfc_2(i) = Tfo_2(i) +  q_2(i)./(4.*pi.*kf_2(i));
	
	eps=abs((Tfc_2(i)-T_prev)./T_prev);
  end
end


figure(5)
plot(z_11,T_1,z_22,T_2, z_11, Tco_1, z_22, Tco_2,z_11,Tci_1,z_22,Tci_2, z_11, Tfo_1, z_22, Tfo_2, z_11, Tfc_1, z_22, Tfc_2 );
plot(z_11,T_1,'r-',z_22,T_2,'b-', z_11, Tco_1,'r--', z_22, Tco_2,'b--',z_11,Tci_1,'ro',z_22,Tci_2,'bo', z_11, Tfo_1,'r:', z_22, Tfo_2,'b:', z_11, Tfc_1, 'r.-', z_22, Tfc_2,'.-b');
legend('Na flow','Pb flow', 'Na clad out', 'Pb clad out','Na clad in', 'PB clad out','Na fuel out', 'Pb fuel out', 'Na fuel center', 'Pb fuel center');
ylabel('Temperature, K','fontsize',16)
xlabel('Distance, m','fontsize',16)
grid on;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Task 4 - Pressure Drop calculation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 4.1 - Calculate friction pressure losses.

rho_1 = mean(rho_Na(T_1))  %mean density of sodium
rho_2 = mean(rho_Pb(T_2))  %mean densty of lead

%Fanning friction factor coefficient
Cf_1 = 0.25.*(0.96.*(PD_1+0.63))./((1.82.*log10(mean(Re_1))-1.64).^2);     % Sodium, For triangular lattice
Cf_2 = 0.0791./(mean(Re_2).^0.25);                                         % Lead, Blassius for square lattice

dp_fric_1 = 4.*Cf_1.*H_1.*(G_1.^2)./(Dh_1.*2.*rho_1);  %sodium pressure drop
dp_fric_2 = 4.*Cf_2.*H_2.*(G_2.^2)./(Dh_2.*2.*rho_2);  %lead pressure drop

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 4.2 - Losses due to the wire wrapper for SFR and spacer grids for LFR. 
% Wire wrap and spacer calcs
Hw = 0.1;  %wire lead
M=((1.034)./(PD_1.^0.124)+(29.7.*(PD_1.^6.94).*(mean(Re_1).^0.086))./((Hw./Dfuel_1).^(2.239))    ).^(0.885);
dp_fric_1 = M.*dp_fric_1

% Spacer grid pressure drop - LFR
eta_2 = 1.95.*(mean(Re_2).^(-0.08));
dp_spacer_2 = 10*eta_2*(G_2.^2)./(2.*rho_2)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 4.3 - Elevation pressure drop / Gravity pressure drop
dp_grav_1 = rho_1.*9.81.*H_1;
dp_grav_2 = rho_2.*9.81.*H_2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 4.4 - Local losses at the assembly inlet and exit.
eta_in  = 0.5;
eta_out = 1.0;
dp_inlet_1 = eta_in*(G_1.^2)./(2.*rho_1);
dp_inlet_2 = eta_in*(G_2.^2)./(2.*rho_2);
dp_outlet_1 = eta_out*(G_1.^2)./(2.*rho_1);
dp_outlet_2 = eta_out*(G_2.^2)./(2.*rho_2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 4.5 - Calculate the total pressure drop across the core.

%Total pressure drop
dp_1 = dp_fric_1 + dp_grav_1 +  dp_inlet_1  + dp_outlet_1
dp_2 = dp_fric_2 + dp_grav_2 +  dp_inlet_2  + dp_outlet_2 + dp_spacer_2








