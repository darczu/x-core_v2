%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% X-Core v2.0 developed by Piotr Darnowski - all rights reserved
% Rev-1 01-10-2018
% Update 24-07-2020
% T [K] and rho [g/cc]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: T				      - temperature, [K]
% 2: p				      - pressure, [Pa]
% 3: mode			      - mode, see below - options 
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: rho			      - density, [g/cc]
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% T                 - 300 K
% p                 - 1e5 Pa
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1:
% 2
% 3: 
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 
%REFERENCES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% see REFERENCES.m file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function out = cp_CS(T,mode)

if(nargin <1)
		T =300.0;  % Default Temperature 
end
if(nargin < 2)
 mode ='MELCOR';  %Default 'MELCOR'
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch mode

%if(strcmp(mode,'MELCOR'))
	case {1, 'MELCOR'}
	% Reference - MELCOR Reference Manual [4]
	T1 = [
	0.0	%added
	273.15 
	373.15 
	473.15 
	573.15 
	673.15 
	773.15 
	873.15 
	923.15 
	973.15 
	1023.15 
	1033.15 
	1073.15 
	1123.15 
	1223.15 
	1349.82 
	1373.15 
	1473.15 
	1573.15 
	1673.15 
	1773.15 
	1810.90 
	1810.91 
	5000.00 
	];

	Cp1 = [
	  435.89   %added
	  435.89 
	  477.45 
	  519.02 
	  560.59 
	  602.15 
	  665.70 
	  753.62 
	  816.43 
	  921.10 
	  1130.44  
	  1339.78 
	  837.36 
	  732.69 
	  690.82 
	  690.82 
	  693.58 
	  705.38 
	  717.18 
	  728.99 
	  740.79 
	  745.25 
	  745.25 
  745.25 
	];

	% Linear inteprolation %vq = interp1(x,y,xi)
	for i=1:numel(T)
		out(i) = interp1(T1,Cp1,T(i));
		
		% No extrapolation is allowed
		if(T(i)>T1(end))
			disp('WARNING: temperature above allowed');
		elseif (T(i)<T1(1))
			disp('WARNING: temperature below allowed');
		end	
	end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	case {2, 'Kolev'}
	
		error('no such modes');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	case {3, '15Mo3_1', 'NARSIS_Forver_1'}
		out = 423.0
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% https://matmatch.com/materials/minfm32015-din-17243-grade-15mo3?utm_term=&utm_medium=ppc&utm_campaign=%5BSD%5D+Materials+Generic&utm_source=adwords&hsa_grp=61532612146&hsa_cam=918492904&hsa_src=g&hsa_net=adwords&hsa_tgt=aud-421245424252:dsa-19959388920&hsa_ad=311442906172&hsa_mt=b&hsa_kw=&hsa_ver=3&hsa_acc=2813184419&gclid=Cj0KCQjw6uT4BRD5ARIsADwJQ19_6P9SetIpT4pHUC5vWD2Vkl4b0TyYNoHHrWOoJAo6no3ldKrTYGsaApOxEALw_wcB
% MIN
	case {4, '15Mo3_2', 'NARSIS_Forver_2'}

	data = [
			-100.36444819497592, 420.933829379881
		19.691876306577058, 459.5274461142718
		99.74374332138655, 478.3790932904611
		200.39111415033483, 498.98901855597796
		300.1916138978746, 514.7330978778137
		399.7315379280576, 535.9770620520318
		500.30374268469154, 556.5424953351474
		600.0491206458673, 584.9513904271937
		3000.0, 584.0  %added articially
			];
		T1 = data(:,1);
		T1 = T1+273.15;		
		Cp1 = data(:,2);

		for i=1:numel(T)
			out(i) = interp1(T1,Cp1,T(i),'linear','extrap');
			
			if(T(i)>T1(end))
				disp('WARNING: temperature above allowed');
			elseif (T(i)<T1(1))
				disp('WARNING: temperature below allowed');
			end	
		end	
		
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% https://matmatch.com/materials/minfm32015-din-17243-grade-15mo3?utm_term=&utm_medium=ppc&utm_campaign=%5BSD%5D+Materials+Generic&utm_source=adwords&hsa_grp=61532612146&hsa_cam=918492904&hsa_src=g&hsa_net=adwords&hsa_tgt=aud-421245424252:dsa-19959388920&hsa_ad=311442906172&hsa_mt=b&hsa_kw=&hsa_ver=3&hsa_acc=2813184419&gclid=Cj0KCQjw6uT4BRD5ARIsADwJQ19_6P9SetIpT4pHUC5vWD2Vkl4b0TyYNoHHrWOoJAo6no3ldKrTYGsaApOxEALw_wcB
% MAX	
	case {5, '15Mo3_3', 'NARSIS_Forver_3'}

	data = [	
		-100.73669921977093, 424.0977483815651
		19.616710234262683, 456.8173775153466
		99.81890939370095, 498.3334658026221
		200.39111415033483, 540.0185109850923
		300.06132603919616, 532.1113282899739
		399.7315379280576, 620.4082017187959
		500.3789087570059, 689.9914094358377
		600.0491206458673, 780.045434574686
		];
	
		T1 = data(:,1);
		T1 = T1+273.15;		
		Cp1 = data(:,2);

		for i=1:numel(T)
			out(i) = interp1(T1,Cp1,T(i),'linear','extrap');
			
			if(T(i)>T1(end))
				disp('WARNING: temperature above allowed');
			elseif (T(i)<T1(1))
				disp('WARNING: temperature below allowed');
			end	
		end	
		
	
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
otherwise
	error('no such modes');
end 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%