% X-Core v2.0 developed by Piotr Darnowski - all rights reserved
% Rev 28-03-2017
%% Created by Piotr Darnowski 2016
% Calculation of atomic density of the mixture
% Based on: MCNP Crticiality Primer II Appendix B: Calculating Atom Densities
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% N  - nuclear density [#at/cm3]   N0 - nuclear density [#at/barn/m]
% rho_mix - mixture density [g/cm3] A_mix - Molecular Weight [g/mole]
% N_A - Avogadro number [#at/mole]  ;  [#at/cm3] = [g/cm3]*[#at/mol]*[mol/g]
%
% Mixture of the Chemical Composition WEIGHT FRACTION CALCULATION
% Chemical substance given by a chemical formula: X_{m} Y_{n},  
% 
% A_i - vector with Atomic Weights
% m_i - vector with number of atomis in chemical compund 
% af_i - atomic fraction
% wf_i - weight fraction
%
% EXAMPLE:
%   Fe3O4   m=3, n=4
%   wfaf_chem([56.0, 16.0],[3 4])
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [wf_i, af_i] = wfaf_chem(A_i,n_i)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    import XCore.AtomicDensityPackage.*

wf_i = n_i.*A_i./sum(n_i.*A_i);
af_i = n_i./sum(n_i);
