% X-Core v2.0 developed by Piotr Darnowski - all rights reserved
% Rev 12-07-2017
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Uranium, Plutonium, MOX Melting Temperatures
% Tsol - solidus temperature [K]
% Tliq - liquidus temperature [K]
% xPu - plutonium [wt%]
% Bu - Burnup [MWd/tU]
% Ref: MATPRO RELAP5 3D Chapter 2.1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [Tsol, Tliq] = T_fuel_melt(BU,xPu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  import XCore.AtomicDensityPackage.*

if nargin < 2 
  xPu=0;
end

if nargin < 1
  BU = 0;
end


if(xPu>0)
Tsol = 3113.15 -5.41395.*xPu + (7.468390e-3).*(xPu.^2) - (3.2e-3).*BU;

Tliq = 3113.15 -3.21660.*xPu - (1.448518e-2).*(xPu.^2) - (3.2e-3).*BU;

elseif(xPu==0)
Tsol = 3113.15 - (3.2e-3).*BU; 
Tliq = Tsol;
end
