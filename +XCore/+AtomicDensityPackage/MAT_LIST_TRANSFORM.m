% X-Core v2.0 developed by Piotr Darnowski - all rights reserved
% Rev 02-08-2017 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MATERIAL LIST TRANSFORM -> takes material vector and transform it to the material vector consistent in size with material database 
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 	vector		- input vector, mass, etc.
% 	vector_name	- input vector material names
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  	new_vector 	- vector with size equal to the input material database size
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% vector_name = {'UO2' 'U' 'SS' 'SSOX'}; vector = [1000 2000 3000 4000];
% [new_vector]=MAT_LIST_TRANSFORM(vector, vector_name)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [new_vector]=MAT_LIST_TRANSFORM(vector, vector_name)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  import XCore.AtomicDensityPackage.*

if(ne(numel(vector),numel(vector_name))) error('vector and vector name not equal'); end

%MASS_2 = reshape(M1,[],1);

[MATERIAL, MATERIAL_NO, TYPE]=MAT_LIST();  %load current material database

mat_size = numel(MATERIAL); %number of materials in the database
new_vector = zeros(mat_size,1);

% 	dummy = sum(strcmp(MATERIAL_i{j}, MATERIAL));
dummy =[];
for i=1:numel(vector_name)
  dummy =[ dummy,  strcmp(vector_name{i},MATERIAL).*vector(i)];
end
dummy=sum(dummy,2);

new_vector = dummy;




