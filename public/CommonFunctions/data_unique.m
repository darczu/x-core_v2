% X-Core v2.0 developed by Piotr Darnowski - all rights reserved
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
% Function reads data file and make it uniqe based on selected column of data file which has to be unique
% column to be used as a basis for uniqness
% data - input data matrix
% column - this column has to be used to make data unique
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
%  function data_out = data_unique(data,column)
%
% EXAMPLE:
% A = randi(10,5)
% data_unique(A,1)
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
function data_out = data_unique(data,column)
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
if nargin < 2
    column = 1;
end    

nRows = size(data,1);
nCols = size(data,2);

if column > nCols
    error('columns larger than input data file');
end    

x= data(:,column);

%for i=1:nCols
 %   if i ~= column
    [x_dummy, xui]  = unique(x);
  %  end
%end
data_out =  data(xui,:);

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	