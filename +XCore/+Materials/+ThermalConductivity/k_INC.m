%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% X-Core v2.0 developed by Piotr Darnowski - all rights reserved
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Inconel Th Cond  % Rev-2 16-09-2020
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: T				      - temperature, [K]
% 2: mode			      - mode, see below - options 
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: k		      - Cp, [J/kg/K]
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% T                 - 300 K
% mode              - 'Default'
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% {1, 'Inc-690','In690', 'Default'}
% {2, 'Inc-600', 'Inc600'}
% {3, 'Inc-625', 'Inc625'}
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Cp_INC(300)
%REFERENCES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% see REFERENCES.m 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function k = k_INC(T,mode)
if(nargin < 1)	
	T = 300.0;  % Default Temperature 
end
if(nargin < 2)
	mode ='Default';  %Default 
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch mode
case {1, 'Inc-690','In690', 'Default'}
	% Reference - [70]
		T_data = [
			100
			200
			300
			400
			500
			600
			700
			800
			900
			1000
		];
		T_data = T_data + 273.15;
		k_data = [
			13.5
			15.4
			17.3
			19.1
			21.0
			22.9
			24.8
			26.6
			28.5
			30.1
		];
	% Linear inteprolation %vq = interp1(x,y,xi)
	for i=1:numel(T)
		k(i) = interp1(T_data,k_data,T(i),'linear','extrap');
		% Extrapolation allowed
	end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
case {2, 'Inc-600', 'Inc600'}
% Taken from FPT model. Reference unknown
	T_data = [
		283.15
		310.9
		422.0
		533.1
		644.3
		755.4
		866.5
	   
	];

	k_data = [
		17.0
		17.0
	   18.92
	   21.18
	   23.21
	   25.22
	   29.51
	];
	% Linear inteprolation %vq = interp1(x,y,xi)
	for i=1:numel(T)
		k(i) = interp1(T_data,k_data,T(i),'linear','extrap');
		% Extrapolation allowed
	end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
case {3, 'Inc-625', 'Inc625'}
	% Taken from FPT model. Reference unknown
		T_data = [
			255.000	
			294.000	
			366.000	
			477.000	
			700.000	
			922.000	
			1033.000
			1561.000
		];
	
		k_data = [
			9.230
			9.810
			10.820
			12.550
			15.720
			19.040
			20.770
			29.000			
		];
		% Linear inteprolation %vq = interp1(x,y,xi)
		for i=1:numel(T)
			k(i) = interp1(T_data,k_data,T(i),'linear','extrap');
			% Extrapolation allowed
		end
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

otherwise
	disp('ERROR: No such case');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%