%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% X-Core v2.0 developed by Piotr Darnowski - all rights reserved
% Rev0 18-07-2017, Rev1 16-06-2023
% Steel Oxide - it is assumed to be FeO+ Iron Monoxide
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: T				      - temperature, [K]
% 2: p				      - pressure, [Pa]
% 3: mode			      - mode, see below - options 
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: rho			      - density, [g/cc]
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% T                 - 300 K
% mode              - 'Default'
% p                 - 1e5 Pa
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% {1, 'Kolev'}
% {2, 'MATPRO'}	
% {3, 'MELCOR', 'Default'}
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% REFERENCES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% see REFERENCES.m file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function rho = rho_SSOX(T,mode,p)

import XCore.AtomicDensityPackage.*
import XCore.Materials.Density.*

rho_liq_pT = @density_liquid_pT; 

  T_melt =1642.0; %Kolev
  %T_melt = 1870.0;
  T_boil = 3687.15; %Kolev
  
if(nargin < 1)
	T =300.0;  % Default Temperature 
end 
if(nargin <2)
	mode = 'MELCOR';  %Default
end
if(nargin < 3)
	p = 1e5;
end

NT = numel(T); rho = zeros(1,NT);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%if(strcmp(mode,'Kolev')==1)
switch mode
	case {1, 'Kolev'}
   		[ ident, T0, rho0, rho0beta, beta, kappa, cpliq, INFO ]= MAT_MELT_DATABASE(8);
    
		for i=1:numel(T)
			if(T(i)<T_melt)
				rho(i) = 5702.133168.*( 1.0 - 3.0.*(  (-0.409e-2) + (1.602e-5).*T(i)  +  (-7.913e-9).*T(i).^2  +    (5.348e-12).*T(i).^3));
				rho(i) = rho(i)./1000;
			else
			
				rho(i) = rho_liq_pT(T(i),T0,p,beta,kappa,rho0l)./1000;			%[10] - to jest cos nie tak
			end
		end  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
	case {2, 'MATPRO'}		% SSOX MATPRO 	
		%elseif(strcmp(mode,'MATPRO')==1)	
		c = [-0.409	1.602e-3	-7.913e-7	 5.348e-10; ...   %OK
			   -0.2537	7.300e-4	 4.964e-7	-1.140e-10; ...   %OK - poprawka wprowadzona !!!
			   -0.214	6.929e-4	-1.107e-7	 8.078e-10 ];    %OK
		%rho0s= 8.0; %@300K
		%rho0s= 5.8; %@300K
		rho0s= 5.2; %@300K dobrane zeby sie z wykresem zgadzalo
	
	%	eps_FeO =[];  	eps_Fe2O3 =[];  	eps_Fe3O4 =[];  eps_avg=[];
		
		for i=1:numel(T)
		%	eps_FeO(i) 	 = c(1,1) + c(1,2).*T(i) + c(1,3).*(T(i).^2)   + c(1,4).*(T(i).^3) ;
		%	eps_Fe2O3(i) = c(2,1) + c(2,2).*T(i) + c(2,3).*(T(i).^2)   + c(2,4).*(T(i).^3) ;
		%	eps_Fe3O4(i) = c(3,1) + c(3,2).*T(i) + c(3,3).*(T(i).^2)   + c(3,4).*(T(i).^3) ;
		%	eps_avg(i)     =  (eps_FeO(i) + eps_Fe2O3(i)  +  eps_Fe3O4(i))./3;
			eps_FeO 	 = c(1,1) + c(1,2).*T(i) + c(1,3).*(T(i).^2)   + c(1,4).*(T(i).^3) 
			eps_Fe2O3 = c(2,1) + c(2,2).*T(i) + c(2,3).*(T(i).^2)   + c(2,4).*(T(i).^3) 
			eps_Fe3O4 = c(3,1) + c(3,2).*T(i) + c(3,3).*(T(i).^2)   + c(3,4).*(T(i).^3) 
			eps_avg     =  (eps_FeO + eps_Fe2O3  +  eps_Fe3O4)./3;
			eps_avg = eps_avg.*1e-2;
			%eps_avg = eps_avg.*1e-2; %[%] -> [-]
			%eps(i) = eps_avg;
			%plot(T,eps_Fe2O3)
		%end
		
			rho(i) = rho0s.*((1+eps_avg).^(-3));
			%rho(i) = rho0s.*((1+3*eps_avg).^(-1));
			%rho(i) = rho0s.*((1-3.*eps_avg));
			
			% rho =m/V  rho0=m/V0 V=L*L*L  V0 = L0^3   rho=rho0*(L0/L)^3 = rho0*(L/L0)^-3 = rho0*(1+dL/L0)^-3
		end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%elseif(strcmp(mode,'MELCOR')==1)		
	case {3, 'MELCOR', 'Default'}
	for i=1:numel(T)
		rho(i) = 5.18; 						%[4] MELCOR 
	end
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	otherwise
		error('No such mode');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  
  
%rho = 7.5512 - (0.11167e-3).*T - (0.15063e-6).*T.^2;  %Ref ?


