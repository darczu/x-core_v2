%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% X-Core v2.0 developed by Piotr Darnowski - all rights reserved
% Rev 4-02-2020
% Rev 29-09-2018  
% UO2 THC
% T - Temperature in [K] 
% option - 'MELCOR' (Default) or 'Kolev'
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: T				      - temperature, [K]
% 2: p				      - pressure, [Pa]
% 3: mode			      - mode, see below - options 
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: rho			      - density, [g/cc]
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% T                 - 300 K
% p                 - 1e5 Pa
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1:
% 2
% 3: 
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 
%REFERENCES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% see REFERENCES.m file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function out = k_UO2(T,option)

if(nargin < 2)
 option ='MELCOR';  %Default 'MELCOR'
end

if(strcmp(option,'MELCOR'))
% Reference - MELCOR Reference Manual [4]

	T_MELCOR = [
	273.15 
	366.3 
	539.0 
	757.0 
	995.0 
	1182.0 
	1490.0 
	1779.0 
	1975.0 
	2181.0 
	2373.0 
	2577.0 
	2773.0 
	3026.0 
	3113.0 
	5000.0 
	];
	
	k_MELCOR = [
	9.24 
	7.79 
	6.53 
	4.92 
	3.87 
	3.20 
	2.53 
	2.19 
	2.17 
	2.25 
	2.56 
	2.80 
	3.15 
	3.75 
	3.96 
	3.96 
	];

	% Linear inteprolation %vq = interp1(x,y,xi)
	for i=1:numel(T)
		out(i) = interp1(T_MELCOR,k_MELCOR,T(i));
		% No extrapolation is allowed
		if(T(i)>T_MELCOR(end))
			error('ERROR(#1): Material temperature above allowed,  273.15<T<5000');
		elseif (T(i)<T_MELCOR(1))
			error('ERROR(#2): Material temperature below allowed,  273.15<T<5000');
		end	
	end


elseif(strcmp(option,'Carbajo'))	 %Suggsted by Carbajo in [3]
	t = T./1000;
	for i=1:numel(T)
		out(i) = 1.158.*((100./(7.5408 + 17.692.*t(i) + 3.6142.*(t(i).^2) ))  + exp(-16.35./t(i)).*6400.0./(t(i).^(5/2))  )
	end

elseif(strcmp(option,'Fink'))	 %Fink work for TD 95% [14]
	t = T./1000;
	for i=1:numel(T)
		out(i) = 1.0*((100./(7.5408 + 17.692.*t(i) + 3.6142.*(t(i).^2) ))  + exp(-16.35./t(i)).*6400.0./(t(i).^(5/2))  )
	end




elseif(strcmp(option,'Kolev'))	









else
	error('no such options');
end  
