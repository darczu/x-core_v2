% X-Core v2.0 developed by Piotr Darnowski - all rights reserved
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TESTING SCRIPT
% add path
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all; clear; clc;
addpath(genpath('./')); % Add main folder all subfolders
pause(1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% IMPORT PACKAGES
import XCore.Materials.Density.*
import XCore.Materials.SpecificHeatCapacity.*
import XCore.Materials.Viscosity.*
import XCore.Materials.ThermalConductivity.*
import XCore.Materials.SteamTables.*
import XCore.AtomicDensityPackage.*


T = linspace(300,1000,10);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tic
disp('Run test suite for functions');
% DENSITY
disp('Density tests...');
rho_AIC(T);
rho_AIR(T);
rho_B2O3(T);
rho_B2ZR(T);
rho_B4C(T);
rho_CON(T);
rho_CS(T);
rho_FE(T);
rho_FP(T);
rho_FPO2(T);
rho_GRP(T);
rho_He(T);
rho_INC(T);
rho_MOX(400.0,0.05,0.05);
rho_Na(T);
rho_PAINT(T);
rho_Pb(T);
rho_PuO2(T);
rho_SS(T);
rho_SSOX(T);
rho_ThPuO2(T);
rho_ThUO2(T);
rho_U(T);
rho_UO2(T);
rho_ZR(T);
rho_ZRO2(T);
%
T = [300,500,1000,1500, 3000, 3500];
rho_ZR(T,1);
rho_ZR(T,2);
rho_ZR(T,3);
rho_ZR(T,4);
rho_ZRO2(T, 1);
rho_ZRO2(T, 2);
rho_ZRO2(T, 3);
rho_UO2(T,1);
rho_UO2(T,2);
rho_UO2(T,4);
rho_UO2(T,5);
rho_UO2(T,6);
rho_U(T,1)
rho_U(T,2)
rho_U(T,3)
% 
density_thermal_expansion(5000,0.03);
density_liquid_pT(T, 400.0, 1e5, 0.01, 0.1, T);
% 
disp('Completed');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Specific Heat Capacity tests...');
% SPEC HEAT CAPACITY
cp_AIC(T);
cp_B4C(T);
cp_CON(T);
cp_CS(T);
cp_GRP(T);
cp_He(T);
cp_INC(T);
cp_Na(T);
cp_PAINT(T);
cp_Pb(T);
cp_SS(T);
cp_SS304(T);
cp_SS316(T);
cp_SSOX(T);
cp_U(T);
cp_UO2(T);
cp_ZR(T);
cp_ZRO2(T);
disp('Completed');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Thermal Conductivity tests...');
k_AIC(T);
k_AIR(T);
k_Am(T);
k_B4C(T);
k_CON(T);
k_CS(T);
k_GRP(T);
k_H(T);
k_He(T);
k_INC(T);
k_MOX(T);
k_Na(T);
k_PAINT(T);
k_Pb(T);
k_Pu_delta(T);
k_Pu_epsilon(T);
k_PuN(T);
k_SS(T);
k_SS304(T);
k_SS316(T);
k_SSOX(T);
k_U(T);
k_U10Zr(T);
k_UN(T);
k_UO2(T);
k_ZR(T);
k_ZRO2(T);
%
k_porosity(10,1e5,2.0);
%
disp('Completed')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Steam Tables - XSteam tests ...')
XSteam('h_pT',40,500-273.15);  % Remeber about units - degC and bars
XSteam('rho_pT',10, 350);

disp('Completed')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Viscosity (dynamic)
disp('Viscosity tests...')
mi_He(T);
mi_Na(T);
mi_Pb(T);
disp('Completed')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PUBLIC FUNCTIONS - NOT PACKAGE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Atomic Density Sub-Package tests
disp('Atomic Density Sub-Package - Tests...')
MAT_LIST();

MOLAR_BASE(3) ;
MOLAR_BASE('SS') ;
MOLAR_BASE({'SS'}) ;
MOLAR_BASE({'SS'},'XSDATA') ;
MOLAR_BASE({'SS'}, 2 );

DENSITY_BASE(3,300)  ;
DENSITY_BASE('SS',300);
DENSITY_BASE({'SS'},300);

[ZAID, COMP]=MAT_DATABASE(3, 0);
[COMP,ZAID, NAME, UNIT, SUMC] = MAT_DATABASE('UO2');
[COMP,ZAID, NAME, UNIT, SUMC] = MAT_DATABASE({'UO2'});
[COMP,ZAID, NAME, UNIT, SUMC] = MAT_DATABASE(['UO2']);
[COMP,ZAID, NAME, UNIT, SUMC] = MAT_DATABASE(3);

vector_name = {'UO2' 'U' 'SS' 'SSOX'}; vector = [1000 2000 3000 4000];
MAT_LIST_TRANSFORM(vector, vector_name);

[ ident, T0, rho0, rho0beta, beta, kappa, cpliq, INFO ] = MAT_MELT_DATABASE(1);

A_mix([1, 2],[16, 1],1); 	
A_mix([1, 2],[16, 1]);   
A_mix([0.05, 0.95],[235, 238],2);
A_mixMAT([0.2,0.8],{'UO2', 'SS'},'af','Simple');

N_mix(10.5,271);
N_mix(10.5,271,1.0,0);
N_mix(18.0,[235 238],[0.2 0.8],1);
N_mix([6.0, 7.0],[235 238],[0.21 0.79],2);
N_mix(5.0,[235 238], [0.2 0.8],3);
N_mix(DENSITY_BASE('SS',1000),MOLAR_BASE('SS'));

N_mixMAT({'SS'},4000);
[N,rho, A ] = N_mixMAT({'SS' 'UO2'});
[N,rho, A ] = N_mixMAT({'SS' 'UO2'},[300, 500]);
[N,rho, A, n ] = N_mixMAT({'SS' 'UO2'},[300, 500],[1000 300]);

wf2af([0.333 0.667],[1.0 16.0],-1);
af2wf([0.333 0.667],[1.0 16.0],-1);
wfaf_chem([56.0, 16.0],[3 4]);

ZAID2A(92235);
ZAID2Z(92235);

mass_i= [ 578 1173.6 24891 1961.3 34174 24985 7887.1];
mat_i = {'CRP'    'INC'    'SS'    'SSOX'    'UO2'    'ZR'    'ZRO2'};
Temp_i = 2500; rho_model = 'MassFrac'; A_model = 'Simple';
CORIUM_MASS2N(mass_i,mat_i,Temp_i,rho_model, A_model);
CORIUM_MASS2N(1000,{'SS'},Temp_i,rho_model, A_model);

x_i=[0.2 0.8]; rho_i = [6.0 9.0]; model = 'Hull'; rho_INC = @rho_INC;  rho_SS = @rho_SS;
rho_mix(x_i,rho_i,model);
rho_mix([0.88, 0.12], [rho_INC(), rho_SS()],'MassFrac');

x_i=[0.2 0.8]; MATERIAL_i ={'UO2', 'ZRO2'}; T=3000; model = 'Hull'; x_mode = 'af'; A_model = 'Complex';
rho_mixMAT(x_i,MATERIAL_i,T,'af',model,'Simple');
rho_mixMAT([1],{'UO2'},3000,'af','AtomFrac','Simple');
rho_mixMAT(1,{'SS'},1000);
rho_mixMAT(x_i,MATERIAL_i,T,x_mode,model,A_model);
rho_mixMAT(1,{'UO2'},2500);

[Tsol, Tliq] = T_fuel_melt(10.0,0.1);

n_input = [10, 10, 10 ]; mat_i={'UO2' 'SS' 'SSOX'}; Temp_i = 2500; A_model = 'Complex'; rho_model = 'MassFrac' ; input_mode = 'mole';
[V_mix, rho_mix, mass_i, mass_mix, N_i, N_mix, n_i, n_mix] = ...
V_mixMAT(n_input ,mat_i, Temp_i,rho_model, A_model,input_mode);

disp('Completed');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Common functions tests
disp('Common functions - Tests...');

C0 = [0.0, 0.0625, 0.125, 0.1875, 0.25, 0.3125, 1.0];  x =0.2;
BisecSearch(x, C0);
A = randi(10,5);
data_unique(A,1);
normalization([1,2,3; 1, 2, 3]);

disp('Completed');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('All tests completed...');
toc
