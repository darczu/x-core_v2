%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% X-Core v2.0 developed by Piotr Darnowski - all rights reserved
% CHF implementation by Edgar Jonczyk 
% Critical heat flux CHF with Jeans-Lottes correlation
% Rev0 30-12-2024 
% TO BE REVIEWED as some data is uncertain
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% varargin              = matlab variable inputs
% 1: G = m/A            % mass flux [kg/s/m2] 
% 2: Tsat               % saturation temperature [degC]
% 3: Tlb                % liquid bulk temperature [degC]
% 4: p                  % pressure [bar]
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: q_cr					- Critical Heat Flux [kW/m2]			      
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% chf_JensLottes(3000,350,300,160) 
% XCore.ThermalHydrualics.CHF.chf_JensLottes(3000,350,300,160)
%REFERENCES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% (Kielkiewicz, 1987) p 212
% COMMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% some details not include in this reference so correlation has to be revised to confrim its validty
% based on term project by Edgar Jonczyk
% Check other references to cofirm:
% https://mooseframework.inl.gov/bison/source/materials/CoolantChannelMaterial.html
% https://www.tandfonline.com/doi/pdf/10.1080/18811248.2001.9715103
% https://www.nrc.gov/docs/ML0628/ML062840437.pdf
% it is old correlation, in different sources presented in a various ways
% be very careful with it. Also it has limited range of validity
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [q_cr] = chf_JensLottes(varargin) 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
num  = numel(varargin);    % number of input variables
in = varargin;             

G = in{1};          % mass flux [kg/m2/s]
Tsat = in{2};       % saturation temperature [C]
T1 = in{3};         % liquid bulk temperature [C]
p = in{4};          % pressure [bar]

if (num < 4)
   error('Not enought input arguments');
end

C = 2641.5.*exp(-0.03.*p);                  % pressure [bar] - TO REVIEW, source unknown
n = 0.0033.*p-0.0464;                       % [-], TO REVIEW, source unknown

q_cr = C.*(G.^n).*((Tsat-T1).^0.22);          %[kW/m^2] 

q_cr = q_cr./1000; %kW/m2->MW/m2