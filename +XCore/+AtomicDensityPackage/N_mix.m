% X-Core v2.0 developed by Piotr Darnowski - all rights reserved
% Rev 11-07-2017
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculation of atomic/nuclear/number denity  Based on: MCNP Crticiality Primer II Appendix B: Calculating Atom Densities
% N  - nuclear density [#at/cm3]   N0 - nuclear density [#at/barn/m]
% UNITS:  [#at/cm3] = [g/cm3]*[#at/mol]*[mol/g]
%  mixture density [g/cm3] A_mix - Molecular Weight [g/mole]
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1:	rho_i	- input density; it can be mixture density rho_mix or partial density
% 2:	A_i		- input atomic mass
% 3:	f_i		- weight fraction or atomic fraction
% 4:	mode 	- MODE (see below)
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1:	N_tot	- total number density
% 2:	N_i		- fractional number density
% 3:	af_i		- atomic fraction
% 4:	wf_i		- weight fraction
% MODEs %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% 0 - normal nuclear density based on rho_mix and A_mix, f_i = 1; IT IS ALSO NUMBER OF ATOMS IF RHO_MIX= N_MIX
% 1 - many materials, given weight fractions and mixture density,  rho_i=rho_mix, f_i=wf_i, A_i 
% 2 - many materials, given weight fractions and individual material densities, rho_i, f_i = wf_i, A_i 
% 3 - many materials given: atom fractions and atom mixture density, rho_i=rho_mix, f_i=f_i,
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% [N_tot, N_i, af_i, wf_i] = N_mix(rho_i,A_i,f_i,mode)
%EXAMPLE MODE 0:   N = N_mix(10.5,271,1.0,0)    or   N = N_mix(10.5,271)  
%EXAMPLE MODE 1:   N = N_mix(18.0,[235 238],[0.2 0.8],1)
%EXAMPLE MODE 2:   N = N_mix([6.0, 7.0],[235 238],[0.21 0.79],2)
%EXAMPLE MODE 3:   N = N_mix(5.0,[235 238], [0.2 0.8],3)    
%EXAMPLE MODE 0:  SINGLE MATERIAL:  N_mix(DENSITY_BASE('SS',1000),MOLAR_BASE('SS'))
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [N_tot, N_i, af_i, wf_i] = N_mix(rho_i,A_i,f_i,mode)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	import XCore.AtomicDensityPackage.*

	%N_A = 0.602214179e+24;  % [atoms/mole]Avogadro number
    %N_A = 0.602214179;  % [atoms/mole]Avogadro number
%	global N_A;
%global N_A; 	N_A = 0.602214179;  	% [atoms/mole]Avogadro number 
%global CM;   	CM  = 100.00; %CONVERSION FACTOR: METERS TO CM
%global KG;		KG 	= 0.001; %CONVERSION FACTOR: GRAMS TO KILOGRAMS
run PHYSICAL_CONSTANTS.m

	
 % 2023 %   disp('INFO #1 (N_mix): Nuclear densities in atoms/barn-cm');
 %   N_A0 = N_A/1e24;        % [atoms/barn/cm]Avogadro number in in 1/barn-cm 
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INITIAL TESTS
	if (nargin <3)
% 2023 %	    disp('INFO #2 (N_mix):Default Mode 0 - nuclear density based on rho_mix and A_mix, f_i = 1');
		mode = 0; f_i=1.0;
	end
	if( sum(f_i) ~= 1)
% 2023 %		disp('WARNING #1 (N_mix): Re-Normalization applied');
		f_i = normalization(f_i);	
%		sum(f_i)
	end
	if(numel(f_i)~=numel(A_i))
		error('ERROR #1 (N_mix): f_i has diffrent size than A_i !!!');
	end
	if( sum(f_i < 0)>0)
		error('ERROR #2 (N_mix): Molar fractions cannot less than zero !!!');
	end
	
% 2023 %	disp(['INFO #3 (N_mix): mode/case  = ', num2str(mode) ]);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
switch(mode)    %mode - type of the nuclear density calculation 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case 0
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 0 - normal nuclear density based on rho_mix and A_mix, f_i = 1;
%   rho_i = rho_mix  - scalar; Mixture density
%   A_i   = A_mix    - scalar; rMixture atomic weight 
%   f_i   = 0  - scalar; whatever
% Example: N = N_mix(18,235,1,0)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(numel(A_i)>1) error('ERROR #3 (N_mix): only one element in MODE 0'); end
	if(numel(rho_i)>1) error('ERROR #4 (N_mix): only one element in MODE 0'); end 
	
    N_tot  = rho_i.*N_A./A_i;
	N_i=[]; f_i = []; af_i = 1.0; wf_i =1.0;
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
    case 1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1 - many materials, given weight fractions and mixture density
%   rho_i = rho_mix - scalar; mixture density 
%   f_i   = wf_i    - vector; i-th material mass fraction vector
%   A_i   = A_i     - vector; i-th atomic weight vector
% Example:  N = N_mix(18.0,[235 238],[0.2 0.8],1)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(numel(rho_i)>1) error('ERROR #5 (N_mix): only one element'); end
	rho_mix = rho_i;
	wf_i = f_i;	
    N_i  = rho_mix.*wf_i.*N_A./A_i;
	N_tot = sum(N_i);
	af_i = N_i./N_tot;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
    case 2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2 - many materials, given weight fractions and individual material densities 
%   rho_i = rho_i   - vector -> rho_mix
%   f_i   = wf_i    - vector
%   A_i   = A_i     - vector i-th atomic weight
% Example:  N = N_mix([6.0, 7.0],[235 238],[0.21 0.79],2)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		wf_i = f_i;
        rho = wf_i./rho_i;
        rho = sum(rho);
        rho_mix = 1./rho;  
        N_i  = rho_mix.*wf_i.*N_A./A_i;
		N_tot = sum(N_i);
		af_i = N_i./N_tot;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case 3
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 3 - two materials given: atom fractions and atom mixture density
%   rho_i = rho_mix  - scalar
%   f_i = af_i       - vector 
%   A_i = A_i        - vector
% Example:  N = N_mix(5.0,[235 238],[0.2 0.8],3)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if(numel(rho_i)>1) error('ERROR #6 (N_mix): only one element');  end       
		af_i = f_i;
		A_mix = sum(A_i.*af_i);
		N_tot  = rho_i.*N_A./A_mix;
		%N_i = f_i.*N_mix;
        N_i = f_i.*N_tot;
		wf_i = af2wf(af_i,A_i);
   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
otherwise
        error('ERROR #7 (N_mix): There is no such a mode available');   
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%