%# INSTLALLATION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Add /x-core_v2 folder to the MATLAB Path. Then you will be able to use the package.
%You can use:
%```matlab
%addpath(genpath('./')); % Add when in folder 
% or
%addpath(genpath('x-core_v2')); % Add from external folder
%% or
%addpath('./')
%``` 
%savepath
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2024 - Generated with ChatGPT
% Generate path for the current folder and all subfolders
fullPath = genpath('./');

% Split the path into individual folders using ';' as a separator
folders = strsplit(fullPath, ';');

% Define patterns for folders to exclude
excludePatterns = {'.git', 'legacy'};

% Filter out folders that do not contain any of the exclusion patterns
filteredFolders = folders(~contains(folders, excludePatterns{1}) & ~contains(folders, excludePatterns{2}));

% Rejoin the folders into a path with semicolons as separators
fullPathFiltered = strjoin(filteredFolders, ';');

% Add the filtered path to MATLAB
addpath(fullPathFiltered);

% SAVE X-CORE in you path
% Savepath
savepath; % remove to addpth only for one matlab session

 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\
%{
% nowe 2024
% Generowanie ścieżki dla bieżącego folderu i wszystkich podfolderów
fullPath = genpath('./');

% Pomijanie określonych folderów
% Usuwanie ścieżek do folderów, których nie chcesz dodać
excludeFolders = {'.git'}; % folders to be excluded
for i = 1:length(excludeFolders)
    % Tworzenie wzorca do usunięcia z pełnej ścieżki
    pattern = fullfile(pwd, excludeFolders{i});
    % Usunięcie wzorca ze ścieżki
    %fullPath = regexprep(fullPath, [pattern '[^;]*;?'], '');
    fullPath = regexprep(fullPath, [regexptranslate('escape', pattern) '[^;]*;?'], '');
end

% Dodanie pozostałych ścieżek do ścieżki MATLAB
addpath(fullPath);
%}