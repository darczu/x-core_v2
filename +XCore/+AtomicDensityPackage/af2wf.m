% X-Core v2.0 developed by Piotr Darnowski - all rights reserved
% Rev 28-03-2017
% Function dedicated to re-calculate atom fraction to the weightfraction
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Based on: MCNP Crticiality Primer II Appendix B: Calculating Atom Densities
% wf - weight fractions vector 
% af - atom fractions vector
% A_i - Atomic weight of the isotope/material
% A_mix - Atomic weight of the mixture
% af = N(Isotope i)/sum(N(All isotopes))
% wf = Mass(Isotope i)/sum(Mass(All isotopes))
% if mode <= 0 calculate on its own!! otherwise uses A_mix function
%      af_i = wf2af([0.333 0.667],[1.0 16.0],-1)
% af2wf([0.333 0.667],[1.0 16.0],-1)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function wf_i = af2wf(af_i,A_i,mode)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    import XCore.AtomicDensityPackage.*

if(nargin < 3) mode = 1; end

if(numel(mode)>1) error('ERROR#1 (af2wf): mode only one element - one average value'); end

af_i = af_i./sum(af_i); %normalization necesarry

if( or( (nargin < 3), (mode>0) ))
     Amix = sum(af_i.*A_i);
     wf_i = af_i.*A_i./Amix;


elseif(mode <=0)
    Amix = A_mix(af_i,A_i,'af');
    wf_i = af_i.*A_i./Amix;
else
   error('ERROR#2 (af2wf): N/A');
end