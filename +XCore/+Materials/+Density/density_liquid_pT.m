%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% X-Core v2.0 developed by Piotr Darnowski - all rights reserved
% Rev 11.07.2017, Rev1 29-06-2023
% Calculations of the liquid material density based on special model developed by Kolev Ref: [10] Eq. 17.1.4 and [11] Eq, 3.238
% it calculated density variation due to the temperature and pressure changes
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: T 				- temperature, [K] 
% 2: T0			- reference temperature, [K] - usually melting temperature in Std. press & Temp
% 2: p				- pressure, [Pa]
% 2: p0			- reference pressure, [Pa] - 1e5 Pa = 1 bar
% 3: beta			- volumetric thermal expansion coefficient [1/K]
% 4: kappa 		- isothermal coefficient of compresiiblity     [1/Pa]                                                                       
% 5: rho0	- reference density	 [kg/m3]/1000 = [g/cm3]		
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: density 	- density, [g/cm3]                                                                                                                                                                                                                                                      
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%
% CAUTION: IT IS APPROXIMATE FOR LIQUID MATERIALS
% IT IS APPLICABLE ONLY WITH KOLEV DATABASE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function rho_liq = density_liquid_pT(T,T0,p,beta,kappa,rho0)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
p0=1e5; %[Pa] if based on the KOLEV databse
rho_liq = rho0.*exp( -beta.*(T-T0)  + kappa.*(p-p0));

 % This equations assume constant vaulues of beta and kappa
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%