%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% X-Core v2.0 developed by Piotr Darnowski - all rights reserved
% Zirconium DiOxide Density
% Rev0 30-04-2019, Rev1 16-06-2023, Rev2 29-06-2023
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: T				      - temperature, [K]
% 2: mode			      - mode, see below - options 
% 3: p				      - pressure, [Pa]  
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: rho			      - density, [g/cc]
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% T                 - 300 K
% mode              - 'Default'
% p 				- 1e5 Pa
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% {1, 'Kolev'}
% {2, 'Powers'} 
% {3, 'MELCOR'}
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% rho_ZRO2(500)
% rho_ZRO2(1000,'Kolev')
% rho_ZRO2(300,1)
%rho_ZRO2([300,1000,1500,3000], 1)
%rho_ZRO2([300,1000,1500,3000], 2)
%rho_ZRO2([300,1000,1500,3000], 3)
% REFERENCES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% see REFERENCES.m 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function rho = rho_ZRO2(T,mode,p)

	import XCore.AtomicDensityPackage.*
	import XCore.Materials.Density.*

	rho_liq_pT = @density_liquid_pT; 

	if(nargin <1)
		T =300.0;  % Default Temperature 
	end
	if(nargin <2) 
		mode = 'MELCOR';  % Default mode
	end
	if(nargin <3)
		p = 1e5;    % Default pressure
	end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  T_melt = 2973.0;  %Kolev
  %T_melt = 2990.0; %MELCOR  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%if(strcmp(mode,'Kolev'))
switch mode
	case {1, 'Kolev'}
			rho0s = 5800.0;

			c = [7.8e-6 -2.34e-3; ...
			1.302e-5 -3.338e-2]; % C matrix
		
			[ ident, T0, rho0, rho0beta, beta, kappa, cpliq, INFO ] = MAT_MELT_DATABASE(4);
		
				for i=1:numel(T)
					%if( T(i)<=300) 		
					%elseif(and(T(i)>300,T(i)<= 1478.0))
					if(T(i)<= 1478.0) % Limited to 300K %Checked with MATPRO - Kolev contains error
						epsilon = c(1,2) + c(1,1).*T(i);
						rho(i) = rho0s.*(1.0-3.*epsilon);
						rho(i) = rho(i)./1000;
						
					elseif(and(T(i)>1478.0,T(i)<= T_melt)) %Checked with MATPRO - Kolev contains error
						epsilon = c(2,2) + c(2,1).*T(i);
						rho(i) = rho0s.*(1.0-3.*epsilon);
						rho(i) = rho(i)./1000;

					elseif( T(i)>T_melt) 	
	
						rho(i) = rho_liq_pT(T(i),T0,p,beta,kappa,rho0)./1000;			%[10]						
					end	
				end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		case {2, 'Powers'}
			for i=1:numel(T)
				rho(i) = 5.863./(1+  T(i).*7.0e-5);  %[1]
			end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		case {3, 'MELCOR', 'Default'}
			for i=1:numel(T)	
				rho(i) = 5.6; 						%[4] MELCOR 
			end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		otherwise
			error('No such mode');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


