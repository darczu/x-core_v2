%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% X-Core v2.0 developed by Piotr Darnowski - all rights reserved
% Uranium Metal Density  
% Rev 16-03-2019, Rev1 29-06-2023
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: T				- temperature, [K]
% 2: mode			- mode, see below
% 3: p				- pressure, [Pa]   
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: rho			- density, [g/cc]
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mode  			- 'MELCOR'  
% p 				- 1E5 
% T					- 300.0
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 'Powers'	or	1 	- simple correlation based on [1] 
% 'MELCOR'	or	2 	- MELCOR based [4]
% 'MELCOR1' or 3    - MELCOR based [4] but single constant value
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% den = rho_U(300,'MELCOR')
% den = rho_U(300,'Powers')
% den = rho_U(300)
% den = rho_U()
% T = [300,500,1000,1500, 3000, 3500];
% rho_U(T,1)
% rho_U(T,2)
% rho_U(T,3)
%REFERENCES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% see REFERENCES.m file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function rho = rho_U(T,mode,p)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
T_melt = 1406.0; %MELCOR

if(nargin < 1) 
   T = 300.0;
end
if(nargin < 2)
  mode = 'MELCOR';  %Default
end 
if(nargin < 3)
 p = 1e5;
end

NT = numel(T); 
rho = zeros(1,NT);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
switch mode
	case {1,'Powers'}
		for i=1:numel(T)
			rho(i) = 19.350 - (1.031e-3).*T(i);  %Powers
		end	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
	case {2,'MELCOR'}
% Reference - MELCOR Reference Manual [4]
	T_MELCOR = [
		273.15 
		298.0 
		366.3 
		477.4 
		588.6 
		699.7 
		810.8 
		921.9 
		1406.0 
		5000.0 
	];
	rho_MELCOR = [
		19080.0/1000.0 
		19050.0/1000.0 
		18970.0/1000.0 
		18870.0/1000.0 
		18760.0/1000.0 
		18640.0/1000.0 
		18500.0/1000.0 
		18330.0/1000.0 
		17580.0/1000.0 
		17580.0/1000.0 
	];
	
	% Linear inteprolation %vq = interp1(x,y,xi)
	for i=1:numel(T)
		rho(i) = interp1(T_MELCOR,rho_MELCOR,T(i));
		
		% No extrapolation is allowed
		if(T(i)>T_MELCOR(end))
			error('ERROR(#1): Material temperature above allowed,  273.15<T<5000');
		elseif (T(i)<T_MELCOR(1))
			error('ERROR(#2): MAterial temperature below allowed,  273.15<T<5000');
		end	
	end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
case {3,'MELCOR1'}			% Single value
	rho(:) = 18.210; % [4] MELCOR
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		
otherwise
	error('No function');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	