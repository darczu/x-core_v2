% X-Core v2.0 developed by Piotr Darnowski - all rights reserved
% Rev 20-07-2017%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% It calculates number density and density of SEPARATE materials - it calculates PROPORTIES OF SEPARATE MATERIALS
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1:	MATERIAL_i 		- material name
% 2:	TEMPERATURE 	- material temperature
% 3: MASS_i 			- vector with masses of materials
% 4:	
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1:	N_tot				- vector with atomic number densities of materials
% 2:	rho_mix			- vector with densities
% 3:	A_mix				- vector with atomic masses
% 4: n_tot				- number of moles
% MODEs %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 0 - 
% 1 - 
% 2 - 
% 3 - 
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% EXAMPLE 1: N_mixMAT({'SS'},4000)
% EXAMPLE 2: [N,rho, A ] = N_mixMAT({'SS' 'UO2'})
% EXAMPLE 3: [N,rho, A ] = N_mixMAT({'SS' 'UO2'},[300, 500])
% EXAMPLE 3: [N,rho, A, n ] = N_mixMAT({'SS' 'UO2'},[300, 500],[1000 300])
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [N_tot, rho_mix, A_mix, n_tot]= N_mixMAT(MATERIAL_i, TEMPERATURE,MASS_i)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%N_A = 0.602214179;  % [atoms/mole]Avogadro number
% global N_A;
run PHYSICAL_CONSTANTS.m

import XCore.AtomicDensityPackage.*


if(nargin<2) 
	TEMPERATURE = 300.0; 
% 2023 %	disp('WARNING (N_mixMAT): Default Temperature 300K'); 
elseif(nargin<3)
% 2023 %	disp('WARNING (N_mixMAT): no mass calculations');
	MASS_i=[];
	n_tot=[]; 
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INITIAL TESTS 
	if(~or(numel(TEMPERATURE)==numel(MATERIAL_i),numel(TEMPERATURE)==1))	 
		error('ERROR #1 (N_mixMAT): Temperature can be scalar or vector with number of values equal to number of material inputs'); 
	end 
	
	if(numel(TEMPERATURE)==1)
		TEMPERATURE = repmat(TEMPERATURE, [1 numel(MATERIAL_i)]);
	end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[MATERIAL, MATERIAL_NO] = MAT_LIST(); %Available MATERIALs

rho_mix =[]; A_mix =[]; N_tot =[]; n_tot=[];
for i=1:numel(MATERIAL_i)

	%rho_mix(i) = DENSITY_BASE(MATERIAL_i(i),TEMPERATURE(i));
	rho_mix(i) = rho_mixMAT([1],MATERIAL_i(i),TEMPERATURE(i),'af','AtomFrac','Complex');
	A_mix(i)    = MOLAR_BASE(MATERIAL_i(i),'XSDATA');
	N_tot(i) = N_mix(rho_mix(i),A_mix(i)); 
	
	if(nargin>2)
		n_tot(i) = N_mix(MASS_i(i),A_mix(i))./N_A; 
	end
end

