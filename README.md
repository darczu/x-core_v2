## README - PROJECT X-CORE v2.0 #
X-CORE v2.0 - MATLAB Toolbox for Nuclear Engineering 
VERSION 2.0.0.1 13-06-2023
% X-Core v2.0 developed by Piotr Darnowski - all rights reserved

Modified new version of the package is being under development.  
Motivation is to comeback to the idea of the X-Core package. Now works with packages create namespaces. Thanks to that user use popular dot notation for the package.  Thanks to that function are not easily accesible from outside. The same for other files which are present in folders.
 
Current package:
https://gitlab.com/darczu/x-core_v2  
Previous X-Core is available: 
https://gitlab.com/darczu/x-core 

# DEVELOPER
The X-Core 2.0 was developed by Piotr Darnowski  
Please reports bugs or issues.  
Contact: piotr.darnowski@pw.edu.pl  

 
# FUNCTIONALITY
Current version covers data and functions for selected matetials - see below:
- Databases cover materials used in nuclear engineering: AIC; AIR; B2O3; B2ZR; B4C; CON; CS; FE; FP; FPO2; GRP; He; INC; MOX;  Na; PAINT; Pb; PuO2; SS; SSOX; ThPuO2; ThUO2; U; UO2; ZR; ZRO2;
- Density database
- Specific Heat Capacity database
- Thermal Conductivity database
- Viscosity database
- Steam Tables - separate database using X-Steam Toolbox
- Atomic Number Density calculations sub-package
- Some older function with basic thermal data. e.g. is MAT_MELT_DATABASE() which case materials databse for severe accident from the Kolve textbook.
- Common functions package


# HOW TO CITE
Cite: P. Darnowski, X-Core v2 Code, 2023.  
@misc{[XCore2] , title = "X-Core Version 2.0", author={Darnowski, P}, year={2023}, url={https://gitlab.com/darczu/x-core_v2} }

# MANUAL
WARNING: MANUAL IS STILL NOT READY  

# LICENSE 
GNU GPL
https://gitlab.com/darczu/x-core_v2/-/blob/master/LICENSE.sdx  
Some Matlab packages, which are necesarry to use this package are included. For example XSteam package.  
All of there are available in the Internet, and proper license files were added.

# INSTLALLATION
Add /x-core_v2 folder to the MATLAB Path. Then you will be able to use the package.
You can use:
```matlab
addpath(genpath('./')); % Add when in folder 
% or
addpath(genpath('x-core_v2')); % Add from external folder
% or
addpath('./')
``` 
Sometimes it demands to execute it directly in command window.

# REFERENCES
Given in /doc/references.m 
In some cases, given in orginal function file



# HELP
Most functions have help section.
Example:
```matlab
help  XCore.Materials.ThermalConductivity.k_MOX
```
Example help content:
```matlab
>> help XCore.Materials.Density.rho_MOX
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  ISSUE: Review recommended, confirm source
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  Mixed DiOxide Fuel Density PuO2 + UO2  Rev 16-03-2019
  INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  1: T				- temperature, [K]
  2: y				- fraction of plutonium, [%/100], it is mole fraction (atomic)
  3: porosity		- porosity, [%/100] 
  4: p				- pressure, [Pa]  - THERE IS NO PRESSURE DEP.
  OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  1: rho			- density, [g/cc]
  DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  porosity 			- 0.0  
  p 				- 1E5
  y					- 0.05  == 5% of Plutonium
  T					- 300
  MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  No modes - correlation is based on Carbajo [3] (CONFIRM!)
  EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  rho = rho_MOX()
  rho = rho_MOX(400.0)
  rho = rho_MOX(400.0,0.05)
  rho = rho_MOX(400.0,0.05,0.05)
 REFERENCES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  see REFERENCES.m file
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 ```


# EXAMPLES AND TESTS

- Example 1:  Caclulates density and specific heat capacity of MOX fuel for 400 K

It is possible to use explicitly package reference:
```matlab 
XCore.Materials.Density.rho_MOX(400.0,0.05,0.05)  %  calculates density of MOX fuel for 400 K 
XCore.Materials.SpecificHeatCapacity.k_MOX(400.0) % calculates thermal conductivity of MOX fuel for 400K
```
Alternatively use import (recommended):
```matlab
import XCore.Materials.Density.*  % import density sub-package
import XCore.Materials.ThermalConductivity.*  % import therm cond sub-package  
rho_MOX(400.0,0.05,0.05)  
k_MOX(400.0)
```
Alternatively (use handles to function):
```matlab
fun1 = @XCore.Materials.Density.rho_MOX
fun2 = @XCore.Materials.ThermalConductivity.k_MOX
fun1(400.0,0.05,0.05)  
fun2(400.0)
```

For more basic examples see: examples/Example_1.m
For more see and run testing suite: examples/Testsuite_<version>.m

Testsuite shows an example to run every operational function in the toolbox. The user is recommended to run it.

```matlab
addpath(genpath('./')); % Add main xcore_v2 folder 
run examples/Testsuite_rev1.m;
```

Some older function can cause problems. Feel free to send me info.




