%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% X-Core v2.0 developed by Piotr Darnowski - all rights reserved
% Rev 29-09-2018
% Pure Uranium Thermal Conductivity  T [K] and rho [g/cc]
% Options: 'MELCOR'  'Wallenius'
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: T				      - temperature, [K]
% 2: p				      - pressure, [Pa]
% 3: mode			      - mode, see below - options 
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: rho			      - density, [g/cc]
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% T                 - 300 K
% p                 - 1e5 Pa
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1:
% 2
% 3: 
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 
%REFERENCES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% see REFERENCES.m file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [k] = k_U(T,option)

if(nargin < 2)
 option ='MELCOR';  %Default 'MELCOR'
end


if(strcmp(option,'MELCOR'))
% Reference - MELCOR Reference Manual [4]

	T_MELCOR = [
	273.15 
	298.0 
	366.3 
	421.9 
	477.4 
	533.0 
	588.6 
	644.1 
	699.7 
	755.2 
	810.8 
	866.3 
	921.9 
	977.4 
	1033.0 
	1406.0 
	5000.0  
	];
	k_MELCOR = [
	24.31 
	25.12 
	27.34 
	28.38 
	29.34 
	30.28 
	31.32 
	32.22 
	33.22 
	34.09 
	35.04 
	35.90 
	36.68 
	37.37 
	38.07 
	42.77 
	42.77 
	];

	% Linear inteprolation %vq = interp1(x,y,xi)
	for i=1:numel(T)
		out(i) = interp1(T_MELCOR,k_MELCOR,T(i));
		% No extrapolation is allowed
		if(T(i)>T_MELCOR(end))
			error('ERROR(#1): Material temperature above allowed,  273.15<T<5000');
		elseif (T(i)<T_MELCOR(1))
			error('ERROR(#2): Material temperature below allowed,  273.15<T<5000');
		end	
	end





elseif(strcmp(option,'Wallenius'))	

	k = 21.7+(1.59e-2).*T + (5.91e-6).*(T.^2); %Wallenius Chapter 6
	out = k;



end