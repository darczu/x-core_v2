% X-Core v2.0 developed by Piotr Darnowski - all rights reserved
% Rev 15-07-2017
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Materials database - mainly contains ISOTOPIC INVENTORY for materials
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% m - melcor type default materials: mat0(k)
% k  - special materials : mat1(k)
% n = genetric materials mat2(k)
%import XCore.AtomicDensityPackage.*
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
% SPECIAL MATERIALS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
import XCore.AtomicDensityPackage.*
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
k = 1; 		% FUKUSHIMA FISSION PRODUCTS
    mat1(k).name = {'Fukushima 1F3 FPs'};
	mat1(k).number = k;
	comp = [36085	2.04E+03
				 38089	2.19E+03
				 38090	3.53E+04
				 42099	2.49E+02
				 51125	4.09E+02
				 52329	6.71E+01 %
				 53131	5.05E+02  
				 52132	2.95E+02
			%	 53133	1.17E+02 %Not available in XSDATA ENDF file
				 54133	6.73E+02
				 55134	5.26E+03
				 55136	3.03E+01
				 55137	7.50E+04
				 56140	1.61E+03
				 57140	2.15E+02
		%		 55144	1.93E+04]; %Not available in XSDATA ENDF file
		];
	mat1(k).zaid = comp(:,1);
	mat1(k).comp = comp(:,2);
	mat1(k).sumc  = sum(comp(:,2));  %total mass
	mat1(k).frac = normalization(comp(:,2));
	mat1(k).dens  = [];  
	mat1(k).unitc = 'g';     
	mat1(k).unitd = 'N/A';
	mat1(k).size  = numel(comp(:,1));
	mat1(k).comm  = 'Masses of importnat FPs';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
k = 2; 	 	% FUKUSHIMA ACTINIDES 
	mat1(k).name = {'Fukushima 1F3 Actinides'};
	mat1(k).number = k;
	comp = [92235	1.69E+06
				 92236	2.85E+05
				 92238	8.91E+07
				 93237	2.20E+04
				 93239	5.01E+03
				 94238	8.73E+03
				 94239	4.52E+05
				 94240	1.61E+05
				 94241	8.26E+04
				 94242	2.80E+04
				 95241	4.40E+03
				 95243	3.86E+03
				 96242	8.48E+02
				 96244	9.05E+02];
	mat1(k).zaid = comp(:,1);
	mat1(k).comp = comp(:,2);
	
	mat1(k).sumc  = sum(comp(:,2));
	mat1(k).frac = normalization(comp(:,2));
	mat1(k).dens  = [];  
	mat1(k).unitc = 'g';     
    mat1(k).unitd = 'N/A';
	mat1(k).size  = numel(comp(:,1));
	mat1(k).comm  = 'Masses of actinides in the core';

	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
% DEFAULTS MATERIALS - MELCOR TYPE MATERIALS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	

	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
m = 1;   	% 'CRP' - MELCOR CONTROL RODS POISON - BORON CARBIDE
	mat0(m).name  = {'CRP'};
	mat0(m).number  = m;
	% atomic version B4C
	comp = [5010	4*0.199
				 5011	4*0.801
				 6012	1.0];
	% mass fraction
	%5010	-0.156522
	%5011	-0.626088
	%6012	-0.21739
	mat0(m).zaid  = comp(:,1);
	mat0(m).comp  = comp(:,2);
	mat0(m).sumc  = sum(comp(:,2));
	mat0(m).frac  = normalization(comp(:,2));
    mat0(m).dens  = []; 
	mat0(m).unitc = 'atoms/molecule';     
    mat0(m).unitd = 'N/A';
	mat0(m).size  = numel(comp(:,1));
	mat0(m).comm  = 'Boron Carbide - atoms defined';	

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
m = 2;   	% 'INC' - MELCOR INCONEL 600 [5]
	mat0(m).name  = {'INC'};
	mat0(m).number  = m;
	comp = [6000	0.004642
				 14000	0.006583
				 16000	0.000177
				 24000	0.169591
				 25055	0.006731
				 26000	0.081498
				 28000	0.727867
				 29000	0.00291];
	mat0(m).zaid  = comp(:,1);
	mat0(m).comp  = normalization(comp(:,2));
	mat0(m).sumc  = sum(comp(:,2));
	mat0(m).frac  = normalization(comp(:,2));
    mat0(m).dens  = []; 
	mat0(m).unitc = 'at%';     
    mat0(m).unitd = 'N/A';
	mat0(m).size  = numel(comp(:,1));
	mat0(m).comm  = 'Inconel 600';	

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
m = 3;   	% 'SS'  - MELCOR STAINLESS STEEL SS304
	mat0(m).name  = {'SS'};
	mat0(m).number  = m;
	comp = [6000	0.00183
			14000	0.009781
			15031	0.000408  %change
			16000	0.000257
			24000	0.200762
			25055	0.010001 %change
			26000	0.690375
			28000	0.086587];
	mat0(m).zaid  = comp(:,1);
	mat0(m).comp  = normalization(comp(:,2));
	mat0(m).sumc  = sum(comp(:,2));
	mat0(m).frac  = normalization(comp(:,2));
    mat0(m).dens  = []; 
	mat0(m).unitc = 'at%';     
    mat0(m).unitd = 'N/A';
	mat0(m).size  = numel(comp(:,1));
	mat0(m).comm  = 'SS but SS304';	
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
m = 4;   	% 'SSOX' - MELCOR STAINLESS STEEL OXIDE
	mat0(m).name  = {'SSOX'};
	mat0(m).number  = m;
	comp = [6000	0.00183
				14000	0.009781
				15031	0.000408	%change
				16000	0.000257
				24000	0.200762
				25055	0.010001	%change
				26000	0.690375
				28000	0.086587
				8016		2.0];
	mat0(m).zaid  = comp(:,1);
	mat0(m).comp  = comp(:,2);
	mat0(m).sumc  = sum(comp(:,2));
	mat0(m).frac  = normalization(comp(:,2));
    mat0(m).dens  = []; 
	mat0(m).unitc = 'atoms/molecule';     
    mat0(m).unitd = 'N/A';
	mat0(m).size  = numel(comp(:,1));
	mat0(m).comm  = 'SS304 Oxide - assumed SSO2';	



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
m = 5;   	% 'UO2' - MELCOR URANIUM DIOXIDE- BUT HERE - ACTINIDE OXIDE
	mat0(m).name  = {'UO2'};
	mat0(m).number  = m;
	comp = mat1(2).zaid;	
	comp = [comp, normalization(mat1(2).comp)];
	%comp
	%comp(:,2)
	%ZAID2A(comp(:,1))
	comp(:,2) = wf2af(comp(:,2), ZAID2A(comp(:,1)), -1);
	
	comp = [comp;
			     8016 2.0];
	mat0(m).zaid  = comp(:,1);
	mat0(m).comp  = comp(:,2);
	mat0(m).sumc  = sum(comp(:,2));
	mat0(m).frac  = normalization(comp(:,2));
    mat0(m).dens  = []; 
	mat0(m).unitc = 'atoms/molecule';     
    mat0(m).unitd = 'N/A';
	mat0(m).size  = numel(comp(:,1));
	mat0(m).comm  = 'Actinide vector based oxide it is in practice ActO2';	


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
m = 6;   	% 'ZR' - MELCOR ZIRCALLOY-4
	mat0(m).name  = {'ZR'};
	mat0(m).number  = m;
	comp = [8016	0.00679
			24000	0.001741
			26000	0.003242
			40000	0.977549
			50000	0.010677];
	mat0(m).zaid  = comp(:,1);
	mat0(m).comp  = normalization(comp(:,2));
	mat0(m).sumc  = sum(comp(:,2));
	mat0(m).frac  = normalization(comp(:,2));
    mat0(m).dens  = []; 
	mat0(m).unitc = 'at%';     
    mat0(m).unitd = 'N/A';
	mat0(m).size  = numel(comp(:,1));
	mat0(m).comm  = 'Zircalloy-4';	


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
m = 7;   	% 'ZRO2' - MELCOR ZIRCONIUM OXIDE - HERE IT IS ZIRCALLOY-4 OXIDE
	mat0(m).name  = {'ZRO2'};
	mat0(m).number  = m;
	comp = [8016	0.00679
			24000	0.001741
			26000	0.003242
			40000	0.977549
			50000	0.010677
			 8016	2.0];
	mat0(m).zaid  = comp(:,1);
	mat0(m).comp  = comp(:,2);
	mat0(m).sumc  = sum(comp(:,2));
	mat0(m).frac  = normalization(comp(:,2));
    mat0(m).dens  = []; 
	mat0(m).unitc = 'atoms/molecule';     
    mat0(m).unitd = 'N/A';
	mat0(m).size  = numel(comp(:,1));
	mat0(m).comm  = 'Zircalloy-4 O2';	

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
m = 8;   	% 'GRP' - MELCOR GRAPHITE
	mat0(m).name  = {'GRP'};
	mat0(m).number  = m;
	comp = [8016	0.0
			24000	0.0
			26000	0.0
			40000	0.0
			50000	0.0
			 8016	0.0];
	mat0(m).zaid  = comp(:,1);
	mat0(m).comp  = comp(:,2);
	mat0(m).sumc  = sum(comp(:,2));
	mat0(m).frac  = normalization(comp(:,2));
    mat0(m).dens  = []; 
	mat0(m).unitc = 'atoms/molecule';     
    mat0(m).unitd = 'N/A';
	mat0(m).size  = numel(comp(:,1));
	mat0(m).comm  = 'Graphite O2';	
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
m = 13;   	% 'FP' based on 1F3 fission products 
	mat0(m).name  = {'FP'};
	mat0(m).number  = m;
	comp = mat1(1).zaid;	
	comp = [comp, normalization(mat1(1).comp)];
	comp(:,2) = wf2af(comp(:,2), ZAID2A(comp(:,1)), -1);
	
	%comp = [comp;
	%		     8016 2.0];
	mat0(m).zaid  = comp(:,1);
	mat0(m).comp  = comp(:,2);
	mat0(m).sumc  = sum(comp(:,2));
	mat0(m).frac  = normalization(comp(:,2));
    mat0(m).dens  = []; 
	mat0(m).unitc = 'atoms/molecule';     
    mat0(m).unitd = 'N/A';
	mat0(m).size  = numel(comp(:,1));
	mat0(m).comm  = 'FPs vector based on 1F3';	
	
	
	
	
	

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% USER PROVIDED MATERIALS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
n = 1;
     mat2(n).name = 'fuel1';
	 comp = [92234	8.56E-06
             92235	1.12E-03
             92238	2.15E-02
             8016	4.53E-02];
	 mat2(n).number = n;
	 mat2(n).zaid = comp(:,1);
	 mat2(n).comp = comp(:,2);
	 mat2(n).size  = numel(comp(:,1));
	 mat2(n).dens = -10.0;
	mat2(n).moder = 0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	 
n = 2;
 % --- MATERIAL     
     mat2(n).name = 'fuel2';
     comp =[92234	7.69E-06
				 92235	1.01E-03
				 92238	2.16E-02
				 8016	4.52E-02];

     mat2(n).number = n;   
     mat2(n).zaid = comp(:,1);
     mat2(n).comp = comp(:,2);
     mat2(n).size = numel(comp(:,1));
	 mat2(n).dens = -11.0;
	mat2(n).moder = 0;
   
n = 3;   
     mat2(n).name = 'water1';
     comp = [1001 0.66667;
	 8016 0.33333 ];
     mat2(n).number = n;   
     mat2(n).zaid = comp(:,1);
     mat2(n).comp = comp(:,2);
     mat2(n).size = numel(comp(:,1));
	 mat2(n).dens = -0.9;
     mat2(n).moder = 1;

n = 4;     
     mat2(n).name = 'B4C';
     comp=	[  5010 -0.156522;
				   5011 -0.626088; 
				   6012 -0.217390];
     mat2(n).number = n;   
     mat2(n).zaid = comp(:,1);
     mat2(n).comp = comp(:,2);
     mat2(n).size = numel(comp(:,1));
	 mat2(n).dens = -2.52;
     mat2(n).moder = 0;

n = 5;     
     mat2(n).name = 'Zr_2';
    comp =  [  8016  0.000296;
					24000 0.000076;
					26000 0.000071;
					28000 0.000034;
					40000 0.042541;
					50000 0.000465 ];   
     mat2(n).number = n;   
     mat2(n).zaid = comp(:,1);
     mat2(n).comp = comp(:,2);
     mat2(n).size = numel(comp(:,1));
	 mat2(n).dens = -6.9;
     mat2(n).moder = 0;

n = 6;
     mat2(n).name = 'SS304';
     comp = [  6000 0.000160;
					14000 0.000858;
					15031 0.000036;
					16000 0.000023;
					24000 0.017605;
					25055 0.000877;
					26000 0.060538;
					28000 0.007593];    
     
     mat2(n).number = n;   
     mat2(n).zaid = comp(:,1);
     mat2(n).comp = comp(:,2);
     mat2(n).size = numel(comp(:,1));
	 mat2(n).dens = -8.0;
     mat2(n).moder = 0;
 
 
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 k_size =k;
 m_size =m;
 n_size=n;
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
 
 
 
 
%else
%	error ('No such flag_mode');
%end
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% VARIABLES TO STORE numberER OF MATERIALS


 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
%end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%FORMATKA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%{
		mat0(m).name  = '';
		mat0(m).number  = m;
		comp = [];
		mat0(m).zaid  = comp(:,1);
		mat0(m).comp  = comp(:,2);
		mat0(m).sumc  = sum(comp(:,2));	
		mat0(m).frac  = normalization(comp(:,2));
		mat0(m).dens  = []; 
		mat0(m).unitc = '';     
		mat0(m).unitd = 'N/A';
		mat0(m).size  = numel(comp(:,1));
		mat0(m).comm  = '';
%}
%{
		mat1(k).name = '';
		mat1(k).number = k;
		comp = [];
		mat1(k).zaid = comp(:,1);
		mat1(k).comp = comp(:,2);
		mat1(k).sumc  = sum(comp(:,2));
		mat1(k).frac = normalization(comp(:,2));
		mat1(k).dens  = ;  %to sprawdzic
		mat1(k).unitc = '';     
		mat1(k).unitd = '';
		mat1(k).size  = numel(comp(:,1));
		mat1(k).comm  = '';
%}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	 
	 
	 
	 
	 
	 
	 
%% ========================================================================
% DATABASE WITH FIXED MATERIALS
% ISOTOPIC INVENOTRY USER INPUT MATERIAL 
%% ========================================================================


%PROTOTYPE OF THE MATERIAL STRUCTURE - LIKE IN MODE 1- IN OLDE VERSION
%mat1(i).size
%mat1(i).name
%mat1(i).number
%mat1(i).zaid
%mat1(i).comp
%Example:
%{
    size: 4
    name: {'fuel1'}
    number: 1
    zaid: [4x1 double]
    comp: [4x1 double]
%}

%{

%Water

%
%

%{
		water1_name = 'water1';
		water1_rho = -0.44376; 
		water1 = [  1001 0.66667;
					8016 0.33333 ];
		%Boron carbide
		4C_name = 'B4C';
		4C_rho = 2.52*0.7;
		4C =	[  5010 -0.156522;
			5011 -0.626088; 
			6012 -0.217390];
		
		%Zircaloy-2
		r_2_name ='Zr_2';
		r_2 =  [  8016 0.000296;
			24000 0.000076;
			26000 0.000071;
			28000 0.000034;
			40000 0.042541;
			50000 0.000465 ];
		
		%Stainless Steel 304
		S304_name = 'SS304';
		S304 = [  6000 0.000160;
				14000 0.000858;
				15031 0.000036;
				16000 0.000023;
				24000 0.017605;
				25055 0.000877;
				26000 0.060538;
				28000 0.007593 ];
		
		
		%Fuel1 and Fuel2 - present in the mode 1 xlsx
		uel1_name ='fuel1';
		uel1_rho = 10.0;
		uel1 = [92234	8.56E-06
				92235	1.12E-03
				92238	2.15E-02
			8016	4.53E-02];
		
		uel2_name ='fuel2';
		uel2_rho = 11.0;
		uel2 =[92234	7.69E-06
			92235	1.01E-03
			92238	2.16E-02
			8016	4.52E-02];
			
		%}
		
		% Default boron for addition to water
		%{
		oron_rho = 1.0;
		oron = [5010   0.199
				5011   0.801];
		%}
		%% ======================================================================
		% === OECD/NEA BENCHMARK IIIC AND KACPER P. MATERIALS 
		%{
		
		% fuel1 material - OECD/NEA Benchmark 4.9 wt%
		mat fuel1 sum	tmp 9.000000e+02 	burn 1
		2234.09c	8.564900e-06
		2235.09c	1.122100e-03
		2238.09c	2.149500e-02
		016.09c		4.525200e-02
		
		% fuel2 material  - OECD/NEA Benchmark 4.4 wt%
		mat fuel2 sum	tmp 9.000000e+02 	burn 1
		2234.09c	7.691000e-06
		2235.09c	1.007600e-03
		2238.09c	2.160900e-02
		016.09c		4.524900e-02
		
		% fuel3 material  - OECD/NEA Benchmark 3.9wt%
		mat fuel3 sum	tmp 9.000000e+02 	burn 1
		2234.09c	6.817000e-06
		2235.09c	8.931500e-04
		2238.09c	2.172300e-02
		016.09c		4.524700e-02
		
		% fuel4 material  - OECD/NEA Benchmark 3.4wt%
		mat fuel4 sum	tmp 9.000000e+02 	burn 1
		2234.09c	5.943100e-06
		2235.09c	7.786500e-04
		2238.09c	2.183800e-02
		016.09c		4.524400e-02
		
		% fuel5 material   - OECD/NEA Benchmark 2.1wt%
		mat fuel5 sum	tmp 9.000000e+02 	burn 1
		2234.09c	3.670800e-06
		2235.09c	4.809400e-04
		2238.09c	2.213400e-02
		016.09c		4.523800e-02
		
		% fuel6 material   - OECD/NEA Benchmark 3.4wt% + Gd2O3 5.0wt%
		mat fuel6 sum	tmp 9.000000e+02 	burn 10
		2234.09c	5.542900e-06
		2235.09c	7.262200e-04
		2238.09c	2.036700e-02
		016.09c		4.467800e-02
		4154.09c	3.605100e-05
		4155.09c	2.447500e-09
		4156.09c	3.385200e-04
		4157.09c	2.588100e-04
		4158.09c	4.107900e-04
		4160.09c	3.648100e-04
		
		% cladding material  - ORGINAL CLADDING WAS DIFFERENT
		mat clad sum   
		%mat clad sum  tmp 559.0  %<=== to mozna zmieniac ?
		016.06c		2.9600004E-04
		4000.06c	7.6000005E-05
		6000.06c	7.0999995E-05
		8000.06c	3.3999998E-05
		0000.06c	4.2541003E-02
		0000.06c	4.6499997E-04
		
		%coolant additional data - recalculated with MATLAB XSTEAM
		%void 0  	600K		580K		559K	
		%				0.64941	0.69763	0.73969	g/cc
		%void 40 	600K		580K		559K	
		%				0.41877	0.43927	0.45843	g/cc
		%void 70 	600K		580K	559K	
		%				0.24579	0.2455	0.24748	g/cc
		%
		%Manual: The cross section libraries provided with the Serpent code are generated between
		%300K intervals and it is recommended that the closest temperature below the broadened value
		%is used as the basis. !!!!  PRZYCZYNA .03c
		
		%Benchmark: void fractions: 0,40,70% water
		% coolant material - in core coolant material - DIFFERENT IN BENCHMARK
		mat coolant -2.722400e-01 moder lwtr_coolant 1001 	tmp 5.589800e+02
		001.03c	 6.666700e-01
		016.03c  3.333300e-01
		%Orginal Benchmark definition - void 0%
		%1001.03c	 4.9316e-02
		%8016.03c	 2.4658e-02
		%Sum           7.3974e-02
		
		%Orginal Benchmark definition - void 40%
		%1001.03c	 3.0588e-02
		%8016.03c	 1.5294e-02
		%Sum           4.5882e-02
		
		%Orginal Benchmark definition - void 70%
		%1001.03c	 1.6542e-02
		%8016.03c	 8.2712e-03
		%Sum          2.48132e-02
		
		herm lwtr_coolant lwe7.10t
		
		
		
		% WCwall material - Water Channel Wall material  ORGINAL CLADDING WAS DIFFERENT
		mat WCwall sum
		016.03c	 4.047304e-05
		016.06c 2.555270e-04
		4000.03c	1.039173e-05
		4000.06c	6.560827e-05
		6000.03c	9.708061e-06
		6000.06c	6.129194e-05
		8000.03c	4.648931e-06
		8000.06c	2.935107e-05
		0000.03c	5.816769e-03
		0000.06c	3.672423e-02
		0000.03c	6.358096e-05
		0000.06c	4.014190e-04
		
		% moderator material - Water Channel Coolant
		% Benchmark: void fraction 0%
		mat moderator -7.397237e-01 moder lwtr_moderator 1001 	tmp 5.589800e+02
		001.03c	 6.666700e-01
		016.03c  3.333300e-01
		%Orginal Benchmark definition
		%1001.03c	 4.9316e-02
		%8016.03c	 2.4658e-02
		%Sum           7.39740e-02
		
		herm lwtr_moderator lwe7.10t
		
		% BOXwall material - Channel Box Wall  ORGINAL CLADDING WAS DIFFERENT
		mat BOXwall sum
		016.03c	 4.047304e-05
		016.06c	 2.555270e-04
		4000.03c	1.039173e-05
		4000.06c	6.560827e-05
		6000.03c	9.708061e-06
		6000.06c	6.129194e-05
		8000.03c	4.648931e-06
		8000.06c	2.935107e-05
		0000.03c	5.816769e-03
		0000.06c	3.672423e-02
		0000.03c	6.358096e-05
		0000.06c	4.014190e-04
		
		% BOXmoderator material - Water Moderator Outer Channel Box
		% Benchmark: void fraction 0%
		mat BOXmoderator -7.397237e-01 moder lwtr_BOXmoderator 1001 	tmp 5.589800e+02
		001.03c	6.666700e-01
		016.03c	3.333300e-01
		%Orginal Benchmark definition
		%1001.03c	 4.9316e-02
		%8016.03c	 2.4658e-02
		%Sum           7.39740e-02
		herm lwtr_BOXmoderator lwe7.10t
		
		%} 
		% === END OECD/NEA BENCHMARK IIIC AND KACPER P. MATERIALS 
		
		
		
		
		%% ======================================================================  
		
		% === PATRYK IGN. AP1000 AND JAKUB W. EPR INPUT  
		
		% --- Water at 2250 psia and 578.1 F:
		%{
		mat water  -0.7194  moder lwtr 1001 rgb 107 196 227
		1001.06c   2.0
		8016.06c   1.0
		therm lwtr lwe7.10t  %(550K) 
		%}
		
		% --- Low carbon steel SA-508 Cl. 3:
		%{
		mat sa508  -7.85  rgb 104 104 104
		6000.06c  -0.0019
		4000.06c  -0.0008
		5055.06c  -0.0135
		5031.06c  -0.00006
		6000.06c  -0.00002
		8000.06c  -0.0082
		4000.06c  -0.0017
		2000.06c  -0.0051
		6000.06c  -0.9996872
		%}
		
		% --- Stainless Steel Type 304:
		%{
		mat ss304  -7.889  rgb 138 196 193
		4028.06c  -9.5274E-04
		4029.06c  -4.8400E-05
		4030.06c  -3.1943E-05
		4050.06c  -7.6778E-04
		4052.06c  -1.4806E-02
		4053.06c  -1.6789E-03
		4054.06c  -4.1791E-04
		5055.06c  -1.7604E-03
		6054.06c  -3.4620E-03
		6056.06c  -5.4345E-02
		6057.06c  -1.2551E-03
		6058.06c  -1.6703E-04
		8058.06c  -5.6089E-03
		8060.06c  -2.1605E-03
		8061.06c  -9.3917E-05
		8062.06c  -2.9945E-04
		8064.06c  -7.6261E-05
		%}
		
		% --- Pyrex - borosilicate glass (B2O3-SiO2):
		%{
		mat pyrex  -2.299  burn 4  rgb 238 92 26
		5010.09c  -0.00699	%B-10
		5011.09c  -0.03207	%B-11
		8016.09c  -0.53902	%O
		3027.09c  -0.01167	%Al
		4000.09c  -0.37856	%Si
		9000.09c  -0.00332	%K
		1023.09c  -0.02837	%Na
		%}
		
		% --- Helium, Density (g/cc) 0.001598:
		%{
		mat helium  sum  rgb 255 255 255
		2004.06c  2.4044E-04
		%}
		
		% --- Air:
		%{
		mat air    -0.001205  rgb 255 255 255
		8016.06c  -0.231781
		7014.06c  -0.755268
		6000.06c  -0.000124
		18040.06c  -0.012827
		%}
		
		% --- Optimized ZIRLO, Density (g/cc) 6.55:
		%{
		mat zirlo  -6.55  rgb 167 167 167
		26000.06c  -0.001  	%Fe
		40000.06c  -0.9811 	%Zr
		41093.06c  -0.01   	%Nb
		50000.06c  -0.0067 	%Sn
		8016.06c  -0.0012 	%O
		%}
		
		% --- Fuel 2.35% Enriched, Density (g/cc) 10.45916:
		%{
		mat UO2_2   sum  burn 1 rgb 242 210 210
		92234.09c   4.4599E-06
		92235.09c   5.5511E-04
		92238.09c   2.2771E-02  
		8016.09c    4.6661E-02
		%}
		% --- Fuel 3.40% Enriched, Density (g/cc) 10.45916:
		%{
		mat UO2_4   sum  burn 1  rgb 254 76 76
		92234.09c   6.4525E-06
		92235.09c   8.0312E-04
		92238.09c   2.2524E-02
		8016.09c    4.6666E-02
		%}
		% --- Fuel 4.45% Enriched, Density (g/cc) 10.45916:
		%{
		mat UO2_5   sum  burn 1  rgb 200 0 0
		2234.09c   8.4451E-06
		2235.09c   1.0511E-03
		2238.09c   2.2276E-02
		8016.09c   4.6672E-02
		%}
		
		% === END PATRYK IGN. AP1000 AND JAKUB W. EPR INPUT  
		
		%% ======================================================================
		
		% === BASE ON BEAVERS INPUT BY VTT
		% Material compositions (created 13/02/18, Rev 1.0, 13/02/13)
		% NOTES: - O-18 missing from cross section libraries (taken out)
		%        - C-13 missing from cross section libraries (C-12 and C-13 converted to C-nat)
		%{
		% --- 975 ppm
		mix cool    rgb 100 150 250  
		water -0.999025
		oron -975E-6
		%}
		% ----- Table 4 — Fuel 1.6% Enriched, Density (g/cc) 10.31362
		%{
		mat fuel16  sum  rgb 150 50 50 burn 1
		U-234.09c  3.0131e-06
		U-235.09c  3.7503e-04
		U-238.09c  2.2626e-02
		O-16.09c  4.5896e-02
		O-17.09c  1.7483e-05
		% O-18.09c  9.4315e-05
		
		% ----- Table 5 — Fuel 2.4% Enriched, Density (g/cc) 10.29769
		
		mat fuel24  sum  rgb 150 150 50 burn 1
		U-234.09c  4.4843e-06
		U-235.09c  5.5815e-04
		U-238.09c  2.2408e-02
		O-16.09c  4.5829e-02
		O-17.09c  1.7457e-05
		% O-18.09c  9.4178e-05
		
		% ----- Table 6 — Fuel 3.1% Enriched, Density (g/cc) 10.301870
		
		mat fuel31  sum  rgb 50 50 150 burn 1
		U-234.09c  5.7988e-06
		U-235.09c  7.2176e-04
		U-238.09c  2.2254e-02
		O-16.09c  4.5851e-02
		O-17.09c  1.7466e-05
		% O-18.09c  9.4224e-05
		
		% --- Table 7 — Air, Density (g/cc) 0.000616
		
		mat air     sum  rgb 255 255 255
		% C-12.09c  6.7565e-09
		% C-13.09c  7.3076e-11
		C-nat.09c  6.8296e-09
		O-16.09c  5.2864e-06
		O-17.09c  2.0137e-09
		% O-18.09c  1.0863e-08
		N-14.09c  1.9681e-05
		N-15.09c  7.1900e-08
		Ar-36.09c  7.9414e-10
		Ar-38.09c  1.4915e-10
		Ar-40.09c  2.3506e-07
		
		% --- Air in instrumentation tube (used to score detectors)
		
		mat itube   sum  rgb 255 200 200
		% C-12.09c  6.7565e-09
		% C-13.09c  7.3076e-11
		C-nat.09c  6.8296e-09
		O-16.09c  5.2864e-06
		O-17.09c  2.0137e-09
		% O-18.09c  1.0863e-08
		N-14.09c  1.9681e-05
		N-15.09c  7.1900e-08
		Ar-36.09c  7.9414e-10
		Ar-38.09c  1.4915e-10
		Ar-40.09c  2.3506e-07
		
		% ----- Table 8 — Borosilicate Glass, Density (g/cc) 2.260000
		mat BP      sum  rgb 50 200 50 burn 1
		B-10.09c  9.6506e-04
		B-11.09c  3.9189e-03
		O-16.09c  4.6511e-02
		O-17.09c  1.7717e-05
		% O-18.09c  9.5581e-05
		Al-27.09c  1.7352e-03
		Si-28.09c  1.6924e-02
		Si-29.09c  8.5977e-04
		Si-30.09c  5.6743e-04
		
		% ----- Table 9 — Ag-In-Cd Control Rods, Density (g/cc) 10.160000
		
		mat AIC     sum  rgb 200 50 200
		g-109.09c  2.1854e-02
		g-107.09c  2.3523e-02
		n-113.09c  3.4291e-04
		n-115.09c  7.6504e-03
		d-106.09c  3.4019e-05
		d-108.09c  2.4221e-05
		d-110.09c  3.3991e-04
		d-111.09c  3.4835e-04
		d-112.09c  6.5669e-04
		d-113.09c  3.3257e-04
		d-114.09c  7.8188e-04
		d-116.09c  2.0384e-04
		
		
		% ----- Table 10 — Helium, Density (g/cc) 0.001598
		
		mat helium  sum  rgb 200 200 20
		He-4.09c  2.4044e-04
		
		% ----- Table 11 — Inconel 718, Density (g/cc) 8.2000
		
		mat inc718  sum  rgb 150 150 150
		Si-28.09c  5.6753e-04
		Si-29.09c  2.8831e-05
		Si-30.09c  1.9028e-05
		Cr-50.09c  7.8239e-04
		Cr-52.09c  1.5088e-02
		Cr-53.09c  1.7108e-03
		Cr-54.09c  4.2586e-04
		Mn-55.09c  7.8201e-04
		Fe-54.09c  1.4797e-03
		Fe-56.09c  2.3229e-02
		Fe-57.09c  5.3645e-04
		Fe-58.09c  7.1392e-05
		Ni-58.09c  2.9320e-02
		Ni-60.09c  1.1294e-02
		Ni-61.09c  4.9094e-04
		Ni-62.09c  1.5653e-03
		Ni-64.09c  3.9864e-04
		
		
		% ----- Table 13 — Zircaloy 4, Density (g/cc) 6.55
		
		mat zirc4   sum  rgb 200 200 200
		O-16.09c  3.0743e-04
		O-17.09c  1.1711e-07
		% O-18.09c  6.3176e-07
		Cr-50.09c  3.2962e-06
		Cr-52.09c  6.3564e-05
		Cr-53.09c  7.2076e-06
		Cr-54.09c  1.7941e-06
		Fe-54.09c  8.6699e-06
		Fe-56.09c  1.3610e-04
		Fe-57.09c  3.1431e-06
		Fe-58.09c  4.1829e-07
		Zr-90.09c  2.1827e-02
		Zr-91.09c  4.7600e-03
		Zr-92.09c  7.2758e-03
		Zr-94.09c  7.3734e-03
		Zr-96.09c  1.1879e-03
		n-112.09c  4.6735e-06
		n-114.09c  3.1799e-06
		n-115.09c  1.6381e-06
		n-116.09c  7.0055e-05
		n-117.09c  3.7003e-05
		n-118.09c  1.1669e-04
		n-119.09c  4.1387e-05
		n-120.09c  1.5697e-04
		n-122.09c  2.2308e-05
		n-124.09c  2.7897e-05
		
		% ----- Table 14 — Borated Water, Density (g/cc) 0.740582
		*
		mat cool    sum  rgb 100 150 250  moder lwtr 1001
		B-10.09c  8.0042e-06
		B-11.09c  3.2218e-05
		H-1.09c  4.9457e-02
		H-2.09c  7.4196e-06
		O-16.09c  2.4672e-02
		O-17.09c  9.3982e-06
		% O-18.09c  5.0701e-05
		/
		
		mat water   sum  moder lwtr 1001
		H-1.09c  4.9457e-02
		H-2.09c  7.4196e-06
		O-16.09c  2.4672e-02
		O-17.09c  9.3982e-06
		% O-18.09c  5.0701e-05
		
		mat boron   1.0
		B-10.09c  0.1990
		B-11.09c  0.8010
		
		% ----- Table 15 — Carbon Steel, Density (g/cc) 7.800
		
		mat steel   sum  rgb 100 100 100
		% C-12.09c  9.6726e-04
		% C-13.09c  1.0462e-05
		C-nat.09c  9.7772e-04
		Si-28.09c  4.2417e-04
		Si-29.09c  2.1548e-05
		Si-30.09c  1.4221e-05
		Mn-55.09c  1.1329e-03
		P-31.09c  3.7913e-05
		Mo-92.09c  3.7965e-05
		Mo-94.09c  2.3725e-05
		Mo-96.09c  4.2875e-05
		Mo-97.09c  2.4573e-05
		Mo-98.09c  6.2179e-05
		Mo-100.09c  2.4856e-05
		Fe-54.09c  4.7714e-03
		Fe-56.09c  7.4900e-02
		Fe-57.09c  1.7298e-03
		Fe-58.09c  2.3020e-04
		Ni-58.09c  2.9965e-04
		Ni-60.09c  1.1543e-04
		Ni-61.09c  5.0175e-06
		Ni-62.09c  1.5998e-05
		Ni-64.09c  4.0742e-06
		%}
		% === END BASE ON BEAVERS INPUT BY VTT
		
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%{
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
k = 1; % FUKUSHIMA FISSION PRODUCTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	mat1(k).name = 'Fukushima FPs';
	mat1(k).numb = k;
	comp = [36085	2.04E+03
			38089	2.19E+03
			38090	3.53E+04
			42099	2.49E+02
			51125	4.09E+02
			52329	6.71E+01
			53131	5.05E+02
			52132	2.95E+02
			53133	1.17E+02
			54133	6.73E+02
			55134	5.26E+03
			55136	3.03E+01
			55137	7.50E+04
			56140	1.61E+03
			57140	2.15E+02
			55144	1.93E+04];
	mat1(k).zaid = comp(:,1);
	mat1(k).comp = comp(:,2);
	mat1(k).sumc  = sum(comp(:,2));
	mat1(k).frac = normalization(comp(:,2));
	mat1(k).dens  = [];  
	mat1(k).unitc = 'kg';     
    mat1(k).unitd = 'N/A';
	mat1(k).size  = numel(comp(:,1));
	mat1(k).comm  = 'Masses of importnat FPs';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
k = 2; % FUKUSHIMA FUEL MATERIAL 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	mat1(k).name = 'Actinides Fukushima';
	mat1(k).numb = k;
	comp = [92235	1.69E+06
			92236	2.85E+05
			92238	8.91E+07
			93237	2.20E+04
			93239	5.01E+03
			94238	8.73E+03
			94239	4.52E+05
			94240	1.61E+05
			94241	8.26E+04
			94242	2.80E+04
			95241	4.40E+03
			95243	3.86E+03
			96242	8.48E+02
			96244	9.05E+02];
	mat1(k).zaid = comp(:,1);
	mat1(k).comp = comp(:,2);
	
	mat1(k).sumc  = sum(comp(:,2));
	mat1(k).frac = normalization(comp(:,2));
	mat1(k).dens  = [];  
	mat1(k).unitc = 'kg';     
    mat1(k).unitd = 'N/A';
	mat1(k).size  = numel(comp(:,1));
	mat1(k).comm  = 'Masses of actinides in the core';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
% DEFAULTS MATERIALS - MELCOR MATERIALS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
m = 1;   % 'CRP' - CONTROL RODS POISON - BORON CARBIDE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	mat0(m).name  = 'CRP';
	mat0(m).numb  = m;
	% atomic version
	comp = [5010	4*0.199
			5011	4*0.801
			6012	1.0];
	% mass fraction
	%5010	-0.156522
	%5011	-0.626088
	%6012	-0.21739
	mat0(m).zaid  = comp(:,1);
	mat0(m).comp  = comp(:,2);
	mat0(m).sumc  = sum(comp(:,2));
	mat0(m).frac  = normalization(comp(:,2));
    mat0(m).dens  = []; 
	mat0(m).unitc = 'atoms/molecule';     
    mat0(m).unitd = 'N/A';
	mat0(m).size  = numel(comp(:,1));
	mat0(m).comm  = 'Boron Carbide - atoms defined';	

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
m = 2;   % 'INC' - INCONEL 600 [5]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	mat0(m).name  = 'INC';
	mat0(m).numb  = m;
	comp = [6000	0.004642
			14000	0.006583
			16000	0.000177
			24000	0.169591
			25000	0.006731
			26000	0.081498
			28000	0.727867
			29000	0.00291];
	mat0(m).zaid  = comp(:,1);
	mat0(m).comp  = comp(:,2);
	mat0(m).sumc  = sum(comp(:,2));
	mat0(m).frac  = normalization(comp(:,2));
    mat0(m).dens  = []; 
	mat0(m).unitc = 'at%';     
    mat0(m).unitd = 'N/A';
	mat0(m).size  = numel(comp(:,1));
	mat0(m).comm  = 'Inconel 600';	

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
m = 3;   % 'SS' - STAINLESS STEEL SS304
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	mat0(m).name  = 'SS';
	mat0(m).numb  = m;
	comp = [6000	0.00183
			14000	0.009781
			15000	0.000408
			16000	0.000257
			24000	0.200762
			25000	0.010001
			26000	0.690375
			28000	0.086587];
	mat0(m).zaid  = comp(:,1);
	mat0(m).comp  = comp(:,2);
	mat0(m).sumc  = sum(comp(:,2));
	mat0(m).frac  = normalization(comp(:,2));
    mat0(m).dens  = []; 
	mat0(m).unitc = 'at%';     
    mat0(m).unitd = 'N/A';
	mat0(m).size  = numel(comp(:,1));
	mat0(m).comm  = 'SS304';	
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
m = 4;   % 'SSOX' - STAINLESS STEEL OXIDE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	mat0(m).name  = 'SSOX';
	mat0(m).numb  = m;
	comp = [6000	0.00183
			14000	0.009781
			15000	0.000408
			16000	0.000257
			24000	0.200762
			25000	0.010001
			26000	0.690375
			28000	0.086587
			 8016	2.0];
	mat0(m).zaid  = comp(:,1);
	mat0(m).comp  = comp(:,2);
	mat0(m).sumc  = sum(comp(:,2));
	mat0(m).frac  = normalization(comp(:,2));
    mat0(m).dens  = []; 
	mat0(m).unitc = 'atoms/molecule';     
    mat0(m).unitd = 'N/A';
	mat0(m).size  = numel(comp(:,1));
	mat0(m).comm  = 'SS304 Oxide';	



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
m = 5;   % 'UO2' - URANIUM DIOXIDE- BUT HERE - ACTINIDE OXIDE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	mat0(m).name  = 'UO2';
	mat0(m).numb  = m;
	comp = mat1(2).zaid;
	comp = [comp, normalization(mat1(2).comp)];
	comp = [comp;
			8016 2.0];
	mat0(m).zaid  = comp(:,1);
	mat0(m).comp  = comp(:,2);
	mat0(m).sumc  = sum(comp(:,2));
	mat0(m).frac  = normalization(comp(:,2));
    mat0(m).dens  = []; 
	mat0(m).unitc = 'atoms/molecule';     
    mat0(m).unitd = 'N/A';
	mat0(m).size  = numel(comp(:,1));
	mat0(m).comm  = 'Actinide vector based oxide it is in practice ActO2';	


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
m = 6;   % 'ZR' - HERE: ZIRCALLOY-4
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	mat0(m).name  = 'ZR';
	mat0(m).numb  = m;
	comp = [8000	0.00679
			24000	0.001741
			26000	0.003242
			40000	0.977549
			50000	0.010677];
			
	mat0(m).zaid  = comp(:,1);
	mat0(m).comp  = comp(:,2);
	mat0(m).sumc  = sum(comp(:,2));
	mat0(m).frac  = normalization(comp(:,2));
    mat0(m).dens  = []; 
	mat0(m).unitc = 'at%';     
    mat0(m).unitd = 'N/A';
	mat0(m).size  = numel(comp(:,1));
	mat0(m).comm  = 'Zircalloy-4';	


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
m = 7;   % 'ZRO2' - ZIRCONIUM OXIDE - HERE IT IS ZIRCALLOY-4 OXIDE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	mat0(m).name  = 'ZR';
	mat0(m).numb  = m;
	comp = [8000	0.00679
			24000	0.001741
			26000	0.003242
			40000	0.977549
			50000	0.010677
			 8016	2.0];
	mat0(m).zaid  = comp(:,1);
	mat0(m).comp  = comp(:,2);
	mat0(m).sumc  = sum(comp(:,2));
	mat0(m).frac  = normalization(comp(:,2));
    mat0(m).dens  = []; 
	mat0(m).unitc = 'atoms/molecule';     
    mat0(m).unitd = 'N/A';
	mat0(m).size  = numel(comp(:,1));
	mat0(m).comm  = 'Zircalloy-4 O2';	


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%{
		mat0(m).name  = '';
		mat0(m).numb  = m;
		comp = [];
		mat0(m).zaid  = comp(:,1);
		mat0(m).comp  = comp(:,2);
		mat0(m).sumc  = sum(comp(:,2));	
		mat0(m).frac  = normalization(comp(:,2));
		mat0(m).dens  = []; 
		mat0(m).unitc = '';     
		mat0(m).unitd = 'N/A';
		mat0(m).size  = numel(comp(:,1));
		mat0(m).comm  = '';
%}
%{
		mat1(k).name = '';
		mat1(k).numb = k;
		comp = [];
		mat1(k).zaid = comp(:,1);
		mat1(k).comp = comp(:,2);
		mat1(k).sumc  = sum(comp(:,2));
		mat1(k).frac = normalization(comp(:,2));
		mat1(k).dens  = ;  %to sprawdzic
		mat1(k).unitc = '';     
		mat1(k).unitd = '';
		mat1(k).size  = numel(comp(:,1));
		mat1(k).comm  = '';
%}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GENERIC MATERIALS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	k=k+1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     mat2(k).name = 'fuel1';
	 comp = [92234	8.56E-06
             92235	1.12E-03
             92238	2.15E-02
             8016	4.53E-02];
	 mat2(k).numb = k;
	 mat2(k).zaid = comp(:,1);
	 mat2(k).comp = comp(:,2);
	 mat2(k).size  = numel(comp(:,1));
	 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	 
    k=k+1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 % --- MATERIAL     
     mat2(k).name = 'fuel2';
     comp =[92234	7.69E-06
            92235	1.01E-03
            92238	2.16E-02
             8016	4.52E-02];
     
     mat2(k).numb = k;   
     mat2(k).zaid = comp(:,1);
     mat2(k).comp = comp(:,2);
     mat2(k).size = numel(comp(:,1));
     k=k+1;
     
% --- MATERIAL 
     mat2(k).name = 'water1';
     comp = [1001 0.66667;
             8016 0.33333 ];
     
     mat2(k).numb = k;   
     mat2(k).zaid = comp(:,1);
     mat2(k).comp = comp(:,2);
     mat2(k).size = numel(comp(:,1));
     k=k+1;
     
% --- MATERIAL 
     mat2(k).name = 'B4C';
     comp=	[  5010 -0.156522;
               5011 -0.626088; 
               6012 -0.217390];
     mat2(k).numb = k;   
     mat2(k).zaid = comp(:,1);
     mat2(k).comp = comp(:,2);
     mat2(k).size = numel(comp(:,1));
     k=k+1;
     
% --- MATERIAL 
     mat2(k).name = 'Zr_2';
    comp =  [  8016 0.000296;
	  24000 0.000076;
	  26000 0.000071;
	  28000 0.000034;
	  40000 0.042541;
	  50000 0.000465 ];   
     mat2(k).numb = k;   
     mat2(k).zaid = comp(:,1);
     mat2(k).comp = comp(:,2);
     mat2(k).size = numel(comp(:,1));
     k=k+1;
	 
% --- MATERIAL 
     mat2(k).name = 'SS304';
     comp = [  6000 0.000160;
		  14000 0.000858;
		  15031 0.000036;
		  16000 0.000023;
		  24000 0.017605;
		  25055 0.000877;
		  26000 0.060538;
		  28000 0.007593];    
     
     mat2(k).numb = k;   
     mat2(k).zaid = comp(:,1);
     mat2(k).comp = comp(:,2);
     mat2(k).size = numel(comp(:,1));
     k=k+1;
     

	 
	 
	 
	 
%% ========================================================================
% DATABASE WITH FIXED MATERIALS
% ISOTOPIC INVENOTRY USER INPUT MATERIAL 
%% ========================================================================


%PROTOTYPE OF THE MATERIAL STRUCTURE - LIKE IN MODE 1- IN OLDE VERSION
%mat1(i).size
%mat1(i).name
%mat1(i).numb
%mat1(i).zaid
%mat1(i).comp
%Example:
%{
    size: 4
    name: {'fuel1'}
    numb: 1
    zaid: [4x1 double]
    comp: [4x1 double]
%}

%{

%Water
water1_name = 'water1';
water1_rho = -0.44376; 
water1 = [  1001 0.66667;
	        8016 0.33333 ];
   
%Boron carbide
B4C_name = 'B4C';
B4C_rho = 2.52*0.7;
B4C =	[  5010 -0.156522;
	   5011 -0.626088; 
	   6012 -0.217390];
 
%Zircaloy-2
Zr_2_name ='Zr_2';
Zr_2 =  [  8016 0.000296;
	  24000 0.000076;
	  26000 0.000071;
	  28000 0.000034;
	  40000 0.042541;
	  50000 0.000465 ];

%Stainless Steel 304
SS304_name = 'SS304';
SS304 = [  6000 0.000160;
		  14000 0.000858;
		  15031 0.000036;
		  16000 0.000023;
		  24000 0.017605;
		  25055 0.000877;
		  26000 0.060538;
		  28000 0.007593 ];
  

%Fuel1 and Fuel2 - present in the mode 1 xlsx
fuel1_name ='fuel1';
fuel1_rho = 10.0;
fuel1 = [92234	8.56E-06
         92235	1.12E-03
         92238	2.15E-02
        8016	4.53E-02];
    
fuel2_name ='fuel2';
fuel2_rho = 11.0;
fuel2 =[92234	7.69E-06
        92235	1.01E-03
        92238	2.16E-02
        8016	4.52E-02];
     
 %}
    
    % Default boron for addition to water
%{
boron_rho = 1.0;
boron = [5010   0.199
         5011   0.801];
%}
%% ======================================================================
% === OECD/NEA BENCHMARK IIIC AND KACPER P. MATERIALS 
%{

% fuel1 material - OECD/NEA Benchmark 4.9 wt%
mat fuel1 sum	tmp 9.000000e+02 	burn 1
92234.09c	8.564900e-06
92235.09c	1.122100e-03
92238.09c	2.149500e-02
8016.09c		4.525200e-02

% fuel2 material  - OECD/NEA Benchmark 4.4 wt%
mat fuel2 sum	tmp 9.000000e+02 	burn 1
92234.09c	7.691000e-06
92235.09c	1.007600e-03
92238.09c	2.160900e-02
8016.09c		4.524900e-02

% fuel3 material  - OECD/NEA Benchmark 3.9wt%
mat fuel3 sum	tmp 9.000000e+02 	burn 1
92234.09c	6.817000e-06
92235.09c	8.931500e-04
92238.09c	2.172300e-02
8016.09c		4.524700e-02

% fuel4 material  - OECD/NEA Benchmark 3.4wt%
mat fuel4 sum	tmp 9.000000e+02 	burn 1
92234.09c	5.943100e-06
92235.09c	7.786500e-04
92238.09c	2.183800e-02
8016.09c		4.524400e-02

% fuel5 material   - OECD/NEA Benchmark 2.1wt%
mat fuel5 sum	tmp 9.000000e+02 	burn 1
92234.09c	3.670800e-06
92235.09c	4.809400e-04
92238.09c	2.213400e-02
8016.09c		4.523800e-02

% fuel6 material   - OECD/NEA Benchmark 3.4wt% + Gd2O3 5.0wt%
mat fuel6 sum	tmp 9.000000e+02 	burn 10
92234.09c	5.542900e-06
92235.09c	7.262200e-04
92238.09c	2.036700e-02
8016.09c		4.467800e-02
64154.09c	3.605100e-05
64155.09c	2.447500e-09
64156.09c	3.385200e-04
64157.09c	2.588100e-04
64158.09c	4.107900e-04
64160.09c	3.648100e-04

% cladding material  - ORGINAL CLADDING WAS DIFFERENT
mat clad sum   
%mat clad sum  tmp 559.0  %<=== to mozna zmieniac ?
8016.06c		2.9600004E-04
24000.06c	7.6000005E-05
26000.06c	7.0999995E-05
28000.06c	3.3999998E-05
40000.06c	4.2541003E-02
50000.06c	4.6499997E-04

%coolant additional data - recalculated with MATLAB XSTEAM
%void 0  	600K		580K		559K	
%				0.64941	0.69763	0.73969	g/cc
%void 40 	600K		580K		559K	
%				0.41877	0.43927	0.45843	g/cc
%void 70 	600K		580K	559K	
%				0.24579	0.2455	0.24748	g/cc
%
%Manual: The cross section libraries provided with the Serpent code are generated between
%300K intervals and it is recommended that the closest temperature below the broadened value
%is used as the basis. !!!!  PRZYCZYNA .03c

%Benchmark: void fractions: 0,40,70% water
% coolant material - in core coolant material - DIFFERENT IN BENCHMARK
mat coolant -2.722400e-01 moder lwtr_coolant 1001 	tmp 5.589800e+02
1001.03c	 6.666700e-01
8016.03c  3.333300e-01
%Orginal Benchmark definition - void 0%
%1001.03c	 4.9316e-02
%8016.03c	 2.4658e-02
%Sum           7.3974e-02

%Orginal Benchmark definition - void 40%
%1001.03c	 3.0588e-02
%8016.03c	 1.5294e-02
%Sum           4.5882e-02

%Orginal Benchmark definition - void 70%
%1001.03c	 1.6542e-02
%8016.03c	 8.2712e-03
%Sum          2.48132e-02

therm lwtr_coolant lwe7.10t



% WCwall material - Water Channel Wall material  ORGINAL CLADDING WAS DIFFERENT
mat WCwall sum
8016.03c	 4.047304e-05
8016.06c 2.555270e-04
24000.03c	1.039173e-05
24000.06c	6.560827e-05
26000.03c	9.708061e-06
26000.06c	6.129194e-05
28000.03c	4.648931e-06
28000.06c	2.935107e-05
40000.03c	5.816769e-03
40000.06c	3.672423e-02
50000.03c	6.358096e-05
50000.06c	4.014190e-04

% moderator material - Water Channel Coolant
% Benchmark: void fraction 0%
mat moderator -7.397237e-01 moder lwtr_moderator 1001 	tmp 5.589800e+02
1001.03c	 6.666700e-01
8016.03c  3.333300e-01
%Orginal Benchmark definition
%1001.03c	 4.9316e-02
%8016.03c	 2.4658e-02
%Sum           7.39740e-02

therm lwtr_moderator lwe7.10t

% BOXwall material - Channel Box Wall  ORGINAL CLADDING WAS DIFFERENT
mat BOXwall sum
8016.03c	 4.047304e-05
8016.06c	 2.555270e-04
24000.03c	1.039173e-05
24000.06c	6.560827e-05
26000.03c	9.708061e-06
26000.06c	6.129194e-05
28000.03c	4.648931e-06
28000.06c	2.935107e-05
40000.03c	5.816769e-03
40000.06c	3.672423e-02
50000.03c	6.358096e-05
50000.06c	4.014190e-04

% BOXmoderator material - Water Moderator Outer Channel Box
% Benchmark: void fraction 0%
mat BOXmoderator -7.397237e-01 moder lwtr_BOXmoderator 1001 	tmp 5.589800e+02
1001.03c	6.666700e-01
8016.03c	3.333300e-01
%Orginal Benchmark definition
%1001.03c	 4.9316e-02
%8016.03c	 2.4658e-02
%Sum           7.39740e-02
therm lwtr_BOXmoderator lwe7.10t

%} 
% === END OECD/NEA BENCHMARK IIIC AND KACPER P. MATERIALS 
 



%% ======================================================================  
  
% === PATRYK IGN. AP1000 AND JAKUB W. EPR INPUT  
  
% --- Water at 2250 psia and 578.1 F:
%{
 mat water  -0.7194  moder lwtr 1001 rgb 107 196 227
 1001.06c   2.0
 8016.06c   1.0
 therm lwtr lwe7.10t  %(550K) 
%}

% --- Low carbon steel SA-508 Cl. 3:
%{
mat sa508  -7.85  rgb 104 104 104
 6000.06c  -0.0019
14000.06c  -0.0008
25055.06c  -0.0135
15031.06c  -0.00006
16000.06c  -0.00002
28000.06c  -0.0082
24000.06c  -0.0017
42000.06c  -0.0051
26000.06c  -0.9996872
%}

% --- Stainless Steel Type 304:
%{
mat ss304  -7.889  rgb 138 196 193
14028.06c  -9.5274E-04
14029.06c  -4.8400E-05
14030.06c  -3.1943E-05
24050.06c  -7.6778E-04
24052.06c  -1.4806E-02
24053.06c  -1.6789E-03
24054.06c  -4.1791E-04
25055.06c  -1.7604E-03
26054.06c  -3.4620E-03
26056.06c  -5.4345E-02
26057.06c  -1.2551E-03
26058.06c  -1.6703E-04
28058.06c  -5.6089E-03
28060.06c  -2.1605E-03
28061.06c  -9.3917E-05
28062.06c  -2.9945E-04
28064.06c  -7.6261E-05
%}

% --- Pyrex - borosilicate glass (B2O3-SiO2):
%{
mat pyrex  -2.299  burn 4  rgb 238 92 26
 5010.09c  -0.00699	%B-10
 5011.09c  -0.03207	%B-11
 8016.09c  -0.53902	%O
13027.09c  -0.01167	%Al
14000.09c  -0.37856	%Si
19000.09c  -0.00332	%K
11023.09c  -0.02837	%Na
%}

% --- Helium, Density (g/cc) 0.001598:
%{
    mat helium  sum  rgb 255 255 255
    2004.06c  2.4044E-04
%}

% --- Air:
%{
    mat air    -0.001205  rgb 255 255 255
    8016.06c  -0.231781
    7014.06c  -0.755268
    6000.06c  -0.000124
    18040.06c  -0.012827
%}

% --- Optimized ZIRLO, Density (g/cc) 6.55:
%{
    mat zirlo  -6.55  rgb 167 167 167
    26000.06c  -0.001  	%Fe
    40000.06c  -0.9811 	%Zr
    41093.06c  -0.01   	%Nb
    50000.06c  -0.0067 	%Sn
    8016.06c  -0.0012 	%O
 %}

% --- Fuel 2.35% Enriched, Density (g/cc) 10.45916:
%{
    mat UO2_2   sum  burn 1 rgb 242 210 210
    92234.09c   4.4599E-06
    92235.09c   5.5511E-04
    92238.09c   2.2771E-02  
    8016.09c    4.6661E-02
%}
% --- Fuel 3.40% Enriched, Density (g/cc) 10.45916:
%{
    mat UO2_4   sum  burn 1  rgb 254 76 76
    92234.09c   6.4525E-06
    92235.09c   8.0312E-04
    92238.09c   2.2524E-02
    8016.09c    4.6666E-02
%}
% --- Fuel 4.45% Enriched, Density (g/cc) 10.45916:
%{
mat UO2_5   sum  burn 1  rgb 200 0 0
92234.09c   8.4451E-06
92235.09c   1.0511E-03
92238.09c   2.2276E-02
 8016.09c   4.6672E-02
%}

% === END PATRYK IGN. AP1000 AND JAKUB W. EPR INPUT  

%% ======================================================================

% === BASE ON BEAVERS INPUT BY VTT
% Material compositions (created 13/02/18, Rev 1.0, 13/02/13)
% NOTES: - O-18 missing from cross section libraries (taken out)
%        - C-13 missing from cross section libraries (C-12 and C-13 converted to C-nat)
%{
% --- 975 ppm
mix cool    rgb 100 150 250  
water -0.999025
boron -975E-6
%}
% ----- Table 4 — Fuel 1.6% Enriched, Density (g/cc) 10.31362
%{
mat fuel16  sum  rgb 150 50 50 burn 1
 U-234.09c  3.0131e-06
 U-235.09c  3.7503e-04
 U-238.09c  2.2626e-02
  O-16.09c  4.5896e-02
  O-17.09c  1.7483e-05
% O-18.09c  9.4315e-05

% ----- Table 5 — Fuel 2.4% Enriched, Density (g/cc) 10.29769

mat fuel24  sum  rgb 150 150 50 burn 1
 U-234.09c  4.4843e-06
 U-235.09c  5.5815e-04
 U-238.09c  2.2408e-02
  O-16.09c  4.5829e-02
  O-17.09c  1.7457e-05
% O-18.09c  9.4178e-05

% ----- Table 6 — Fuel 3.1% Enriched, Density (g/cc) 10.301870

mat fuel31  sum  rgb 50 50 150 burn 1
 U-234.09c  5.7988e-06
 U-235.09c  7.2176e-04
 U-238.09c  2.2254e-02
  O-16.09c  4.5851e-02
  O-17.09c  1.7466e-05
% O-18.09c  9.4224e-05

% --- Table 7 — Air, Density (g/cc) 0.000616

mat air     sum  rgb 255 255 255
% C-12.09c  6.7565e-09
% C-13.09c  7.3076e-11
 C-nat.09c  6.8296e-09
  O-16.09c  5.2864e-06
  O-17.09c  2.0137e-09
% O-18.09c  1.0863e-08
  N-14.09c  1.9681e-05
  N-15.09c  7.1900e-08
 Ar-36.09c  7.9414e-10
 Ar-38.09c  1.4915e-10
 Ar-40.09c  2.3506e-07

% --- Air in instrumentation tube (used to score detectors)

mat itube   sum  rgb 255 200 200
% C-12.09c  6.7565e-09
% C-13.09c  7.3076e-11
 C-nat.09c  6.8296e-09
  O-16.09c  5.2864e-06
  O-17.09c  2.0137e-09
% O-18.09c  1.0863e-08
  N-14.09c  1.9681e-05
  N-15.09c  7.1900e-08
 Ar-36.09c  7.9414e-10
 Ar-38.09c  1.4915e-10
 Ar-40.09c  2.3506e-07

% ----- Table 8 — Borosilicate Glass, Density (g/cc) 2.260000
mat BP      sum  rgb 50 200 50 burn 1
  B-10.09c  9.6506e-04
  B-11.09c  3.9189e-03
  O-16.09c  4.6511e-02
  O-17.09c  1.7717e-05
% O-18.09c  9.5581e-05
 Al-27.09c  1.7352e-03
 Si-28.09c  1.6924e-02
 Si-29.09c  8.5977e-04
 Si-30.09c  5.6743e-04

% ----- Table 9 — Ag-In-Cd Control Rods, Density (g/cc) 10.160000

mat AIC     sum  rgb 200 50 200
Ag-109.09c  2.1854e-02
Ag-107.09c  2.3523e-02
In-113.09c  3.4291e-04
In-115.09c  7.6504e-03
Cd-106.09c  3.4019e-05
Cd-108.09c  2.4221e-05
Cd-110.09c  3.3991e-04
Cd-111.09c  3.4835e-04
Cd-112.09c  6.5669e-04
Cd-113.09c  3.3257e-04
Cd-114.09c  7.8188e-04
Cd-116.09c  2.0384e-04


% ----- Table 10 — Helium, Density (g/cc) 0.001598

mat helium  sum  rgb 200 200 20
  He-4.09c  2.4044e-04

% ----- Table 11 — Inconel 718, Density (g/cc) 8.2000

mat inc718  sum  rgb 150 150 150
 Si-28.09c  5.6753e-04
 Si-29.09c  2.8831e-05
 Si-30.09c  1.9028e-05
 Cr-50.09c  7.8239e-04
 Cr-52.09c  1.5088e-02
 Cr-53.09c  1.7108e-03
 Cr-54.09c  4.2586e-04
 Mn-55.09c  7.8201e-04
 Fe-54.09c  1.4797e-03
 Fe-56.09c  2.3229e-02
 Fe-57.09c  5.3645e-04
 Fe-58.09c  7.1392e-05
 Ni-58.09c  2.9320e-02
 Ni-60.09c  1.1294e-02
 Ni-61.09c  4.9094e-04
 Ni-62.09c  1.5653e-03
 Ni-64.09c  3.9864e-04
 
 
 % ----- Table 13 — Zircaloy 4, Density (g/cc) 6.55

mat zirc4   sum  rgb 200 200 200
  O-16.09c  3.0743e-04
  O-17.09c  1.1711e-07
% O-18.09c  6.3176e-07
 Cr-50.09c  3.2962e-06
 Cr-52.09c  6.3564e-05
 Cr-53.09c  7.2076e-06
 Cr-54.09c  1.7941e-06
 Fe-54.09c  8.6699e-06
 Fe-56.09c  1.3610e-04
 Fe-57.09c  3.1431e-06
 Fe-58.09c  4.1829e-07
 Zr-90.09c  2.1827e-02
 Zr-91.09c  4.7600e-03
 Zr-92.09c  7.2758e-03
 Zr-94.09c  7.3734e-03
 Zr-96.09c  1.1879e-03
Sn-112.09c  4.6735e-06
Sn-114.09c  3.1799e-06
Sn-115.09c  1.6381e-06
Sn-116.09c  7.0055e-05
Sn-117.09c  3.7003e-05
Sn-118.09c  1.1669e-04
Sn-119.09c  4.1387e-05
Sn-120.09c  1.5697e-04
Sn-122.09c  2.2308e-05
Sn-124.09c  2.7897e-05

% ----- Table 14 — Borated Water, Density (g/cc) 0.740582
/*
mat cool    sum  rgb 100 150 250  moder lwtr 1001
  B-10.09c  8.0042e-06
  B-11.09c  3.2218e-05
   H-1.09c  4.9457e-02
   H-2.09c  7.4196e-06
  O-16.09c  2.4672e-02
  O-17.09c  9.3982e-06
% O-18.09c  5.0701e-05
*/

mat water   sum  moder lwtr 1001
   H-1.09c  4.9457e-02
   H-2.09c  7.4196e-06
  O-16.09c  2.4672e-02
  O-17.09c  9.3982e-06
% O-18.09c  5.0701e-05

mat boron   1.0
  B-10.09c  0.1990
  B-11.09c  0.8010
  
  % ----- Table 15 — Carbon Steel, Density (g/cc) 7.800

mat steel   sum  rgb 100 100 100
% C-12.09c  9.6726e-04
% C-13.09c  1.0462e-05
 C-nat.09c  9.7772e-04
 Si-28.09c  4.2417e-04
 Si-29.09c  2.1548e-05
 Si-30.09c  1.4221e-05
 Mn-55.09c  1.1329e-03
  P-31.09c  3.7913e-05
 Mo-92.09c  3.7965e-05
 Mo-94.09c  2.3725e-05
 Mo-96.09c  4.2875e-05
 Mo-97.09c  2.4573e-05
 Mo-98.09c  6.2179e-05
Mo-100.09c  2.4856e-05
 Fe-54.09c  4.7714e-03
 Fe-56.09c  7.4900e-02
 Fe-57.09c  1.7298e-03
 Fe-58.09c  2.3020e-04
 Ni-58.09c  2.9965e-04
 Ni-60.09c  1.1543e-04
 Ni-61.09c  5.0175e-06
 Ni-62.09c  1.5998e-05
 Ni-64.09c  4.0742e-06
 %}
 % === END BASE ON BEAVERS INPUT BY VTT
 %}
		
%}	