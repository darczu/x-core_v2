% X-Core v2.0 developed by Piotr Darnowski - all rights reserved
% Rev 30.03.2017 
% Rev 21-07-2017 modifications to include meta nuclides
% to be used once to save XSDATa.mat file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Read molar masses available in SERPENT ENDF XSDATA file
% Results: [g/mol] - Gram Atomic Weight or Molar Mass
% SCRIPT TO READ XSDATA FILE TO IDENTFY ISOTOPES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear; clc; close all;
file_xsdata = 'sss_endfb7u.xsdata';
file_xsdata2 = 'XSDATA.xlsx';
sheet = 'ZAID-MASS';
%range1 = 'B2:B5183';
range1 = 'E2:E5183';
range2 = 'C2:C5183';
%range3 = 'C2:C5183'; %meta nuclide flag

%ZAID_XSDATA = 		xlsread(file_xsdata2,sheet,range1);   OLD ZAID WITHOUT META
ZAID_XSDATA = 		xlsread(file_xsdata2,sheet,range1); 
AMASS_XSDATA = 	xlsread(file_xsdata2,sheet,range2);
%META_XSDATA = 		xlsread(file_xsdata2,sheet,range3);
%%
ZAID =[ZAID_XSDATA(1)];
AMASS = [AMASS_XSDATA(1)];

test1 = ZAID_XSDATA(1);
%test2 = META_XSDATA(1);
for i=2:numel(ZAID_XSDATA)
   %if(and(ZAID_XSDATA(i)==test1,META_XSDATA(i)==test2))
   if(ZAID_XSDATA(i)==test1)
      %no action
   else
    ZAID = [ZAID; ZAID_XSDATA(i)];
	AMASS = [AMASS; AMASS_XSDATA(i)];
    test1 = ZAID_XSDATA(i);
   end
end

XSDATA=[ZAID, AMASS];
 save('./MAT/XSDATA.mat','XSDATA');
%{
%[ZAID MASS]
fid = fopen(file_xsdata);    %open file
%lineN = linecount(fid);  	 %counting lines in the flie
lineN=5182;	 %end at last isotope, moderators not included

for i=0:lineN-1
 line = fgetl(fid);
 
end

fclose(fid);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ZAID_XSDATA = []; A_XSDATA=[];


{textscan(fif,'%.4f',bank_column,'delimiter','\n','headerlines',key1)}


key1 = 'EXP 3D MAP 1.0E+00';

%% FIND LINE WITH 'EXP 3D MAP 1.0E+00' - given key
key1line =[];
for i=0:lineN2-1
 line = fgetl(FID2);
 k = strfind(line,key1);
 if(k>0)
     key1line = [key1line; i+1]; %Numer lini z key1
 end
end
Nkey1 = numel(key1line);
%% READ DATA TO VARIABLE  - B (burnup, 20 data banks, axial)
Nlines = 20; %number of lines per bank 
Nbanks = 20; %number of square banks with data %N_banks = 193/20;
FID2 = fopen(fileP);

B(i,j,k) = {textscan(FID2,'%.4f',bank_column,'delimiter','\n','headerlines',key1)};  

%}