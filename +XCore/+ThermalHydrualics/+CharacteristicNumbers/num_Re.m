%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% X-Core v2.0 developed by Piotr Darnowski - all rights reserved
% Reynolds number calculation % old function created ~2017, Rev1 19-06-2023
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: mode                   - selection of formula to calculate number\
% 2: varargin               = mode dependent inputs
%
% m                      - mass flow [kg/s]
% A                      - flow area [m2]
% Dh                     - hydraulic diamter, [m]
% mi					 - dynamic viscosity [Pa s] 
% u                      - flow velocity [m/s]
% G = m/A                - mass flux [kg/s/m2] 
% Dh or L                - hydrualic diameter  [m] or characteristic linear demension
% rho                    - density [kg/m3]
% mi                     - dynamics viscosity  [Pa s] or [N s / m2] or [kg /m/s]
% nu = mi/rho           - kinematic viscosity [ m2/s] or [N m s /kg] or [J s / kg]
% Q                     - volumetric flow rate [m3/s] 
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: Re						- Reynolds Number [-]			      
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: Re = G * Dh / mi 
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% {1, 'mass-flux', 'Re = G * Dh / mi', 'Default'}
% Re = G * Dh / mi 
%       G  = in{1};   
%       Dh = in{2}; 
%       mi = in{3};
% -------------------------------------------------
% {2, 'velocity', 'u * L / nu'}
% Re = u * L / nu 
%        u  = in{1}; 
%        L  = in{2}; 
%        nu = in{3};
% -------------------------------------------------
% 3: Re = rho * u * L / mi
%
% -------------------------------------------------
% 4: Re = m * L / (mi * A)
%
% -------------------------------------------------
% 5: Re = rho * Q * L /(mi * A)
% 
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%REFERENCES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% see REFERENCES.m 
% https://en.wikipedia.org/wiki/Dimensionless_numbers_in_fluid_mechanics
% COMMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function [Re] = num_Re(mode, varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [Re] = num_Re(mode, varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
num  = numel(varargin);
in = varargin;
if (num < 3 | num > 4)
   error('Some error with input');
end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Different definitions of Reynolds number
switch mode
    case {1, 'mass-flux', 'Re = G * Dh / mi', 'Default'}
        disp('Re = G * Dh / mi');
        G = in{1};   
        Dh = in{2}; 
        mi = in{3};
        Re(:) = G.*Dh./mi;

    case {2, 'velocity', 'u * L / nu'}
        disp('Re = u * L / nu');
        u  = in{1}; 
        L  = in{2}; 
        nu = in{3};
        Re(:) = u.*L./nu;

    case {3}

    case {4}

    case {5}

    otherwise
        error('No such option.');
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Reynolds number	Re	
%Re={\displaystyle \mathrm {Re} ={\frac {UL\rho }{\mu }}={\frac {UL}{\nu }}}	
% fluid mechanics (ratio of fluid inertial and viscous forces)[4]
%
% % {\displaystyle \mathrm {Re} ={\frac {uD_{\text{H}}}{\nu }}={\frac {\rho uD_{\text{H}}}{\mu }}={\frac {\rho QD_{\text{H}}}{\mu A}}={\frac {WD_{\text{H}}}{\mu A}},}
%varargin
% https://ch.mathworks.com/help/matlab/ref/varargin.html
% https://ch.mathworks.com/help/matlab/matlab_prog/support-variable-number-of-inputs.html
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% OLD function %function 
% [Re] = num_Re(in1, in2, in3, mode)
% function [Re] = num_Re(m,A,Dh,mi)
% G = m./A;
% Re = G.*Dh./mi;