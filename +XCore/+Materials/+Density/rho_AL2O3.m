

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function rho = rho_AL2O3(T,mode,p)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    import XCore.AtomicDensityPackage.*
    import XCore.Materials.Density.*

    rho_liq_pT = @density_liquid_pT; 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
if(nargin < 1)
    T =300.0;  % Default Temperature 
end
if(nargin < 2)
 	mode = 'Kolev'; %Default
end
if(nargin < 3)
 p = 1e5;
end

NT = numel(T); 
rho = zeros(1,NT);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch mode
    % Kolev based