%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% X-Core v2.0 developed by Piotr Darnowski - all rights reserved
% Iron Metal Density   % Rev0 14-07-2017, Rev1 21-02-2021, Rev 2 16-06-2023
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: T				- temperature, [K]
% 2: mode			- mode, see below - options 
% 4: p				- pressure, [Pa]     
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: rho			- density, [g/cc]
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% p 				- 1E5
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% REFERENCES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% see REFERENCES.m file
% [1] - Density Stratification and Fission Product Partitioning in Molten Corium Phases
% COMMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function out = rho_FE(T,p)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function out = rho_FE(T,p)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


	if(nargin < 1)
		T =300.0;  % Default Temperature 
	end
	if(nargin < 2)
	p = 1e5;
	end

	NT = numel(T); out = zeros(1,NT);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	for i=1:NT	
		out(i) = 8.612 - (0.883e-3).*T(i);
	end	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%{
    if(strcmp(mode,'Kolev_Iron') | strcmp(mode,'Leibowitz') | strcmp(mode,'Kolev1'))
	% KOLEV DATABASE  - Iron Density
	[ ident, T0, rho0, rho0beta, beta, kappa, cpliq, INFO ] MAT_MELT_DATABASE(5);
	for i=1:numel(T)
		%if(T(i)<T0)     	
			rho(i) = 8084. + (-4.209e-1).*T(i) + (-3.894e-5).*T(i).^2;  %[10]  based on Leibowitz et al. (1976).
			rho(i)=rho(i)/1000;
		%else
		%	rho(i) = rho_liq_pT(T(i),T0,p,beta,kappa,rho0)/1000;
		%end
    end
    
    %}
