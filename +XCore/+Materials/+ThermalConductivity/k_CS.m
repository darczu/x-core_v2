%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% X-Core v2.0 developed by Piotr Darnowski - all rights reserved
% Rev 29-09-2018
% Update 24-07-2020
% Carbon Steel th. cond.  T [K] and rho [g/cc]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: T				      - temperature, [K]
% 2: p				      - pressure, [Pa]
% 3: mode			      - mode, see below - options 
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: rho			      - density, [g/cc]
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% T                 - 300 K
% p                 - 1e5 Pa
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1:
% 2
% 3: 
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 
%REFERENCES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% see REFERENCES.m file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function k = k_CS(T,mode)

if(nargin < 2)
 mode ='MELCOR';  %Default 'MELCOR'
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch mode
	
case {1, 'MELCOR'}	
	% Reference - MELCOR Reference Manual [4]
		T1 = [
	273.15 
	373.15 
	473.15 
	573.15 
	673.15 
	773.15 
	873.15 
	973.15 
	1076.80 
	1173.15 
	1273.15 
	1373.15 
	1473.15 
	1573.15 
	1673.15 
	1773.15 
	1810.90 
	5000.00 
		];
		k1 = [
	45.437 
	44.229 
	42.681 
	40.794 
	38.568 
	36.002 
	33.098 
	29.854 
	26.135 
	27.100 
	28.100 
	29.100 
	30.100 
	31.100 
	32.100 
	33.100 
	33.477 
	33.477 
	];

	% Linear inteprolation %vq = interp1(x,y,xi)
	for i=1:numel(T)
		k(i) = interp1(T1,k1,T(i));
		% No extrapolation is allowed
		if(T(i)>T1(end))
			disp('WARNING: temperature above allowed');
		elseif (T(i)<T1(1))
			disp('WARNING: temperature below allowed');
		end	
	end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	case {2}  
% I do not remeber source of the data ?!
	data = [
			273.15          55.0
			373.15          52.0
			473.15          48.0
			573.15          45.0
			673.15          42.0
			873.15          35.0
			1073.15          31.0
			1223.15          29.0
			1473.15          31.0
			9973.15          31.0
	];
		T1 = data(:,1);	
		k1 = data(:,2);

		for i=1:numel(T)
			k(i) = interp1(T1,k1,T(i),'linear','extrap');
			
			if(T(i)>T1(end))
				disp('WARNING: temperature above allowed');
			elseif (T(i)<T1(1))
				disp('WARNING: temperature below allowed');
			end	
			
		end	
		
		
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% https://matmatch.com/materials/minfm32015-din-17243-grade-15mo3?utm_term=&utm_medium=ppc&utm_campaign=%5BSD%5D+Materials+Generic&utm_source=adwords&hsa_grp=61532612146&hsa_cam=918492904&hsa_src=g&hsa_net=adwords&hsa_tgt=aud-421245424252:dsa-19959388920&hsa_ad=311442906172&hsa_mt=b&hsa_kw=&hsa_ver=3&hsa_acc=2813184419&gclid=Cj0KCQjw6uT4BRD5ARIsADwJQ19_6P9SetIpT4pHUC5vWD2Vkl4b0TyYNoHHrWOoJAo6no3ldKrTYGsaApOxEALw_wcB
	case {4, '15Mo3_2', 'NARSIS_Forver_2'}

	data = [	
		19.537389769688957, 51.96896728700623
		99.83358138530372, 50.94067554144385
		199.53635784091892, 48.698744085918406
		299.6125468361977, 44.8767693925212
		399.469308251599, 41.88391904326527
		499.3251922884893, 38.965923977869785
		599.1835010805371, 35.8410561279873
		3000.0, 33.0								%ADDED ATRIFICALLY
		];
	
		T1 = data(:,1);
		T1 = T1+273.15;		
		k1 = data(:,2);

		for i=1:numel(T)
			k(i) = interp1(T1,k1,T(i),'linear','extrap');
			
			if(T(i)>T1(end))
				disp('WARNING: temperature above allowed');
			elseif (T(i)<T1(1))
				disp('WARNING: temperature below allowed');
			end	
			
		end	

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
otherwise
	error('no such modes');
end  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%