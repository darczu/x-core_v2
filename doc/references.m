% IT will be updated with manual in latex when ready
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Main References
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% [001]-[199] General Documents and Fuels
% [1] 	- Density Stratification and Fission Product Partitioning in Molten Corium Phases
% [2] 	- MATPRO RELAP5 3D Vol 4 A LIBRARY OF  MATERIALS PROPERTIES FOR  LIGHT-WATER-REACTOR ACCIDENT  ANALYSIS
% [3] 	- Carbajo  A review of the thermophysical proporties of MOX and UO2 fuels.
% [4] 	- MELCOR Reference Manual or MELCOR User Guide
% [5] 	- PNNL-15870rev1 Compendium of Material  Composition Data for Radiation  Transport Modeling
% [6] 	- SCALE Code Data Manual
% [7] 	- BEAVRS Benchmark MIT 2.0.2, Specification
% [8] 	- High-Temperature Density Determination of Boron Oxide and Binary Rubidium and Cesium Borates  >600degC
% [9] 	- Thermal Expansion - Nonmetallic Solids <600degC    PAGE 1352 % Old Kolev for Nuclear was vol. 4
% [10] 	- Kolev, Multiphase Flow Dynamics 5 -  Nuclear Thermal Hydraulics, 2011, Springer, mainly Chapter 17
% [11] 	- Kolev, Multiphase Flow Dynamics 1 - Fundamentals, 2011, Springer
% [12] 	- NUREG/CR-7024 Rev1; PNNL-19417 - Material Property Correlations: Comparisons between FRAPCON-3.4, FRAPTRAN1.4, and MATPRO
% [13]	- IAEA-TECDOC-1496 Thermophysical proporties database of materials for light water reactors and ....
% [14]  - Fink, Thermophysical Proporties of UO2, 2000, J. Nuc. Materials
% [20]  - IAEA-TECDOC-949 
% [21]  - ORNL/TM-2000/351 Thermophysical Props of MOX and UO2....
% [22]  - UK EPR PCSR Ch 16.2
% [50] https://en.wikipedia.org/wiki/Boron_trioxide
% [51] https://www.americanelements.com/zirconium-diboride-12045-64-6
% [52] Fast Spec Reactors
% [40] - MCNP Criticality Primer Appendix Number Density Calculations
% [41] - Thermal Properties of Structural Materials Found in Light Water  Reactor Vessels INL/EXT-09-16121
% [45] - Westinghouse model of Krshko NPP presented in NENE2014 conf
% [50] - Modular High-temperature Gas-cooled Reactor Power Plant, Book Springer, Kugler, 2013
% [60] - J. Wallenius Transmutation of Nuclear Waste
% [61] - FOREVER Experiment Report - publicy available, see (Sehgal, 1999)

% [70] - inconel-alloy-690.pdf 

% [600]-[699] Thorium 
% [600] - Springer, Thesical Proporties of Thoria-based Fuels
% [601] - zajete
% [602] - zajete
% [603] - zajete
% [604] - Melting behavior of (Th,U)O2and (Th,Pu)O2mixed oxides  Journal of Nuclear Materials 479 (2016) 112-122

% [70] - H. Anglart, Thermal-Hydraulics in Nuclear systems
% 

% Other
% []



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%{
  BIBTeX references - use http://www.citationmachine.net/bibtex/cite-a-website
@misc{[1] , title = "Density Stratification and Fission Product Partitioning in Molten Corium Phases",
	url={http://www.oecd-nea.org/nsd/workshops/masca2004/oc/papers/USA_powers.pdf}}

@misc{[2], title={SCDAP/RELAP5/MOD 3.3 Code Manual: MATPRO - A Library of Materials Properties for Light-Water-Reactor Accident Analysis (NUREG/CR-6150, Volume 4, Revision 2)}, 
	url={https://www.nrc.gov/reading-rm/doc-collections/nuregs/contract/cr6150/v4/}, 
	journal={United States Nuclear Regulatory Commission}}
	
@article{[3], title={Thermophysical Properties of MOX and UO2 Fuels Including the Effects of Irradiation}, 
	DOI={10.2172/777671}, author={Carbajo, J J}, 
	year={2001}, month={Nov}}	 
	 
%}	
% UNDER DEVELOPMENT

