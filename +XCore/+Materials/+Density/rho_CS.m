%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% X-Core v2.0 developed by Piotr Darnowski - all rights reserved
% Carbon Steel Density  Rev 01-07-2019, 21-02-2021
% Function is used by rho_SS which is more general for steels
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: T				- temperature, [K]
% 2: mode			- mode, see below - options 
% 4: p				- pressure, [Pa]     
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1: rho			- density, [g/cc]
% DEFAULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mode  			- 'MELCOR'  
% p 				- 1E5
% MODES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% EXAMPLES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% REFERENCES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% see REFERENCES.m file
% COMMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function rho = rho_CS(T,mode,p)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


	if(nargin <1)
		T =300.0;  % Default Temperature 
	end

	if(nargin < 2)
	mode ='MELCOR';
	end

	if(nargin < 3)
	p = 1e5;
	end

	NT = numel(T); rho = zeros(1,NT);
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch mode
	case {1,'Powers'}	
		error('Not implemented');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	case {2,'MELCOR'}	
	%[4] Base on MELCOR MANUAL
		%rho(1:NT) = 7.7529;
		rho(:) = 7.7529;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	case {3,'NARSIS_Forever_1', '15Mo3_1'}	
		% NARSIS Gen II 
		%rho(1:NT) = 7.85;
		rho(:) = 7.85;
		
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		
% 	https://matmatch.com/materials/minfm32015-din-17243-grade-15mo3?utm_term=&utm_medium=ppc&utm_campaign=%5BSD%5D+Materials+Generic&utm_source=adwords&hsa_grp=61532612146&hsa_cam=918492904&hsa_src=g&hsa_net=adwords&hsa_tgt=aud-421245424252:dsa-19959388920&hsa_ad=311442906172&hsa_mt=b&hsa_kw=&hsa_ver=3&hsa_acc=2813184419&gclid=Cj0KCQjw6uT4BRD5ARIsADwJQ19_6P9SetIpT4pHUC5vWD2Vkl4b0TyYNoHHrWOoJAo6no3ldKrTYGsaApOxEALw_wcB	
	case {4, 'NARSIS_Forever_2', '15Mo3_2'}	

	% in reference there are more datapoints
		T1 = [
		20.0
		600.0
		3000.0 %added artifcially
			];
		T1 = T1+273.15;
		
		rho1 = [
		7.848
		7.643
		7.643  %added artifically
			];

		for i=1:numel(T)
			rho(i) = interp1(T1,rho1,T(i));
		end	

	
	otherwise
		error('No function');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
